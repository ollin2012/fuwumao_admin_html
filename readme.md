# 首次安装 #
1. 安装 nodejs

1. 安装 .net framework 45 (仅windows需要)

1. 安装 visual studio 2012以上版本 (仅windows需要)

1. 安装 python 2.x
   安装成功后请在cmd中输入python --version进行确认

1. 全局安装 gulp
	`npm install -g gulp`

1. 全局安装 bower
	`npm install -g bower`

1. 安装 npm 插件
	`npm install`

1. 安装 bower 插件 	
	`bower install`

# 插件更新 #
    npm install
	bower install

# 测试 #
1. 本地浏览器测试 + mock ##
    gulp

1. 开发机发布 ##
    gulp dev
    把生成在dist下的内容复制到Web服务器下

# 生成帮助文档 #
	gulp doc
	生成后的文档在./doc目录下

# 代码生成 #
1. 在generator的datas下以表名建立json定义文件
   如： photo.json

1. 执行gulp gen --data [json定义文件名]
   如： gulp gen --data photo

1. 代码模板会生成在generator/outputs下，将生成的目录复制到工程目录下。

## 常用组件 ##
1. Bootstrap 3 样式库
   [Bootstrap3](http://v3.bootcss.com/)
   *注意：Bootstrap的Javscript插件中的部分不能直接使用， 需要使用ui-bootstrap。

1. 基本功能库
	1. [lodash](https://lodash.com/)
		提供了针对数组，对象等的一些常用扩展方法

1. [ui-bootstrap](https://angular-ui.github.io/bootstrap/)
	bootstrap js组件的angular版本， 需要翻墙

1. 输入项目验证组件	
	[angular-validation](https://github.com/huei90/angular-validation)

1. 表格组件
	[ng-table](https://github.com/esvit/ng-table)

1. API Mock组件
	[gulp-rest-emulator](https://github.com/temrdm/gulp-rest-emulator)
	分状态用法：API Url后加上?restEmulatorPreset=[prefix]

1. 多选框组件
	[angularjs-dropdown-multiselect](http://dotansimha.github.io/angularjs-dropdown-multiselect/#/)

1. 日期库
	1. [moment](http://momentjs.com/docs/)
		提供了各种日期检查、操作、格式化

1. 上传组件
	1. [angular-file-upload](https://github.com/nervgh/angular-file-upload)
	2. [阿里云OSS](https://help.aliyun.com/document_detail/44688.html?spm=5176.doc44686.6.934.XxUisA)