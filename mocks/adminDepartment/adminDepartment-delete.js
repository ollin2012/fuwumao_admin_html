/**
 * mock for 后台组织删除
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/adminDepartment/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
