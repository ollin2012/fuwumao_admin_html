/**
 * mock for 取得后台无组织成员列表
 * @type {Object}
 *
 * params: 
 * {}
 */
module.exports = {
    '/api/adminUnDepartmentUser/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        userId: 6,
                        userName: "GH006",
                        name: "小猫6",
                        updateTime: "2016-12-01 09:00:00:000"
                    }, {
                        userId: 7,
                        userName: "GH007",
                        name: "小猫7",
                        updateTime: "2016-12-02 09:00:00:000"
                    }, {
                        userId: 8,
                        userName: "GH008",
                        name: "小猫8",
                        updateTime: "2016-12-03 09:00:00:000"
                    }, {
                        userId: 9,
                        userName: "GH009",
                        name: "小猫9",
                        updateTime: "2016-12-04 09:00:00:000"
                    }, {
                        userId: 10,
                        userName: "GH010",
                        name: "小猫10",
                        updateTime: "2016-12-05 09:00:00:000"
                    }, ]
                }
            }
        }
    }
};
