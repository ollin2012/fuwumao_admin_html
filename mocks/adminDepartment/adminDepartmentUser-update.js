/**
 * mock for 后台组织成员变更
 * @type {Object}
 *
 * params: 
 * {
    id: "1",
    //设定后组织成员
    departmentUserList:[{
        userId:""
        userName:"",
        updateTime：""
    },{...}],
    //设定后非组织成员
    unDepartmentUserList:[...]
 }
 */
module.exports = {
    '/api/adminDepartmentUser/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
