/**
 * mock for 部门列表
 * @type {Object}
 *
 */
module.exports = {
    '/api/adminDepartment/getCodeList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        id: "1",
                        departmentName: "管理部"
                    },{
                        id: "2",
                        departmentName: "服务管理部"
                    },{
                        id: "3",
                        departmentName: "交易处理部"
                    },{
                        id: "4",
                        departmentName: "财务部"
                    },{
                        id: "5",
                        departmentName: "人事行政部"
                    }]
                }
            }
        }
    }
};