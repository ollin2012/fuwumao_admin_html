/**
 * mock for 后台组织详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/adminDepartment/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: 1,
                    departmentName: '后台组织1',
                    departmentDescr: '描述',
                    updateTime: '2016-01-22 23:30:30:000'
                }
            }
        }
    }
};
