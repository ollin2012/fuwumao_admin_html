/**
 * mock for 后台组织变更
 * @type {Object}
 *
 * params:
 * {
 * id: "1",
 * departmentName: "组织名",
 * departmentDescr: "组织详细",
 * }
 */
module.exports = {
    '/api/adminDepartment/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
