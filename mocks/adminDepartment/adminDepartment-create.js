/**
 * mock for 后台组织新增
 * @type {Object}
 *
 * params:
 * {
 	departmentName: "组织名",
 	departmentDescr: "组织描述", 
 * }
 */
module.exports = {
    '/api/adminDepartment/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
