/**
 * mock for 后台组织列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     departmentName: "组织名", 
 *     departmentDescr: "组织描述", 
 * }
 */
module.exports = {
    '/api/adminDepartment/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 25,
                    list: [{
                        id: 1,
                        departmentName: '后台组织1',
                        departmentDescr: '后台组织描述1',
                    }, {
                        id: 2,
                        departmentName: '后台组织2',
                        departmentDescr: '后台组织描述2',
                    }, {
                        id: 3,
                        departmentName: '后台组织3',
                        departmentDescr: '后台组织描述3',
                    }, {
                        id: 4,
                        departmentName: '后台组织4',
                        departmentDescr: '后台组织描述4',
                    }, {
                        id: '5',
                        departmentName: '后台组织5',
                        departmentDescr: '后台组织描述5',
                    }, ]
                }
            }
        }
    }
};
