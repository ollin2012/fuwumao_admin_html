/**
 * mock for 取得后台组织成员列表
 * @type {Object}
 *
 * params: 
 * {
 *     id: "1", 
 * }
 */
module.exports = {
    '/api/adminDepartmentUser/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        userId: 1,
                        userName: "GH001",
                        name: "小猫1",
                        updateTime: "2016-12-01 09:00:00:000"
                    }, {
                        userId: 2,
                        userName: "GH002",
                        name: "小猫2",
                        updateTime: "2016-12-02 09:00:00:000"
                    }, {
                        userId: 3,
                        userName: "GH003",
                        name: "小猫3",
                        updateTime: "2016-12-03 09:00:00:000"
                    }, {
                        userId: 4,
                        userName: "GH004",
                        name: "小猫4",
                        updateTime: "2016-12-04 09:00:00:000"
                    }, {
                        userId: 5,
                        userName: "GH005",
                        name: "小猫5",
                        updateTime: "2016-12-05 09:00:00:000"
                    }, ]
                }
            }
        }
    }
};
