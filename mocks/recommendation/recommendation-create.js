/**
 * mock for 推荐管理新增
 * @type {Object}
 *
 * params:
 * {
 * 	name: "test1", 
 * }
 */
module.exports = {
    '/api/recommendation/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
