/**
 * mock for 推荐管理删除
 * @type {Object}
 */
module.exports = {
    '/api/recommendation/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
