/**
 * mock for 推荐管理列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/recommendation/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	"id": "1",
                        "companyNumber": "C1234",
                        "companyName": "测试公司",
                        "targetType": '1',
                        "serviceNumber": "#121",
                        "serviceName": "服务名称",
                        "weight": 1,
                        "targetId": 1,
                        "recommendationDate": "2017-01-01 11:22:33.345",
                        "recommendationStatus": 1
                    }, {
                    	"id": "2",
                        "companyNumber": "C1234",
                        "companyName": "测试公司",
                        "targetType": '2',
                        "serviceNumber": "#122",
                        "serviceName": "服务名称",
                        "weight": 1,
                        "targetId": 1,
                        "recommendationDate": "2017-01-01 11:22:33.345",
                        "recommendationStatus": 1
                    }, {
                    	"id": "3",
                        "companyNumber": "C1234",
                        "companyName": "测试公司",
                        "targetType": 1,
                        "serviceNumber": "#123",
                        "serviceName": "服务名称",
                        "weight": 1,
                        "targetId": 1,
                        "recommendationDate": "2017-01-01 11:22:33.345",
                        "recommendationStatus": 1
                    }, {
                    	"id": "4",
                        "companyNumber": "C1234",
                        "companyName": "测试公司",
                        "targetType": 2,
                        "serviceNumber": "#124",
                        "serviceName": "服务名称",
                        "weight": 1,
                        "targetId": 1,
                        "recommendationDate": "2017-01-01 11:22:33.345",
                        "recommendationStatus": 1
                    },{
                    	"id": "5",
                        "companyNumber": "C1234",
                        "companyName": "测试公司",
                        "targetType": 1,
                        "serviceNumber": "#125",
                        "serviceName": "服务名称",
                        "weight": 1,
                        "targetId": 1,
                        "recommendationDate": "2017-01-01 11:22:33.345",
                        "recommendationStatus": 1
                    }, ]
                }
            }
        }
    }
};
