/**
 * mock for 推荐管理变更
 * @type {Object}
 */
module.exports = {
    '/api/recommendation/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
