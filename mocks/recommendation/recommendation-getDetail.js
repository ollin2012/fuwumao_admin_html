/**
 * mock for 推荐管理详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/recommendation/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    "targetType": '1',
                    "weight": '1',
                    "companyId": '1',
                    "companyName": "服务猫",
                    "companyNumber": "QY00000000001",
                    "serviceId":"2",
                    "serviceName":"快递",
                    "serviceNumber":"FW00000001",
                    "recommendationStatus": "1",
                    "recommendationDate": "2017-01-01 11:22:33.456",
                    "updateTime": "2017-01-02 11:22:33.456"
                },
            }
        }
    }
};
