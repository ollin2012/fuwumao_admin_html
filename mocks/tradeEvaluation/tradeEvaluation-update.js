/**
 * mock for 交易评价变更
 * @type {Object}
 *
 * params: 
 * {
 *     id: "1", 
 *     evaluationScore: "10", 
 *     evaluationContent: "JY001", 
 *     evaluationReplyContent: "企业1", 
 *     updateTime: "2016-12-12 12:12:12:123", 
 * }
 */
module.exports = {
    '/api/tradeEvaluation/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
