/**
 * mock for TRD029_评价一览取得(tradeEvaluation_getList)
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     tradeNumber: "JY001", 
 *     agencyName: "企业1", 
 *     companyNumber: "QY00110", 
 *     createTimeFrom: "2016-12-12 12:12:12.123", 
 *     createTimeTo: "2016-12-12 12:12:12.123", 
 *     evaluationScoreFrom: "1", 
 *     evaluationScoreTo: "5", 
 *     sortCols: {tradeNumber:'asc'}companyNameEvaluting/companyNumberEvaluting/companyNameEvaluted/companyNumberEvaluted/evaluationScore/evaluationTime, 
 * }
 */

module.exports = {
    '/api/tradeEvaluation/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: '2',
                    list: [{
                        id: '1',
                        tradeId: '1',
                        tradeNumber: 'JY001',
                        companyIdEvaluting: '1',
                        companyNameEvaluting: '企业1',
                        companyNumberEvaluting: 'QY001',
                        companyIdEvaluted: '2',
                        companyNameEvaluted: '企业2',
                        companyNumberEvaluted: 'QY002',
                        evaluationScore: '5',
                        evaluationTime: '2016-12-12 12:12:12.000'
                    }, {
                        id: '2',
                        tradeId: '2',
                        tradeNumber: 'JY002',
                        companyIdEvaluting: '1',
                        companyNameEvaluting: '企业1',
                        companyNumberEvaluting: 'QY001',
                        companyIdEvaluted: '3',
                        companyNameEvaluted: '企业3',
                        companyNumberEvaluted: 'QY003',
                        evaluationScore: '3',
                        evaluationTime: '2016-12-12 12:12:12.000'
                    }]
                }
            }
        }
    }
};
