/**
 * mock for 平台规则设定详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/platformFee/getList': {
        POST: {
            data: {
                resultCd: 1,
                "results": {
                    "current": [{
                        "id": '1',
                        "companyLevel": "1",
                        "settingStartTime": "2016-11-11 22:22:33.456",
                        "buyerFeePer": '0.1',
                        "sellerFeePer": '0.2'
                    }, {
                        "id": '2',
                        "companyLevel": "2",
                        "settingStartTime": "2017-11-11 22:22:33",
                        "buyerFeePer": '0',
                        "sellerFeePer": '0.2'
                    }, {
                        "id": '3',
                        "companyLevel": "3",
                        "settingStartTime": "2017-11-11 22:22:33",
                        "buyerFeePer": '0',
                        "sellerFeePer": '0.4'
                     }, ],
                    // "reserve": [{
                    //     "id": '1',
                    //     "companyLevel": "1",
                    //     "settingStartTime": "2017-11-11 22:22:33",
                    //     "buyerFeePer": '0.1',
                    //     "sellerFeePer": '0.2'
                    // }, {
                    //     "id": '2',
                    //     "companyLevel": "2",
                    //     "settingStartTime": "2017-11-11 22:22:33",
                    //     "buyerFeePer": '0.5',
                    //     "sellerFeePer": '0.4'
                    // }, {
                    //     "id": '2',
                    //     "companyLevel": "3",
                    //     "settingStartTime": "2017-11-11 22:22:33",
                    //     "buyerFeePer": '0.1',
                    //     "sellerFeePer": '0.2'
                    // }]
                }
            }
        }
    }
};
