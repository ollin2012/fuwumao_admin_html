/**
 * mock for 关键字列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 *     keyword: "关键字", 
 *     targetType: "1", 
 *     displayType: "1", 
 *     searchDate: "2017-01-01", 
 * }
 */
module.exports = {
    '/api/keywords/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 12,
                    list: [{
                        id: '1',
                        targetType: '1',
                        keyword: '关键字1',
                        createDate: '2017-01-01',
                        searchTimes: '333',
                    }, {
                        id: '2',
                        targetType: '1',
                        keyword: '关键字2',
                        createDate: '2017-01-02',
                        searchTimes: '3333',
                    },{
                        id: '3',
                        targetType: '1',
                        keyword: '关键字3',
                        createDate: '2017-01-03',
                        searchTimes: '44444',
                    }]
                }
            }
        }
    }
};
