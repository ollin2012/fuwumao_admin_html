/**
 * mock for 各帮助类型下的一览取得
 * 
 * @type {Object}
 * 
 * params: {}
 */
module.exports = {
    '/api/content/getList': {
        POST: {
            data: {
                resultCd: '1',
                results: {
                	count: 43,
                	list: [{
                        id: '1',
                        language: 'cn',
                        category: '21',
                        title: '怎样注册账户',
                        content: '<img src="/images/common/helper-center-detail-mock.jpg" />',
                        links: '',
                        showType: '1',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '2',
                        language: 'en',
                        category: '21',
                        title: '哪几种情况无法注册成功',
                        content: '<img src="/images/common/helper-center-detail-mock.jpg" />',
                        links: '',
                        showType: '1',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '3',
                        language: 'cn',
                        category: '21',
                        title: '账户注册后，可以修改吗？',
                        content: '<img src="/images/common/helper-center-detail-mock.jpg" />',
                        links: '',
                        showType: '1',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '4',
                        language: 'en',
                        category: '21',
                        title: '账户安全须知',
                        content: '',
                        links: 'http://www.baidu.com',
                        showType: '2',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '5',
                        language: 'cn',
                        category: '21',
                        title: '成为bolstract用户的优势',
                        content: '',
                        links: 'http://www.baidu.com',
                        showType: '2',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '6',
                        language: 'cn',
                        category: '21',
                        title: '怎样注册账户',
                        content: '<img src="/images/common/helper-center-detail-mock.jpg" />',
                        links: '',
                        showType: '1',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '7',
                        language: 'cn',
                        category: '21',
                        title: '哪几种情况无法注册成功',
                        content: '<img src="/images/common/helper-center-detail-mock.jpg" />',
                        links: '',
                        showType: '1',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '8',
                        language: 'cn',
                        category: '21',
                        title: '账户注册后，可以修改吗？',
                        content: '<img src="/images/common/helper-center-detail-mock.jpg" />',
                        links: '',
                        showType: '1',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '9',
                        language: 'cn',
                        category: '21',
                        title: '账户安全须知',
                        content: '',
                        links: 'http://www.baidu.com',
                        showType: '2',
                        updateTime: '2017-01-01 00:00:00'
                	}, {
                        id: '10',
                        language: 'cn',
                        category: '21',
                        title: '成为bolstract用户的优势',
                        content: '',
                        links: 'http://www.baidu.com',
                        showType: '2',
                        updateTime: '2017-01-01 00:00:00'
                	}]
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};
