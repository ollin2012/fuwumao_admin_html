/**
 * mock for 网站内容详情
 * @type {Object}
 *
 * params:
 * {category: "1"}
 */
module.exports = {
    '/api/content/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                	id: '1',
                    language: 'cn',
                    category: "21",
                    content: "<h3>怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户怎样注册账户</h3>",
                    links: "www.baidu.com",
                    showType: "1",
                    title: "怎样注册账户",
                    updateTime: "2017-01-01 02:02:02",
                }
            }
        }
    }
};
