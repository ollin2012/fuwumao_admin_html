/**
 * mock for 网站内容变更
 * @type {Object}
 *
 * params:
 * {
 * category: "1",
 * content: "<h3>网站内容详细常见问题等等等</h3>",
 * links: "www.baidu.com",
 * showType: "1",
 * title: "网站内容",
 * updateTime: "2017-01-01 02:02:02",
 * }
 */
module.exports = {
    '/api/content/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
