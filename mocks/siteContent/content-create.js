/**
 * mock for 网站内容添加
 * @type {Object}
 *
 * params:
 * {
 * category: "1",
 * content: "<h3>网站内容详细常见问题等等等</h3>",
 * links: "www.baidu.com",
 * showType: "1",
 * title: "网站内容",
 * }
 */
module.exports = {
    '/api/content/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
