/**
 * mock for 消息一览变更
 * @type {Object}
 */
module.exports = {
    '/api/message/updateFlg': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
