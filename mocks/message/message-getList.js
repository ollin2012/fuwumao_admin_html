/**
 * mock for 消息一览列表
 * @type {Object}
 *
 * params:
 * {
 *     page: "1",
 *     count: "10",
 *     name: "t",
 * }
 */
module.exports = {
    '/api/message/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    "list": [{
                        "id": "1",
                        "templateCode": "INF_TRD_GIVEN_COMPLAINT",
                        "templateParam": "['TRD20170503000000014','test@126','sunhz@infogreat']",
                        "receiverId": 1, // TODO
                        "links":"main.company.detail",
                        "readFlg":'0',
                        "sendTime": "2016-12-12 12:12:12.000",
                        "messageType": '1',
                        "updateTime": "2016-12-12 12:12:12.111"
                    },{
                        "id": "2",
                        "templateCode": "INF_TRD_VERIFY_SUCCESS",
                        "templateParam": "['TRD20170503000000014','test@126','sunhz@infogreat']",
                        "receiverId": 1, // TODO
                        "links":"main.service.detail",
                        "readFlg":'0',
                        "sendTime": "2016-12-12 12:12:12.000",
                        "messageType": '2',
                        "updateTime": "2016-12-12 12:12:12.111"
                    },{
                        "id": "3",
                        "templateCode": "INF_TRD_VERIFY_SUCCESS",
                        "templateParam": ["JY00001","服务商交易审核人"],
                        "receiverId": 1, // TODO
                        "links":"main.trade.detail",
                        "readFlg":'1',
                        "sendTime": "2016-12-12 12:12:12.000",
                        "messageType": '3',
                        "updateTime": "2016-12-12 12:12:12.111"
                    },{
                        "id": "4",
                        "templateCode": "ERR_CON_CONTENT_TYPE_DUPLICATE",
                        "templateParam": [],
                        "receiverId": 1, // TODO
                        "links":"main.refund.edit",
                        "readFlg":'1',
                        "sendTime": "2016-12-12 12:12:12.000",
                        "messageType": '3',
                        "updateTime": "2016-12-12 12:12:12.111"
                    },{
                        "id": "5",
                        "templateCode": "INF_TRD_BUYER_PAYED_STAGE_PRICE",
                        "templateParam": ["JY00001","需求分析","15,000","人民币"],
                        "receiverId": 2, // TODO
                        "links":"main.company.detail",
                        "readFlg":'1',
                        "sendTime": "2016-12-12 12:12:12.000",
                        "messageType": '3',
                        "updateTime": "2016-12-12 12:12:12.111"
                    },{
                        "id": "6",
                        "templateCode": "ERR_CON_CONTENT_TYPE_IS_NOT_EXIST",
                        "templateParam": [],
                        "receiverId": 2, // TODO
                        "links":"main.trade.detail",
                        "readFlg":'1',
                        "sendTime": "2016-12-12 12:12:12.000",
                        "messageType": '2',
                        "updateTime": "2016-12-12 12:12:12.111"
                    }]
                }
            }
        }
    }
};
