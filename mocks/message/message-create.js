/**
 * mock for 消息一览新增
 * @type {Object}
 *
 * params:
 * {
 * 	name: "test1", 
 * }
 */
module.exports = {
    '/api/messageList/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
