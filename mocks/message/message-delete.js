/**
 * mock for 消息一览删除
 * @type {Object}
 */
module.exports = {
    '/api/messageList/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
