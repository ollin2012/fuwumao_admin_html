/**
 * mock for 消息一览详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/messageList/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: 1,
                    name: '消息一览1',
                }
            }
        }
    }
};
