/**
 * mock for 企业行为列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 *     companyNumber: "1", 
 *     companyName: "10", 
 *     agencyFlg: "t", 
 *     mdFlg: "1", 
 *     actionsTime: "10", 
 * }
 */
module.exports = {
    '/api/companyAct/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 12,
                    list: [{
                        date: '2016-12-31',
                        companyId: '1',
                        companyName: '企业1',
                        agencyFlg: '1',
                        loginTimes: '33',
                        newServiceCount: '3',
                        totalServiceCount: '13',
                        newTradeCount: '7',
                        downTradeCount: '0',
                        totalTradeCount: '55',
                    }, {
                        date: '2016-12-31',
                        companyId: '1',
                        companyName: '企业1',
                        agencyFlg: '1',
                        loginTimes: '33',
                        newServiceCount: '3',
                        totalServiceCount: '113',
                        newTradeCount: '1117',
                        downTradeCount: '0111',
                        totalTradeCount: '11155',
                    }]
                }
            }
        }
    }
};
