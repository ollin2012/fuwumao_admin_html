/**
 * mock for COP037_后台企业认证履历一览得
 * @type {Object}
 *
 * params:
 * {companyId: '1'}
 */
module.exports = {
    '/api/companyCert/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    companyCertId: '1',
                    verifySubmitTime: '2016-12-01 08:00:00',
                    verifyResultFlg: '0',
                    verifyResultDetail: '审核结果描述',
                    updateTime: '2016-12-01 08:00:00',
                    sysVerifyResultFlg: '1',
                    sysVerifyResultDetail: '系统审核结果描述',
                    sysVerifyTime: '2016-12-02 08:00:00',
                    // overseasFlg: '1',
                    // certContentJson: '{"营业执照注册号":"9999999999999999","住所":"上海市徐汇区桂平路680号","法人":"王二","公司类型":"有限责任公司","注册资本":"壹仟万圆整","实收资本":"壹仟万圆整","经营范围":"经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围","成立日期":"2016年12月01日"}',
                    // updateTime: new Date(),
                    // verifyFile: [{
                    //     fileId: 1,
                    //     fileName: '营业执照.jpg',
                    //     type: 'jpg',
                    //     path: 'images/tmp/u225.jpg',
                    //     secretKey: '111dasdsak'
                    // }, {
                    //     fileId: 2,
                    //     fileName: '公司入住服务猫展望.pdf',
                    //     type: 'pdf',
                    //     path: 'images/tmp/u225.jpg',
                    //     secretKey: '111dasdsak'
                    // }],
                    companyCertAccount:[{
                        accountType: '银行',
                        bank:'上海银行',
                        accountNumber:'62011494109317',
                        bankAccountArea: '上海'
                    },{
                        accountType: '银行',
                        bank:'交通银行',
                        accountNumber:'99011494109399',
                        bankAccountArea: '北京'
                    }],
                    companyVerifyHistory:[{
                        companyCertId: 9,
                        verifySubmitTime: new Date(),
                        verifyResultFlg: '0',
                        verifyResultDetail: '1元认证为到账，无法确认。',
                        verifierId: 1001,
                        verifierName: '平台001',
                        verifieTime: new Date()
                    },{
                        companyCertId: 8,
                        verifySubmitTime: '2016-11-29 09:00:00',
                        verifyResultFlg: '0',
                        verifyResultDetail: '支付宝账号不可用',
                        verifierId: 1002,
                        verifierName: '平台002',
                        verifieTime: '2016-11-29 10:00:00'
                    },{
                        companyCertId: 7,
                        verifySubmitTime: '2016-11-28 09:00:00',
                        verifyResultFlg: '1',
                        verifyResultDetail: '通过01',
                        verifierId: 1002,
                        verifierName: '平台002',
                        verifieTime: '2016-11-28 10:00:00'
                    },{
                        companyCertId: 6,
                        verifySubmitTime: '2016-11-27 09:00:00',
                        verifyResultFlg: '1',
                        verifyResultDetail: '通过02',
                        verifierId: 1003,
                        verifierName: '平台003',
                        verifieTime: '2016-11-27 10:00:00'
                    },{
                        companyCertId: 5,
                        verifySubmitTime: '2016-11-26 09:00:00',
                        verifyResultFlg: '1',
                        verifyResultDetail: '通过03',
                        verifierId: 1004,
                        verifierName: '平台004',
                        verifieTime: '2016-11-26 10:00:00'
                    }],
                    companyPassCertAccount:[{
                        accountType: '银行',
                        bank:'上海银行',
                        accountNumber:'62011494109317',
                        bankAccountArea: '上海'
                    },{
                        accountType: '银行',
                        bank:'交通银行',
                        accountNumber:'99011494109399',
                        bankAccountArea: '北京'
                    }],
                    receiveBankCode: '08600001', //中信银行
                    payBankCode: '85200001' //渣打银行
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};