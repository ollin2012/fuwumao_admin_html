/**
 * mock for 企业认证审核
 * @type {Object}
 */
module.exports = {
    '/api/companyCert/verify': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
