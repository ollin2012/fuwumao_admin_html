/**
 * mock for COP007_企业认证情报取得
 * @type {Object}
 *
 * params:
 * {companyId: '1'}
 */
module.exports = {
    '/api/companyCert/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    companyId: '1',
                    overseasFlg:'1',
                    verifySubmitTime: new Date(),
                    certContentJson: '{"营业执照注册号":"9999999999999999","住所":"上海市徐汇区桂平路680号","法人":"王二","公司类型":"有限责任公司","注册资本":"壹仟万圆整","实收资本":"壹仟万圆整","经营范围":"经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围","成立日期":"2016年12月01日"}'
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};