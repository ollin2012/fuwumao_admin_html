/**
 * mock for 投诉详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/tradeComplaint/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: 1,
                    complaintNumber: 'TS00000000001',
                    tradeId: 1,
                    tradeNumber: 'JY00000000001',
                    complaintCompanyId: 1,
                    complaintCompanyName: '投诉方企业一',
                    complaintCompanyNumber: 'QY00000000001',
                    refundApplyFlg: '1',
                    refundApplyAmount: '130000',
                    currencyType: 'RMB',
                    complaintTime: '2017-01-01 19:00:00',
                    complaintStatus: '0',
                    complaintContent: '投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容投诉详细内容',
                    feedbackContent: '被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容被投诉方反馈内容',
                }
            }
        }
    }
};
