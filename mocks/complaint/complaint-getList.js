/**
 * mock for 投诉列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/tradeComplaint/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: 1,
                        complaintNumber: 'TS00000000001',
                        tradeId: 1,
                        tradeNumber: 'JY00000000001',
                        complaintCompanyId: 1,
                        complaintCompanyName: '投诉方企业一',
                        complaintCompanyNumber: 'QY00000000001',
                        refundApplyFlg: '1',
                        refundApplyAmount: '130000',
                        currencyType: 'RMB',
                        complaintTime: '2017-01-01 19:00:00',
                        complaintStatus: '0',
                    }, {
                        id: 2,
                        complaintNumber: 'TS00000000002',
                        tradeId: 2,
                        tradeNumber: 'JY00000000002',
                        complaintCompanyId: 2,
                        complaintCompanyName: '投诉方企业二',
                        complaintCompanyNumber: 'QY00000000002',
                        refundApplyFlg: '1',
                        refundApplyAmount: '180000',
                        currencyType: 'USD',
                        complaintTime: '2017-01-01 19:00:00',
                        complaintStatus: '1',
                    }, {
                        id: 3,
                        complaintNumber: 'TS00000000003',
                        tradeId: 3,
                        tradeNumber: 'JY00000000003',
                        complaintCompanyId: 3,
                        complaintCompanyName: '投诉方企业三',
                        complaintCompanyNumber: 'QY00000000003',
                        refundApplyFlg: '0',
                        refundApplyAmount: '1050000',
                        currencyType: 'RMB',
                        complaintTime: '2017-01-01 19:00:00',
                        complaintStatus: '2',
                    }, {
                        id: 4,
                        complaintNumber: 'TS00000000004',
                        tradeId: 4,
                        tradeNumber: 'JY00000000004',
                        complaintCompanyId: 4,
                        complaintCompanyName: '投诉方企业四',
                        complaintCompanyNumber: 'QY00000000004',
                        refundApplyFlg: '1',
                        refundApplyAmount: '850000',
                        currencyType: 'RMB',
                        complaintTime: '2017-01-01 19:00:00',
                        complaintStatus: '9',
                    }, {
                        id: 5,
                        complaintNumber: 'TS00000000005',
                        tradeId: 5,
                        tradeNumber: 'JY00000000005',
                        complaintCompanyId: 5,
                        complaintCompanyName: '投诉方企业五',
                        complaintCompanyNumber: 'QY00000000005',
                        refundApplyFlg: '0',
                        refundApplyAmount: '60000',
                        currencyType: 'USD',
                        complaintTime: '2017-01-01 19:00:00',
                        complaintStatus: '1',
                    }, ]
                }
            }
        }
    }
};
