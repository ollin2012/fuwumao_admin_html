/**
 * mock for 关联成员企业
 * @type {Object}
 */
module.exports = {
    '/api/companyInvite/relate': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
