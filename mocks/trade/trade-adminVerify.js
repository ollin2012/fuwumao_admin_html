/**
 * mock for 交易审核
 * @type {Object}
 *
 * params: 
 * {
 *   //交易审核履历ID
 *   tradeVerifyId:'1',
 *   verifyResultFlg:'9',
 *   verifyResultDetail:'审核不通过',
 *   updateTime:'2017-01-18 11:30:30',
 * }
 */
module.exports = {
    '/api/trade/adminVerify': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
