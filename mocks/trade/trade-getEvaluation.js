/**
 * mock for 交易评价一览
 * @type {Object}
 *
 * params: 
 * {
 * tradeId:'1',
 * }
 */
module.exports = {
    '/api/trade/getEvaluation': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        id: '1',
                        tradeId: '1',
                        evaluationType: '1',
                        tradeName: '交易1',
                        companyIdEvaluting: '1',
                        companyName: '企业1',
                        companyIdEvaluted: '2',
                        companyNameEvaluted: '企业2',
                        tradeStartTime: '2016-12-11 12:12:12.000',
                        tradeEndTime: '2016-12-14 12:12:12.000',
                        evaluationTime: '2016-12-12 12:12:12.000',
                        evaluationScore: '4',
                        evaluationContent: '服务很好服务很好服务很好服务很好服务很好服务很好',
                        evaluationReplyTime: '2016-12-13 12:12:12.000',
                        evaluationReplyContent: '配合默契配合默契配合默契配合默契配合默契配合默契',
                        updateTime: '2016-12-12 12:12:12.000',
                    }, {
                        id: '2',
                        tradeId: '1',
                        evaluationType: '2',
                        tradeName: '交易1',
                        companyIdEvaluting: '1',
                        companyName: '企业1',
                        companyIdEvaluted: '2',
                        companyNameEvaluted: '企业2',
                        tradeStartTime: '2016-12-12 12:12:12.000',
                        tradeEndTime: '2016-12-15 12:12:12.000',
                        evaluationTime: '2016-12-13 12:12:12.000',
                        evaluationScore: '5',
                        evaluationContent: '服务很好服务很好',
                        evaluationReplyTime: '2016-12-13 12:12:12.000',
                        evaluationReplyContent: '配合默契配合默契',
                        updateTime: '2016-12-12 12:12:12.000',
                    }, ]
                }
            }
        }
    }
};
