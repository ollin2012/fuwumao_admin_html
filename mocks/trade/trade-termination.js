/**
 * mock for 交易终止
 * @type {Object}
 *
 * params: 
 * {
 *   tradeId:'1',
 *   stopReason:'终止理由',
 *   tradeUpdatetime:'2017-01-18 11:30:30',
 * }
 */
module.exports = {
    '/api/trade/termination': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
