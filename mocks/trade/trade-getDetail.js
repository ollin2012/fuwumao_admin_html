/**
 * mock for 交易详情
 * @type {Object}
 *
 * params:
 * {
    //交易ID
    tradeId: '1',
    //查询选项
    queryOpts: {
        //是否包含审核履历
        includeTradeVerifyHistory: '1',
    },
 * }
 */
module.exports = {
    '/api/trade/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: '1',
                    tradeNumber: 'JY001',
                    tradeName: '交易1',
                    agencyId: '1',
                    agencyNumber: 'QY001',
                    agencyName: '企业1',
                    agencyContact: '企业联系人1',
                    agencyPhoneCountry: '08',
                    agencyPhone: '13888888888',
                    companyId: '2',
                    companyNumber: 'QY002',
                    companyName: '企业2',
                    companyContact: '企业联系人2',
                    companyPhoneCountry: '08',
                    companyPhone: '13999999999',
                    tradePrice: '123',
                    tradeDeposit: '12',
                    tradeOrderTime: '2017-01-01 00:00:00',
                    tradeOrderMessage: '订单留言',
                    currencyType: 'RMB',
                    tradeDetail: '<h3 style="color:red">交易交易交易交易交易交易详细内容详细内容详细内容</h3>',
                    tradeMemo: '1.交易备注1 2.交易备注2 3.交易备注3',
                    tradeStatus: '70',
                    evaluationStatus: '1',
                    updateTime: '2016-12-12 12:12:12.000',
                    serviceType: '1',
                    industry1: '100',
                    industry2: '101',
                    area1: '10000',
                    area2: '10800',
                    area3: '10831',
                    currentStageId: '2',
                    tradeStageList: [{
                        stageId: '1',
                        stageName: '阶段1',
                        stageOrder: '1',
                        stagePrice: '123',
                        stageDetail: '<h2 style="color:red">阶段阶段阶段阶段阶段详细内容详细内容详细内容</h2>',
                        stageStartTime: '2016-12-12 12:12:12.000',
                        stageEndTime: '2016-12-15 12:12:12.000',
                        stageStatus: '45',
                        capitalFlowNumber: 'JULS01',
                        capitalFlowMemo: '资金流水备注',
                        capitalFlowStatus: '3',
                        updateTime: '2017-01-01 12:12:12.000'
                    }, {
                        stageId: '2',
                        stageName: '阶段2',
                        stageOrder: '2',
                        stagePrice: '1234',
                        stageDetail: '<h2 style="color:blue">阶段阶段阶段阶段阶段阶段详细内容详细内容详细内容</h2>',
                        stageStartTime: '2017-01-12 12:12:12.000',
                        stageEndTime: '2017-02-15 12:12:12.000',
                        stageStatus: '10',
                        capitalFlowNumber: 'JULS02',
                        capitalFlowMemo: '资金流水备注二',
                        capitalFlowStatus: '1',
                        updateTime: '2017-01-02 12:12:12.000'
                    }],
                    tradePicList: [{
                        fileId: '1',
                        filePath: 'https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/1/service_img/1f9064e4-5d04-4938-823d-edb070c39099.jpg',
                        logoFlg: '1',
                        showOrder: '1'
                    }],
                    tradeVerifyHistoryList: [{
                        tradeVerifyId: '1',
                        sysVerifyResultFlg: '9',
                        sysVerifyResultDetail: '系统审核失败',
                        sysVerifier: '系统',
                        sysVerifyTime: '2017-01-01 09:00:00.000',
                        verifySubmitStatus: '1',
                        verifySubmitTime: '2016-12-16 12:12:12.000',
                        verifyResultFlg: '1',
                        verifyResultDetail: '审核通过',
                        verifierCompanyId: '999',
                        verifierId: '888',
                        verifierName: '平台001',
                        verifierTime: '2016-12-16 12:12:12.000',
                        updateTime: '2016-12-12 12:12:12.000',
                    }],
                    depositInfo: {
                    	paidDeposit: '20000',
                    	paidCurrencyType: 'RMB',
                    	toBePaidDeposit: '10000',
                    	toBePaidCurrencyType: 'RMB',
                    	toBePaidFlowStatus: '1',
                    	toBePaidFlowId: '9',
                    	toBePaidFlowNumber: 'ZJLSD001'
                    }
                }
            }
        }
    }
};
