/**
 * mock for 交易列表
 * @type {Object}
 *
 * params:
 * {
    //交易搁置查询标志 0:不需要1:需要
    tradeDelayFlg: '0',
    //服务ID
    serviceId: '',
    //交易编号
    tradeNumber: '1',
    //服务名称
    tradeName: '服务1',
    //交易状态
    tradeStatus: '1',
    //服务商名称
    sellerAgencyName: '服务商',
    //服务商协会区分
    sellerAgencyInstitute: '1',
    //企业名称
    buyerCompanyName: '企业',
    //企业协会区分
    buyerCompanyInstitute: '1',
    //服务分类
    serviceType: '1',
    //交易价格区间FROM
    tradePriceFrom: '10',
    //交易价格区间To
    tradePriceTo: '1000',
    //货币种类
    currencyType: 'RMB',
    //交易时间区间FROM
    tradeStartTimeFrom: '2016-12-12 12:12:12',
    //交易时间区间To
    tradeStartTimeTo: '2017-01-12 12:12:12',
    //交易语言区分
    tradeLanguage: '1',
    page: '2',
    count: '20',
 * }
 */
module.exports = {
    '/api/trade/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: '9',
                        tradeNumber: 'JY001',
                        tradeName: '软件',
                        sellerAgencyId: '1',
                        sellerAgencyNumber: 'BH001',
                        sellerAgencyName: '服务商1',
                        sellerAgencyInstitute: '1',
                        buyerCompanyId: '2',
                        buyerCompanyNumber: 'BH002',
                        buyerCompanyName: '企业1',
                        buyerAgencyInstitute: '1',
                        tradeLanguage: 'cn',
                        tradePrice: '100',
                        currencyType: 'RMB',
                        tradeDeposit: '10',
                        tradeOrderTime: '2016-12-28 18:00:00',
                        verifySubmitTime: '2016-12-28 18:00:00',
                        tradeStartTime: '2016-12-28 20:00:00',
                        tradeEndTime: '2016-12-30 18:00:00',
                        tradeStatus: '60',
                        evaluationStatus: '4',
                        updateTime: '2016-12-31 18:00:00',
                        tradeStageInfo: [{
                            stageName: '阶段1',
                            stageOrder: '1',
                            stagePrice: '20',
                            stageStartTime: '2016-12-28 18:00:00',
                            stageEndTime: '2016-12-30 18:00:00',
                            stageStatus: '5'
                        }],
                        tradeLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/u225.jpg'
                        },
                    }, {
                        id: '1',
                        tradeNumber: 'JY001',
                        tradeName: '服装',
                        sellerAgencyId: '1',
                        sellerAgencyNumber: 'BH001',
                        sellerAgencyName: '服务商1',
                        sellerAgencyInstitute: '1',
                        buyerCompanyId: '2',
                        buyerCompanyNumber: 'BH002',
                        buyerCompanyName: '企业1',
                        buyerAgencyInstitute: '1',
                        tradeLanguage: 'cn',
                        tradePrice: '100',
                        currencyType: 'RMB',
                        tradeDeposit: '10',
                        tradeOrderTime: '2016-12-28 18:00:00',
                        verifySubmitTime: '2016-12-28 18:00:00',
                        tradeStartTime: '2016-12-28 20:00:00',
                        tradeEndTime: '2016-12-30 18:00:00',
                        tradeStatus: '70',
                        evaluationStatus: '4',
                        updateTime: '2016-12-31 18:00:00',
                        tradeStageInfo: [{
                            stageName: '阶段1',
                            stageOrder: '1',
                            stagePrice: '20',
                            stageStartTime: '2016-12-28 18:00:00',
                            stageEndTime: '2016-12-30 18:00:00',
                            stageStatus: '50'
                        }],
                        tradeLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/u225.jpg'
                        },
                    }, {
                        id: '2',
                        tradeNumber: 'JY002',
                        tradeName: '服装',
                        sellerAgencyId: '2',
                        sellerAgencyNumber: 'BH002',
                        sellerAgencyName: '服务商2',
                        sellerAgencyInstitute: '1',
                        buyerCompanyId: '2',
                        buyerCompanyNumber: 'BH002',
                        buyerCompanyName: '企业1',
                        buyerAgencyInstitute: '1',
                        tradeLanguage: 'en',
                        tradePrice: '100',
                        currencyType: 'RMB',
                        tradeDeposit: '10',
                        tradeOrderTime: '2016-12-28 18:00:00',
                        verifySubmitTime: '2016-12-28 18:00:00',
                        tradeStartTime: '2016-12-28 20:00:00',
                        tradeEndTime: '2016-12-30 18:00:00',
                        tradeStatus: '80',
                        evaluationStatus: '4',
                        updateTime: '2016-12-31 18:00:00',
                        tradeStageInfo: [{
                            stageName: '阶段1',
                            stageOrder: '1',
                            stagePrice: '20',
                            stageStartTime: '2016-12-28 18:00:00',
                            stageEndTime: '2016-12-30 18:00:00',
                            stageStatus: '10'
                        }],
                        tradeLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/u225.jpg'
                        },
                    }, {
                        id: '3',
                        tradeNumber: 'JY003',
                        tradeName: '化学',
                        sellerAgencyId: '2',
                        sellerAgencyNumber: 'BH002',
                        sellerAgencyName: '服务商2',
                        sellerAgencyInstitute: '1',
                        buyerCompanyId: '2',
                        buyerCompanyNumber: 'BH002',
                        buyerCompanyName: '企业1',
                        buyerAgencyInstitute: '1',
                        tradeLanguage: 'en',
                        tradePrice: '100',
                        currencyType: 'RMB',
                        tradeDeposit: '10',
                        tradeOrderTime: '2016-12-28 18:00:00',
                        verifySubmitTime: '2016-12-28 18:00:00',
                        tradeStartTime: '2016-12-28 20:00:00',
                        tradeEndTime: '2016-12-30 18:00:00',
                        tradeStatus: '90',
                        evaluationStatus: '4',
                        updateTime: '2016-12-31 18:00:00',
                        tradeStageInfo: [{
                            stageName: '阶段1',
                            stageOrder: '1',
                            stagePrice: '20',
                            stageStartTime: '2016-12-28 18:00:00',
                            stageEndTime: '2016-12-30 18:00:00',
                            stageStatus: '45'
                        }],
                        tradeLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/u225.jpg'
                        },
                    }, {
                        id: '4',
                        tradeNumber: 'JY004',
                        tradeName: '化学',
                        sellerAgencyId: '2',
                        sellerAgencyNumber: 'BH002',
                        sellerAgencyName: '服务商2',
                        sellerAgencyInstitute: '1',
                        buyerCompanyId: '2',
                        buyerCompanyNumber: 'BH002',
                        buyerCompanyName: '企业1',
                        buyerAgencyInstitute: '1',
                        tradeLanguage: 'en',
                        tradePrice: '100',
                        currencyType: 'RMB',
                        tradeDeposit: '10',
                        tradeOrderTime: '2016-12-28 18:00:00',
                        verifySubmitTime: '2016-12-28 18:00:00',
                        tradeStartTime: '2016-12-28 20:00:00',
                        tradeEndTime: '2016-12-30 18:00:00',
                        tradeStatus: '80',
                        evaluationStatus: '4',
                        updateTime: '2016-12-31 18:00:00',
                        tradeStageInfo: [{
                            stageName: '阶段1',
                            stageOrder: '1',
                            stagePrice: '10',
                            stageStartTime: '2016-12-28 18:00:00',
                            stageEndTime: '2016-12-30 18:00:00',
                            stageStatus: '10'
                        },{
                            stageName: '阶段2',
                            stageOrder: '1',
                            stagePrice: '5',
                            stageStartTime: '2016-12-28 18:00:00',
                            stageEndTime: '2016-12-30 18:00:00',
                            stageStatus: '5'
                        }],
                        tradeLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/u225.jpg'
                        },
                    }]
                }
            }
        }
    }
};
