/**
 * mock for 聊天记录列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: '1',
 *     count: '10',
 *     companyNumber: '相关企业编号',
 *     companyName: '相关企业名称',
 *     topicType: '1',//1.服务咨询 2.服务交易 3.企业咨询
 *     tradeNumber: '1',
 *     serviceNumber: '1',
 *     createTimeFrom: '2017-01-01',
 *     createTimeTo: '2017-01-01',
 * }
 */
module.exports = {
    '/api/adminConversation/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        conversationId: '1',
                        companyId1: '1',
                        companyId2: '2',
                        companyNumber1: 'COM001',
                        companyNumber2: 'COM002',
                        companyName1: 'QY001',
                        companyName2: 'QY002',
                        tradeId: '',
                        tradeNumber: '',
                        serviceId: '1',
                        serviceNumber: 'FW001',
                        topicType: '1',
                        createTime: '2017-01-01 02:02:02',
                    }, {
                        conversationId: '2',
                        companyId1: '1',
                        companyId2: '3',
                        companyNumber1: 'COM001',
                        companyNumber2: 'COM003',
                        companyName1: 'QY001',
                        companyName2: 'QY003',
                        tradeId: '2',
                        tradeNumber: 'JY002',
                        serviceId: '',
                        serviceNumber: '',
                        topicType: '2',
                        createTime: '2017-01-02 02:02:02',
                    }, {
                        conversationId: '3',
                        companyId1: '3',
                        companyId2: '2',
                        companyNumber1: 'COM003',
                        companyNumber2: 'COM002',
                        companyName1: 'QY003',
                        companyName2: 'QY002',
                        tradeId: '',
                        tradeNumber: '',
                        serviceId: '1',
                        serviceNumber: 'FW001',
                        topicType: '1',
                        createTime: '2017-01-01 02:02:02',
                    }, ]
                }
            }
        }
    }
};
