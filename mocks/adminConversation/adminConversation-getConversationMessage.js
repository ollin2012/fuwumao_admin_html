/**
 * mock for 后台会话管理-会话消息一览
 * @type {Object}
 *
 * params: {}
 */
module.exports = {
    '/api/adminConversation/getConversationMessage': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	id: '1',
                    	sendUserId: '1',
                    	sendUserName: '发送者一',
                    	sendTime: '2017-01-01 02:02:02',
                    	contentType: '1',
                    	content: '文本内容一',
                    	fileId: '',
                    	filePath: '',
                    	fileName: '', // TODO api式样书中目前没有
                    	fileType: '', // TODO api式样书中目前没有
                        markUserId: '99',
                        markUserName: '接收者一' // TODO api式样书中目前没有
                    }, {
                    	id: '2',
                    	sendUserId: '1',
                    	sendUserName: '发送者二',
                    	sendTime: '2017-01-01 02:02:02',
                    	contentType: '2',
                    	content: '内容二',
                    	fileId: '1',
                    	filePath: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/2b68ae3f-c590-45e4-bfe5-10ba44c67267.jpg',
                    	fileName: '交易贴图',
                    	fileType: '.jpg',
                        markUserId: '99',
                        markUserName: '接收者一'
                    }, {
                    	id: '3',
                    	sendUserId: '1',
                    	sendUserName: '发送者一',
                    	sendTime: '2017-01-01 02:02:02',
                    	contentType: '3',
                    	content: '内容三',
                    	fileId: '2',
                    	filePath: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/2b68ae3f-c590-45e4-bfe5-10ba44c67267.docx',
                    	fileName: '交易合同',
                    	fileType: '.docx',
                        markUserId: '99',
                        markUserName: '接收者一'
                    }, {
                    	id: '4',
                    	sendUserId: '1',
                    	sendUserName: '发送者四',
                    	sendTime: '2017-01-01 02:02:02',
                    	contentType: '1',
                    	content: '文本内容四',
                    	fileId: '',
                    	filePath: '',
                    	fileName: '',
                    	fileType: '',
                        markUserId: '99',
                        markUserName: '接收者一'
                    }, {
                    	id: '5',
                    	sendUserId: '1',
                    	sendUserName: '发送者五',
                    	sendTime: '2017-01-01 02:02:02',
                    	contentType: '1',
                    	content: '文本内容五',
                    	fileId: '',
                    	filePath: '',
                    	fileName: '',
                    	fileType: '',
                        markUserId: '99',
                        markUserName: '接收者一'
                    }]
                }
            }
        }
    }
};
