/**
 * mock for 聊天记录详情
 * @type {Object}
 *
 * params:
 * {conversationId: "1"}
 */
module.exports = {
    '/api/adminConversation/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    conversationId: '1',
                    title: '聊天记录一',
                    topicType: '1',//1.服务咨询 2.服务交易 3.企业咨询
                    topicTypeId: '1',
                    createTime: '2017-01-01 02:02:02',
                    conversationUser: [{
                        userId: '1',
                        userName: '用户一',
                        companyId: '1',
                        companyName: '企业一',
                        createTime: '2017-01-01 02:02:02'
                    }, {
                        userId: '2',
                        userName: '用户二',
                        companyId: '2',
                        companyName: '企业二',
                        createTime: '2017-01-01 02:02:02'
                    }]
                }
            }
        }
    }
};
