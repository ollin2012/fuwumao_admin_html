/**
 * mock for 平台规则设定新增
 * @type {Object}
 */
module.exports = {
    '/api/commissionSetting/getList': {
        POST: {
            data: {
                resultCd: 1,
                "results": {
                    "current":{
                        "settingStartDate": "2016-11-11 22:22:33",
                        "commissionPer": "10"
                    },
                    "reserve":{
                        "settingStartDate": "2017-11-11 22:22:33",
                        "commissionPer": "20"
                    }
                },
            }
        }
    }
};
