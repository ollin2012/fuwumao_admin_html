/**
 * mock for 放款列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/loan/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	tradeNumber: 'JY00000000001',
                    	tradeID: 1,
                    	tradeName: '服务猫交易一',
                    	agencyID: 1,
                    	agencyName: '服务商一',
                    	id: 1,
                    	stageName: '阶段一',
                    	stagePrice: '50000',
                    	currencyType: 'RMB',
                    	stageStatus: '50',
                    	platformSettleTime: '2017-01-01 09:30:13',
                    }, {
                    	tradeNumber: 'JY00000000001',
                    	tradeID: 1,
                    	tradeName: '服务猫交易一',
                    	agencyID: 1,
                    	agencyName: '服务商一',
                    	id: 2,
                    	stageName: '阶段二',
                    	stagePrice: '250000',
                    	currencyType: 'RMB',
                    	stageStatus: '40',
                    	platformSettleTime: '2017-01-02 09:30:13',
                    }, {
                    	tradeNumber: 'JY00000000002',
                    	tradeID: 2,
                    	tradeName: '服务猫交易二',
                    	agencyID: 2,
                    	agencyName: '服务商二',
                    	id: 3,
                    	stageName: '阶段一',
                    	stagePrice: '80000',
                    	currencyType: 'RMB',
                    	stageStatus: '40',
                    	platformSettleTime: '2017-01-01 10:30:13',
                    }, {
                    	tradeNumber: 'JY00000000003',
                    	tradeID: 3,
                    	tradeName: '服务猫交易三',
                    	agencyID: 3,
                    	agencyName: '服务商三',
                    	id: 4,
                    	stageName: '阶段一',
                    	stagePrice: '20000',
                    	currencyType: 'RMB',
                    	stageStatus: '50',
                    	platformSettleTime: '2017-01-01 11:30:13',
                    }, {
                    	tradeNumber: 'JY00000000003',
                    	tradeID: 3,
                    	tradeName: '服务猫交易三',
                    	agencyID: 3,
                    	agencyName: '服务商三',
                    	id: 5,
                    	stageName: '阶段二',
                    	stagePrice: '180000',
                    	currencyType: 'RMB',
                    	stageStatus: '40',
                    	platformSettleTime: '2017-01-02 11:30:13',
                    }, ]
                }
            }
        }
    }
};
