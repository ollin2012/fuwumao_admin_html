/**
 * mock for 放款详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/loan/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                	tradeNumber: 'JY00000000001',
                	tradeID: 1,
                	tradeName: '服务猫交易一',
                	agencyID: 1,
                	agencyName: '服务商一',
                	id: 2,
                	stageName: '阶段二',
                	stagePrice: '250000',
                	currencyType: 'RMB',
                	stageStatus: '40',
                	platformSettleTime: '2017-01-02 09:30:13',
                	updateTime: '2017-01-02 13:00:00',
                }
            }
        }
    }
};
