/**
 * mock for 平台规则设定删除
 * @type {Object}
 */
module.exports = {
    '/api/depositSetting/getList': {
        POST: {
            data: {
                resultCd: 1,
                "results": {
                    "current": [
                        {
                            "settingStartDate": "2016-11-11 22:22:33.456",
                            'depositAmount': "8888.88",
                            'currencyType': "RMB"
                        },
                        {
                            "settingStartDate": "2016-11-11 22:22:33.456",
                            'depositAmount': "6666.6",
                            'currencyType': "USD"
                        }
                    ],
                    "reserve": [
                        {
                            "settingStartDate": "2017-11-11 22:22:33",
                            'depositAmount': "8888.88",
                            'currencyType': "RMB"
                        },
                        {
                            "settingStartDate": "2017-11-11 22:22:33",
                            'depositAmount': "777.7",
                            'currencyType': "USD"
                        }
                    ]
                }
            }
        }
    }
};
