/**
 * mock for 平台规则设定变更
 * @type {Object}
 */
module.exports = {
    '/api/depositSetting/update': {
        POST: {
            data: {
                resultCd: 1,
                status: "200",
                errorType: "",
                errorMsg: "提交失败"
            }
        }
    }
};

