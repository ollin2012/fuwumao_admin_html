/**
 * mock for 退款变更
 * @type {Object}
 */
module.exports = {
    '/api/refund/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
