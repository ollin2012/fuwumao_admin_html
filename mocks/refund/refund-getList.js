/**
 * mock for 退款列表
 * @type {Object}
 *
 * params:
 * {
 *     page: "1",
 *     count: "10",
 *     name: "t",
 * }
 */
module.exports = {
    '/api/refund/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: 1,
                        applyCompanyId: 1,
                        companyNumber: 'QY00000000001',
                        company: '服务猫管理有限公司一',
                        refundType: '1',
                        verifySubmitTime: '2017-01-01 09:30:13',
                        refundAmount: '132800',
                        currencyType: 'RMB',
                        refundStatus: '0',
                        refundDate: null,
                        verifyResultFlg: '0',
                        updateTime: '2017-01-02 09:30:13',
                    }, {
                        id: 2,
                        applyCompanyId: 2,
                        companyNumber: 'QY00000000002',
                        company: '服务猫管理有限公司二',
                        capitalFlowId: 2,
                        capitalFloNumber: 'CFN0000000002',
                        refundType: '2',
                        tradeId: null,
                        tradeNumber: null,
                        tradeStatus: null,
                        verifySubmitTime: '2017-01-01 09:30:13',
                        refundAmount: '12750',
                        currencyType: 'RMB',
                        refundStatus: '0',
                        verifyResultFlg: '1',
                        updateTime: '2017-01-02 09:30:13',
                    }, {
                        id: 3,
                        applyCompanyId: 3,
                        companyNumber: 'QY00000000003',
                        company: '服务猫管理有限公司三',
                        capitalFlowId: 3,
                        capitalFloNumber: 'CFN0000000003',
                        refundType: '1',
                        tradeId: 3,
                        tradeNumber: 'JY00000000003',
                        tradeStatus: '99',
                        verifySubmitTime: '2017-01-01 09:30:13',
                        refundAmount: '25750',
                        currencyType: 'RMB',
                        refundStatus: '0',
                        refundDate: null,
                        verifyResultFlg: '0',
                        updateTime: '2017-01-02 09:30:13',
                    }, {
                        id: 4,
                        applyCompanyId: 4,
                        companyNumber: 'QY00000000004',
                        company: '服务猫管理有限公司四',
                        capitalFlowId: 4,
                        capitalFloNumber: 'CFN0000000004',
                        refundType: '1',
                        tradeId: 4,
                        tradeNumber: 'JY00000000004',
                        tradeStatus: '99',
                        verifySubmitTime: '2017-01-01 09:30:13',
                        refundAmount: '257050',
                        currencyType: 'RMB',
                        refundStatus: '9',
                        refundDate: null,
                        verifyResultFlg: '1',
                        updateTime: '2017-01-02 09:30:13',
                    }, {
                        id: 5,
                        applyCompanyId: 5,
                        companyNumber: 'QY00000000005',
                        company: '服务猫管理有限公司五',
                        capitalFlowId: 5,
                        capitalFloNumber: 'CFN0000000005',
                        refundType: '2',
                        tradeId: null,
                        tradeNumber: null,
                        tradeStatus: null,
                        verifySubmitTime: '2017-01-01 09:30:13',
                        refundAmount: '180000',
                        currencyType: 'RMB',
                        refundStatus: '1',
                        refundDate: '2017-01-02 09:30:13',
                        verifyResultFlg: '0',
                        updateTime: '2017-01-02 09:30:13',
                    }, ]
                }
            }
        }
    }
};
