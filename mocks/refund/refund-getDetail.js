/**
 * mock for 退款详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/refund/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: 1,
                    applyCompanyId: 1,
                    companyNumber: 'QY00000000001',
                    company: '服务猫管理有限公司一',
                    capitalFlowId: 1,
                    capitalFloNumber: 'CFN0000000001',
                    refundType: '1',
                    tradeId: 1,
                    tradeNumber: 'JY00000000001',
                    tradeStatus: '99',
                    verifySubmitTime: '2017-01-01 09:30:13',
                    refundAmount: '132800',
                    currencyType: 'RMB',
                    refundStatus: '0',
                    verifyResultFlg: '1',
                    refundDate: null,
                    refundReason: '退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由退款理由',
                    refundStatusDetail: '审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述审核结果描述',
                    updateTime: '2017-01-02 09:30:13',
                }
            }
        }
    }
};
