/**
 * mock for AGY007_服务商情报取得
 * @type {Object}
 *
 * params:
 * {companyId: '1'}
 */
module.exports = {
    '/api/agency/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    companyId: '1',
                    agencyBannerFilePath: 'http://xxxx.com/1313139527.jpg',
                    agencyStatus: '0',
                    serviceScore: '3',
                    tradeCount: '27',
                    becomingTime: '2016-12-01 09:00:00',
                    depositPayStatus: '1',
                    agencyAreaList: [{
                        area1: '10000',
                        area2: '10800',
                        area3: '10801',
                        areaOther: 'QQ'
                    }, {
                        area1: '10000',
                        area2: '10800',
                        area3: '10802',
                        areaOther: 'PP'
                    },{
                        area1: '10000',
                        area2: '10800',
                        area3: '10803',
                        areaOther: 'MM'
                    }],
                    agencyIndustryList: [{
                        industry1: '100',
                        industry2: '101',
                        industryOther: 'AA'
                    }, {
                        industry1: '100',
                        industry2: '102',
                        industryOther: 'ZZ'
                    },{
                        industry1: '100',
                        industry2: '103',
                        industryOther: 'YY'
                    }],
                    updateTime: '2016-12-12 18:00:13'
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};