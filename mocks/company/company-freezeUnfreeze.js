/**
 * mock for 企业详情
 * @type {Object}
 *
 * params:
 * {'companyId': '1', 
 *  'frozenFlg':'冻结/解冻区分',
 *  'frozenReason':'冻结/解冻理由', 
 *  'updateTime':'排他check用'
 * }
 */
module.exports = {
    '/api/company/freezeUnfreeze': {
        POST: {
            data: {
                resultCd: 1,
                // results: {
                //     companyId: '1'
                // },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};