/**
 * mock for 企业名称取得
 * @type {Object}
 *
 */
module.exports = {
    '/api/company/getName': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                	companyName: '受邀企业二',
                    companyId: '1'
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};
