/**
 * mock for 企业变更
 * @type {Object}
 */
module.exports = {
    '/api/company/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
