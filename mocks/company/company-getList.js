/**
 * mock for 企业列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/company/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	id: "1",
                        companyLogoFilePath: "images/tmp/img.jpg",
                        companyName: "企业一",
                        companyArea1: "亚洲",
                        companyArea2: "中国",
                        companyArea3: "华东",
                        industry1: "策划",
                        industry2: "软件",
                        companyStatus: "0",
                        companyInstitute: "1",
                        agencyFlg: "0",
                        registerTime: new Date(),
                        verifySubmitTime: new Date(),
                        serviceCount: "0",
                        tradeCount: "7",
                        companyCertFlg: '0',
                        agencyCertFlg: '0',
                        annualFeeCertFlg: '0',
                        preferredLanguage: "1"
                    }, {
                    	id: "2",
                        companyLogoFilePath: "images/tmp/img.jpg",
                        companyName: "企业二",
                        companyArea1: "亚洲",
                        companyArea2: "中国",
                        companyArea3: "",
                        industry1: "策划",
                        industry2: "",
                        companyStatus: "1",
                        companyInstitute: "2",
                        agencyFlg: "1",
                        registerTime: "2016-12-03 09:00:00",
                        verifySubmitTime: "2016-12-04",
                        serviceCount: "0",
                        tradeCount: "0",
                        companyCertFlg: '1',
                        agencyCertFlg: '0',
                        annualFeeCertFlg: '0',
                        preferredLanguage: "1"
                    }, {
                    	id: "3",
                        companyLogoFilePath: "images/tmp/img.jpg",
                        companyName: "企业三",
                        companyArea1: "",
                        companyArea2: "中国",
                        companyArea3: "华东",
                        industry1: "",
                        industry2: "软件",
                        companyStatus: "8",
                        companyInstitute: "0",
                        agencyFlg: "1",
                        registerTime: "2016-12-05 09:00:00",
                        verifySubmitTime: "2016-12-06",
                        serviceCount: "2",
                        tradeCount: "13",
                        companyCertFlg: '0',
                        agencyCertFlg: '1', //服务商材料认证
                        annualFeeCertFlg: '0',
                        preferredLanguage: "1"
                    }, {
                    	id: "4",
                        companyLogoFilePath: "images/tmp/img.jpg",
                        companyName: "企业四",
                        companyArea1: "亚洲",
                        companyArea2: "",
                        companyArea3: "",
                        industry1: "教育",
                        industry2: "软件",
                        companyStatus: "9",
                        companyInstitute: "1",
                        agencyFlg: "0",
                        registerTime: "2016-12-07 09:00:00",
                        verifySubmitTime: "2016-12-08",
                        serviceCount: "3",
                        tradeCount: "0",
                        companyCertFlg: '0',
                        agencyCertFlg: '2', //服务商入驻保证金认证
                        annualFeeCertFlg: '0',
                        preferredLanguage: "1"
                    }, {
                    	id: "5",
                        companyLogoFilePath: "images/tmp/img.jpg",
                        companyName: "企业五",
                        companyArea1: "欧洲",
                        companyArea2: "德国",
                        companyArea3: "墨尔本",
                        industry1: "教育",
                        industry2: "建材",
                        companyStatus: "1",
                        companyInstitute: "0",
                        agencyFlg: "1",
                        registerTime: "2016-12-09 09:00:00",
                        verifySubmitTime: "2016-12-10",
                        serviceCount: "0",
                        tradeCount: "0",
                        companyCertFlg: '0',
                        agencyCertFlg: '3', //成为服务商认证
                        annualFeeCertFlg: '0',
                        preferredLanguage: "1"
                    }, {
                    	id: "6",
                        companyLogoFilePath: "images/tmp/img.jpg",
                        companyName: "企业六",
                        companyArea1: "欧洲",
                        companyArea2: "德国",
                        companyArea3: "墨尔本",
                        industry1: "教育",
                        industry2: "建材",
                        companyStatus: "1",
                        companyInstitute: "0",
                        agencyFlg: "1",
                        registerTime: "2016-12-09 09:00:00",
                        verifySubmitTime: "2016-12-10",
                        serviceCount: "0",
                        tradeCount: "0",
                        companyCertFlg: '0',
                        agencyCertFlg: '0',
                        annualFeeCertFlg: '1',
                        preferredLanguage: "1"
                    }]
                }
            }
        }
    }
};
