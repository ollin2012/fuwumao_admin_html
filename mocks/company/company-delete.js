/**
 * mock for 企业删除
 * @type {Object}
 */
module.exports = {
    '/api/company/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
