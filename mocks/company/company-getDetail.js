/**
 * mock for 企业详情
 * @type {Object}
 *
 * params:
 * {companyId: '1', 'companyLanguage':'企业语言区分'}
 */
module.exports = {
    '/api/company/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: '1',
                    companyNumber: 'ABC0000001',
                    companyLogoFilePath: 'images/tmp/u225.jpg',
                    companyName: 'XXXXXXXXXXXXXXX企业名称',
                    companyStatus: '1',
                    companyScore: '4',
                    agencyFlg: '0',
                    contact: '张三',
                    contactSex: '0',
                    contactsPhoneCountry: '+86',
                    contactsPhone: '13800000001',
                    contactsJob: '项目经理',
                    contactsMail: 'zhangsan@test.com',
                    companyInstitute: '1',
                    currencyType: 'RMB',
                    registerTime: '2016-12-01 08:00:00',
                    companyInviteId: '2',
                    preferredLanguage: 'cn',
                    companyIntro: '企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介企业简介',
                    companyArea1: '10000',
                    companyArea2: '10800',
                    companyArea3: '10801',
                    companyAreaOther: '三里屯',
                    industry1: '100',
                    industry2: '101',
                    industryOther: 'java开发',
                    updateTime: '2016-12-01 09:00:00',
                    languageList: [{
                        language: 'cn',
                        languageName: '中文'
                    }, {
                        language: 'en',
                        languageName: '英文'
                    }]
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};