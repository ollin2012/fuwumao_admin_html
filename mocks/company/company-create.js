/**
 * mock for 企业新增
 * @type {Object}
 *
 * params:
 * {
 * 	name: "test1", 
 * }
 */
module.exports = {
    '/api/company/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
