/**
 * mock for 成员企业佣金一览导出
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/companyCommission/export': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        name: '佣金发生企业',
                        number: 'QY00001',
                        agencyFlg: '0',
                        tradeName: '交易名称',
                        tradeNumber: 'JY00001',
                        tradeStageName: '阶段名称',
                        capitalFlowNumber: '',
                        commissionAmount: '135000',
                        currencyType: 'RMB',
                        happenTime: '2016-12-31 09:02:01',
                        commissionDiv: '1',
                        payFlg: '0',
                        payTime: '',
                        payedCompanyName: '佣金支付企业',
                        payedCompanyNumber: 'QY00091'
                    }, {
                        name: '佣金发生企业',
                        number: 'QY00002',
                        agencyFlg: '1',
                        tradeName: '交易名称',
                        tradeNumber: 'JY00002',
                        tradeStageName: '阶段名称',
                        capitalFlowNumber: 'ZJLS002',
                        commissionAmount: '85000',
                        currencyType: 'USD',
                        happenTime: '2016-12-31 09:02:01',
                        commissionDiv: '2',
                        payFlg: '1',
                        payTime: '2017-01-02 09:02:01',
                        payedCompanyName: '佣金支付企业',
                        payedCompanyNumber: 'QY00092'
                    }]
                }
            }
        }
    }
};
