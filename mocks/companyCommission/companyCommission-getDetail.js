/**
 * mock for 成员企业佣金一览
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/companyCommission/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	id: '1',
                        name: '佣金发生企业',
                        number: 'QY00001',
                        agencyFlg: '0',
                        tradeId: '1',
                        tradeName: '交易名称',
                        tradeNumber: 'JY00001',
                        tradeStageId: '11',
                        tradeStageName: '阶段名称',
                        capitalFlowNumber: '',
                        commissionAmount: '135000',
                        currencyType: 'RMB',
                        happenTime: '2016-12-31 09:02:01',
                        commissionDiv: '1',
                        payFlg: '0',
                        payTime: '',
                        payedCompanyId: '91',
                        payedCompanyName: '佣金支付企业',
                        payedCompanyNumber: 'QY00091'
                    }, {
                    	id: '2',
                        name: '佣金发生企业',
                        number: 'QY00002',
                        agencyFlg: '1',
                        tradeId: '2',
                        tradeName: '交易名称',
                        tradeNumber: 'JY00002',
                        tradeStageId: '22',
                        tradeStageName: '阶段名称',
                        capitalFlowNumber: 'ZJLS002',
                        commissionAmount: '85000',
                        currencyType: 'USD',
                        happenTime: '2016-12-31 09:02:01',
                        commissionDiv: '2',
                        payFlg: '1',
                        payTime: '2017-01-02 09:02:01',
                        payedCompanyId: '92',
                        payedCompanyName: '佣金支付企业',
                        payedCompanyNumber: 'QY00092'
                    }]
                }
            }
        }
    }
};
