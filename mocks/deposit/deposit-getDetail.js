/**
 * mock for 保证金详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/agencyDeposit/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    companyId: 1,
                    agencyName: '服务猫管理有限公司一',
                    agencyCompanyNumber: 'QY00000000001',
                    capitalFlowId: 1,
                    capitalFlowNumber: 'CFN0000000001',
                    tradeId: 1,
                    tradeNumber: 'JY000000000001',
                    tradeStatus: '80',
                    depositType: 1,
                    depositAmount: '13850000',
                    currencyType: 'RMB',
                    capitalChangeType: '1',
                    frozenFlg: '1',
                    depositChangeTime: new Date(),
                    updateTime: new Date(),
                }
            }
        }
    }
};
