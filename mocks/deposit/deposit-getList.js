/**
 * mock for 保证金列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/agencyDeposit/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	agencyDepositId: 1,
                        companyId: 1,
                        agencyName: '服务猫管理有限公司一',
                        agencyCompanyNumber: 'QY00000000001',
                        tradId: null,
                        tradNumber: null,
                        depositType: 1,
                        depositAmount: '10000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowId: 1,
                        capitalFlowNumber: 'CFN0000000001',
                        capitalChangeType: '1',
                        frozenFlg: '1',
                    }, {
                    	agencyDepositId: 2,
                        companyId: 1,
                        agencyName: '服务猫管理有限公司一',
                        agencyCompanyNumber: 'QY00000000001',
                        tradId: 2,
                        tradNumber: 'JY00000000001',
                        depositType: 2,
                        depositAmount: '1300000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowId: 5,
                        capitalFlowNumber: 'CFN0000000005',
                        capitalChangeType: '1',
                        frozenFlg: '1',
                    }, {
                    	agencyDepositId: 3,
                        companyId: 1,
                        agencyName: '服务猫管理有限公司二',
                        agencyCompanyNumber: 'QY00000000002',
                        tradId: 3,
                        tradNumber: 'JY00000000002',
                        depositType: 3,
                        depositAmount: '1300000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowId: 2,
                        capitalFlowNumber: 'CFN0000000002',
                        capitalChangeType: '2',
                        frozenFlg: '2',
                    }, {
                    	agencyDepositId: 4,
                        companyId: 1,
                        agencyName: '服务猫管理有限公司三',
                        agencyCompanyNumber: 'QY00000000003',
                        tradId: 4,
                        tradNumber: 'JY00000000003',
                        depositType: 2,
                        depositAmount: '1300000000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowId: 3,
                        capitalFlowNumber: 'CFN0000000003',
                        capitalChangeType: '2',
                        frozenFlg: '2',
                    }, {
                    	agencyDepositId: 5,
                        companyId: 1,
                        agencyName: '服务猫管理有限公司四',
                        agencyCompanyNumber: 'QY00000000004',
                        tradId: 5,
                        tradNumber: 'JY00000000004',
                        depositType: 2,
                        depositAmount: '1300000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowId: 4,
                        capitalFlowNumber: 'CFN0000000004',
                        capitalChangeType: '3',
                        frozenFlg: '2',
                    }, ]
                }
            }
        }
    }
};
