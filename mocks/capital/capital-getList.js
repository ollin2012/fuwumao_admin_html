/**
 * mock for 收支列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/capitalFlow/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                    	id: '1',
                    	capitalFlowNumber: 'CFN0000000001',
                    	payerCompanyId: '1',
                    	payerCompanyNumber: 'QY00001',
                    	payerCompanyName: '付款方企业一',
                    	payeeCompanyId: '11',
                    	payeeCompanyNumber: 'QY00011',
                    	payeeCompanyName: '收款方企业一',
                    	tradeId: '1',
                    	tradeNumber: 'JY00001',
                    	amount: '37500',
                    	currencyType: 'RMB',
                    	capitalType: '10',
                    	bankCode: '08600001',
                    	status: '1',
                    	updateTime: '2017-01-04 11:22:33'
                    }, {
                    	id: '2',
                    	capitalFlowNumber: 'CFN0000000002',
                    	payerCompanyId: '2',
                    	payerCompanyNumber: 'QY00002',
                    	payerCompanyName: '付款方企业二',
                    	payeeCompanyId: '22',
                    	payeeCompanyNumber: 'QY00022',
                    	payeeCompanyName: '收款方企业二',
                    	tradeId: '2',
                    	tradeNumber: 'JY00002',
                    	amount: '180000',
                    	currencyType: 'USD',
                    	capitalType: '20',
                    	bankCode: '85200001',
                    	status: '2',
                    	updateTime: '2017-01-04 11:22:33'
                    }, {
                    	id: '3',
                    	capitalFlowNumber: 'CFN0000000003',
                    	payerCompanyId: '3',
                    	payerCompanyNumber: 'QY00003',
                    	payerCompanyName: '付款方企业三',
                    	payeeCompanyId: '33',
                    	payeeCompanyNumber: 'QY00033',
                    	payeeCompanyName: '收款方企业三',
                    	tradeId: '3',
                    	tradeNumber: 'JY00003',
                    	amount: '90000',
                    	currencyType: 'RMB',
                    	capitalType: '30',
                    	bankCode: '85200001',
                    	status: '11',
                    	updateTime: '2017-01-04 11:22:33'
                    }, {
                    	id: '4',
                    	capitalFlowNumber: 'CFN0000000004',
                    	payerCompanyId: '4',
                    	payerCompanyNumber: 'QY00004',
                    	payerCompanyName: '付款方企业四',
                    	payeeCompanyId: '44',
                    	payeeCompanyNumber: 'QY00044',
                    	payeeCompanyName: '收款方企业四',
                    	tradeId: '4',
                    	tradeNumber: 'JY00004',
                    	amount: '1500000',
                    	currencyType: 'RMB',
                    	capitalType: '40',
                    	bankCode: '08600001',
                    	status: '5',
                    	updateTime: '2017-01-04 11:22:33'
                    }]
                }
            }
        }
    }
};
