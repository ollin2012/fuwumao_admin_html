/**
 * mock for 收支详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/capitalFlow/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                	capitalFlowNumber: 'CFN0000000002',
                	payerCompanyId: '1',
                	payerCompanyNumber: 'QY00001',
                	payerCompanyName: '付款方企业一',
                	payeeCompanyId: '11',
                	payeeCompanyNumber: 'QY00011',
                	payeeCompanyName: '收款方企业一',
                	status: '1',
                	updateTime: '2017-01-01 00:00:00',
                	capitalType: '10',
                	amount: '37500',
                	currencyType: 'RMB',
                	subAccountNo: '62226001100099999',
                	bankCode: '08600001',
                	tradeId: '1',
                    tradeNumber: 'JY00001',
                    payDate : '2017-01-01 00:00:00',
                    receiveDate  : '2016-01-01 00:00:00',
                    failDetail   : '失败了',
                	tranAmount    : '123456'
                }
            }
        }
    }
};
