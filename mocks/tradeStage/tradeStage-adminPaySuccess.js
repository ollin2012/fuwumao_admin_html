/**
 * mock for 交易款平台支付成功
 * @type {Object}
 *
 * params: {}
 */
module.exports = {
    '/api/tradeStage/adminPaySuccess': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
