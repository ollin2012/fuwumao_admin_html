/**
 * mock for 交易阶段预付款支付成功处理
 * @type {Object}
 *
 * params: {}
 */
module.exports = {
    '/api/tradeStage/prepaySuccess': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
