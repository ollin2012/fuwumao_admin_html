/**
 * mock for 放款处理
 * @type {Object}
 *
 * params: 
 * {
 *   id: 1, //交易阶段ID
 *   stagePrice: '130000'
 * }
 */
module.exports = {
    '/api/tradeStage/adminPay': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
