/**
 * mock for 广告删除
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/advert/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
