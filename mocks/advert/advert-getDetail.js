/**
 * mock for 广告详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/advert/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: '1',
                    customer: '广告客户1',
                    title: '广告标题1',
                    posCode: '2',//api取名不准确，该值是posId
                    language: 'cn',
                    weight: '30',
                    currencyType: 'RMB',
                    contractAmount: '10005.05',
                    links: 'http://www.baidu.com',
                    openStartDate: '2016-11-01 09:00:00.000',
                    openEndDate: '2017-03-01 09:00:00.000',
                    openStatus: '1',
                    requirement: '在投放期间全天投放',
                    updateTime: '2017-01-01 09:00:00.000',
                    adverImage: [{
                        id: '231',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/backend_advert/4b42ee98-c1bb-4460-bed0-97a7f6c37b2d.gif',
                    }, {
                        id: '229',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/backend_advert/cfb7d463-e4b8-414f-9ebd-d034fde10afc.jpg',
                    }],
                }
            }
        }
    }
};
