/**
 * mock for 广告列表
 * @type {Object}
 *
 * params: 
 * {
    page: "1",
    count: "10",
    //广告客户名称
    customer:'',
    //广告位置
    posCode:'1',
    //语言区分
    language:'1',
    //广告费币种
    currencyType:'RMB',
    //广告费
    contractAmountFrom:10,
    contractAmountTo:1000,
    //广告标题
    title:'',
    //广告公开状态 0: 未公开1: 公开
    openStatus:'1',
    //残余日数
    remainingOpendaysFrom:5,
    remainingOpendaysTo:10,
    //公开期间
    openDateFrom:'date',
    openDateTo:'date',
    //广告公开状态 0: 未过期1: 过期
    expireStatus:'0',
    }
 */
module.exports = {
    '/api/advert/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: 1,
                        customer: '广告客户1',
                        title: '广告标题1',
                        posCode: '1',
                        language: 'cn',
                        weight: 0.1,
                        currencyType: 'RMB',
                        contractAmount: 10000,
                        openStartDate: new Date(),
                        openEndDate: '2017-02-21 07:01:46.001',
                        openStatus: '1',
                        updateTime: '2017-01-01 09:00:00.002',
                    }, {
                        id: 2,
                        customer: '广告客户2',
                        title: '广告标题2',
                        posCode: '1',
                        language: 'cn',
                        weight: 0.2,
                        currencyType: 'RMB',
                        contractAmount: 10000,
                        openStartDate: '2016-11-01 09:00:00.001',
                        openEndDate: '2016-12-01 09:00:00.002',
                        openStatus: '1',
                        updateTime: '2017-01-01 09:00:00',
                    }, {
                        id: 3,
                        customer: '广告客户3',
                        title: '广告标题3',
                        posCode: '2',
                        language: 'en',
                        weight: 0.3,
                        currencyType: 'RMB',
                        contractAmount: 10000,
                        openStartDate: '2016-11-01 09:00:00',
                        openEndDate: '2017-02-01 09:00:00',
                        openStatus: '1',
                        updateTime: '2017-01-01 09:00:00',
                    }, {
                        id: 4,
                        customer: '广告客户4',
                        title: '广告标题4',
                        posCode: '3',
                        language: 'cn',
                        weight: 0.3,
                        currencyType: 'RMB',
                        contractAmount: 10000,
                        openStartDate: '2016-11-01 09:00:00',
                        openEndDate: '2017-01-08 09:00:00',
                        openStatus: '1',
                        updateTime: '2017-01-01 09:00:00',
                    }, {
                        id: 5,
                        customer: '广告客户5',
                        title: '广告标题5',
                        posCode: '2',
                        language: 'en',
                        weight: 0.3,
                        currencyType: 'RMB',
                        contractAmount: 10000,
                        openStartDate: '2016-11-01 09:00:00',
                        openEndDate: '2016-12-01 09:00:00',
                        openStatus: '1',
                        updateTime: '2017-01-01 09:00:00',
                    }, ]
                }
            }
        }
    }
};
