/**
 * mock for 广告变更
 * @type {Object}
 * params:
 * {
 *	advertLanguage: 'cn',
 *  id: '1',
 *  posCode: '1',
 *  currencyType: 'RMB',
 * 	weight: 0.3,
 *  contractAmount: 10000,
 *  title: '广告标题1',
 *  links: 'http://www.baidu.com',
 * 	customer: '广告客户1',
 *  requirement: '在投放期间全天投放',
 *  openStartDate: '2016-11-01 09:00:00',
 *  openEndDate: '2017-03-01 09:00:00',
 *  openStatus: '1',
 *  updateTime: '2017-03-01 09:00:00',
 *  adverImages: [1,2,3],
 * }
 */
module.exports = {
    '/api/advert/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
