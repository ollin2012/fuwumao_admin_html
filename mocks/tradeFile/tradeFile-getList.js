/**
 * mock for 取得交易文件一览
 * @type {Object}
 *
 */
module.exports = {
    '/api/tradeFile/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 26,
                    list: [{
                        id: '1',
                        type: '1',
                        name: '文件名1.ai',
                        size: '6423043204',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业1',
                        notes: '操作手册'
                    }, {
                        id: '2',
                        type: '1',
                        name: '文件名2.psd',
                        size: '631231313123',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '3',
                        type: '1',
                        name: '文件名3.jpg',
                        size: '122132131313',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '4',
                        type: '1',
                        name: '文件名2',
                        size: '24012313',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '5',
                        type: '1',
                        name: '文件名2',
                        size: '12321',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '6',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '7',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '8',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '9',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '10',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }, {
                        id: '11',
                        type: '1',
                        name: '文件名2',
                        size: '10241024',
                        path: 'https://bolstra-public.oss-cn-hongkong.aliyuncs.com/company/1/company_logo/b9683d41-4f53-4c22-a148-039316788a47.jpg',
                        uploadTime: '2016-12-12 12:12:12.123',
                        uploadCompanyName: '企业2',
                        notes: '素材'
                    }]
                }
            }
        }
    }
};
