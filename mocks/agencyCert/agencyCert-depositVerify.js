/**
 * mock for 服务商入驻保证金认证审核
 * @type {Object}
 */
module.exports = {
    '/api/agencyCert/depositVerify': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
