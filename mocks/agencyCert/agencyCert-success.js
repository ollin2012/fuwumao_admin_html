/**
 * mock for 成为服务商
 * @type {Object}
 */
module.exports = {
    '/api/agencyCert/success': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
