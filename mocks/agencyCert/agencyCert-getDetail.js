/**
 * mock for AGY001_服务商认证情报取得
 * @type {Object}
 *
 * params:
 * {companyId: '1'}
 */
module.exports = {
    '/api/agencyCert/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    companyId: '1'
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};