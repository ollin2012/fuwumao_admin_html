/**
 * mock for AGY017_后台服务商认证履历一览取得
 * @type {Object}
 *
 * params:
 * {companyId: '1'}
 */
module.exports = {
    '/api/agencyCert/getList': {
        POST: {
	        default: {
	            data: {
	                resultCd: 1,
	                results: {
	                    agencyCertId: 1,
	                    verifySubmitTime: '2017-04-01 09:00:00',
	                    verifyResultFlg: '0',
	                    verifyResultDetail: '服务商审核结果描述',
	                    sysVerifyResultFlg: '0',
	                    sysVerifyResultDetail: '系统服务商审核结果描述',
	                    sysVerifyTime: '2016-12-02 08:00:59',
	                    certContentJson: '{"businessLicenseNumber": "12345678901234567890","businessCardsfileList": [{"fileId": "1071","filePath": "https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/800/company_material/a2ef7fc9-75f3-4ee1-92f0-6945d6b68130.jpg"},{"fileId": "1072","filePath": "https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/800/company_material/191e2995-f021-4b90-9b66-9d7609ecaa8b.jpg"},{"fileId": "1073","filePath": "https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/800/company_material/81d1820e-f96d-40f6-b85f-ac37c5b5daac.jpg"}],"idCardsfileList": [{"fileId": "1074","filePath": "https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/800/company_material/918a215d-3894-46da-934c-3a64d2073244.jpg"},{"fileId": "1075","filePath": "https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/800/company_material/d92b0547-0717-48f8-80bd-7fcb2f644ef5.jpg"}],"orgStruCodeNumber": "12345678901234567890","taxRegistNumber": "12345678901234567890","businessScope": "大幅度反对","companyType": "港、澳、台商投资企业","companyRegisteredAddress": "中国上海反对法地方大发","companyLegalPerson": "热饿"}',
	                    updateTime: '2017-01-01 09:00:14',
	                    verifyFile: [{
	                        fileId: '1071',
	                        fileName: '营业执照11.png',
	                        type: 'png',
	                        path: 'https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/25572/company_material/3758542d-6114-4d79-89d8-a6bec38094d3.png',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: '1072',
	                        fileName: '服务猫展望111.png',
	                        type: 'png',
	                        path: 'https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/25572/company_material/c588d955-ab92-43cd-bf71-a11e67d63896.png',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: '1073',
	                        fileName: '服务猫展望222.png',
	                        type: 'png',
	                        path: 'https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/25572/company_material/3758542d-6114-4d79-89d8-a6bec38094d3.png',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: '1074',
	                        fileName: '服务猫展望333.png',
	                        type: 'png',
	                        path: 'https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/25572/company_material/c588d955-ab92-43cd-bf71-a11e67d63896.png',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: '1075',
	                        fileName: '服务猫展望444.png',
	                        type: 'png',
	                        path: 'https://bolstra-public-stg.oss-cn-hongkong.aliyuncs.com/company/25572/company_material/c588d955-ab92-43cd-bf71-a11e67d63896.png',
	                        secretKey: '111dasdsak'
	                    }],
	                    agencyVerifyHistory:[{
	                        agencyCertId: 9,
	                        verifySubmitTime: new Date(),
	                        verifyResultFlg: '0',
	                        verifyResultDetail: '不通过001。',
	                        verifierId: 1001,
	                        verifierName: '平台001',
	                        verifieTime: new Date()
	                    },{
	                        agencyCertId: 8,
	                        verifySubmitTime: '2016-11-29 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过002。',
	                        verifierId: 1002,
	                        verifierName: '平台002',
	                        verifieTime: '2016-11-29 10:00:00'
	                    },{
	                        agencyCertId: 7,
	                        verifySubmitTime: '2016-11-28 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过003.',
	                        verifierId: 1003,
	                        verifierName: '平台003',
	                        verifieTime: '2016-11-28 10:00:00'
	                    },{
	                        agencyCertId: 6,
	                        verifySubmitTime: '2016-11-27 09:00:00',
	                        verifyResultFlg: '9',
	                        verifyResultDetail: '不通过004。',
	                        verifierId: 1003,
	                        verifierName: '平台004',
	                        verifieTime: '2016-11-27 10:00:00'
	                    },{
	                        companyCertId: 5,
	                        verifySubmitTime: '2016-11-26 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过005。',
	                        verifierId: 1004,
	                        verifierName: '平台005',
	                        verifieTime: '2016-11-26 10:00:00'
	                    }],
	                    depositPayStatus: '0',
	                    informationVerifyStatus: '0',
	                    storeAgencyFlg: '1',
	                    agencyUpdateTime : '2017-01-02 13:00:00',
	                    capitalFlowMemo: '入驻保证金备注',
	                    depositUpdateTime : '2017-01-03 13:00:00',
	                    currencyType: 'RMB',
	                    depositAmount: '13000',
	                    agencyFlg: '0'
	                },
	                errorType: '',
	                errorMsg: ''
	            }
	        }, // 服务商材料认证end
	        2: {
	            data: {
	                resultCd: 1,
	                results: {
	                    agencyCertId: 1,
	                    verifySubmitTime: '2017-04-02 09:00:00',
	                    verifyResultFlg: '1',
	                    verifyResultDetail: '服务商审核结果描述',
	                    sysVerifyResultFlg: '0',
	                    sysVerifyResultDetail: '系统服务商审核结果描述',
	                    sysVerifyTime: '2016-12-02 08:00:59',
	                    certContentJson: '{"营业执照注册号":"9999999999999999","住所":"上海市徐汇区桂平路680号","法人":"王二","公司类型":"有限责任公司","注册资本":"壹仟万圆整","实收资本":"壹仟万圆整","经营范围":"经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围","成立日期":"2016年12月01日"}',
	                    updateTime: '2017-01-01 09:00:14',
	                    verifyFile: [{
	                        fileId: 1,
	                        fileName: '营业执照11.jpg',
	                        type: 'jpg',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: 2,
	                        fileName: '公司入住服务猫展望111.pdf',
	                        type: 'pdf',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: 3,
	                        fileName: '公司入住服务猫展望222.pdf',
	                        type: 'pdf',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: 4,
	                        fileName: '公司入住服务猫展望333.pdf',
	                        type: 'pdf',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }],
	                    agencyVerifyHistory:[{
	                        agencyCertId: 9,
	                        verifySubmitTime: new Date(),
	                        verifyResultFlg: '0',
	                        verifyResultDetail: '不通过001。',
	                        verifierId: 1001,
	                        verifierName: '平台001',
	                        verifieTime: new Date()
	                    },{
	                        agencyCertId: 8,
	                        verifySubmitTime: '2016-11-29 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过002。',
	                        verifierId: 1002,
	                        verifierName: '平台002',
	                        verifieTime: '2016-11-29 10:00:00'
	                    },{
	                        agencyCertId: 7,
	                        verifySubmitTime: '2016-11-28 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过003.',
	                        verifierId: 1003,
	                        verifierName: '平台003',
	                        verifieTime: '2016-11-28 10:00:00'
	                    },{
	                        agencyCertId: 6,
	                        verifySubmitTime: '2016-11-27 09:00:00',
	                        verifyResultFlg: '9',
	                        verifyResultDetail: '不通过004。',
	                        verifierId: 1003,
	                        verifierName: '平台004',
	                        verifieTime: '2016-11-27 10:00:00'
	                    },{
	                        companyCertId: 5,
	                        verifySubmitTime: '2016-11-26 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过005。',
	                        verifierId: 1004,
	                        verifierName: '平台005',
	                        verifieTime: '2016-11-26 10:00:00'
	                    }],
	                    depositPayStatus: '0',
	                    informationVerifyStatus: '1',
	                    storeAgencyFlg: '1',
	                    agencyUpdateTime : '2017-01-02 13:00:00',
	                    capitalFlowMemo: '入驻保证金备注',
	                    depositUpdateTime : '2017-01-03 13:00:00',
	                    currencyType: 'RMB',
	                    depositAmount: '13000',
	                    agencyFlg: '0'
	                },
	                errorType: '',
	                errorMsg: ''
	            }
	        }, // 服务商入驻保证金认证end
	        3: {
	            data: {
	                resultCd: 1,
	                results: {
	                    agencyCertId: 1,
	                    verifySubmitTime: '2017-04-03 09:00:00',
	                    verifyResultFlg: '1',
	                    verifyResultDetail: '服务商审核结果描述',
	                    sysVerifyResultFlg: '0',
	                    sysVerifyResultDetail: '系统服务商审核结果描述',
	                    sysVerifyTime: '2016-12-02 08:00:59',
	                    certContentJson: '{"营业执照注册号":"9999999999999999","住所":"上海市徐汇区桂平路680号","法人":"王二","公司类型":"有限责任公司","注册资本":"壹仟万圆整","实收资本":"壹仟万圆整","经营范围":"经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围经营范围","成立日期":"2016年12月01日"}',
	                    updateTime: '2017-01-01 09:00:14',
	                    verifyFile: [{
	                        fileId: 1,
	                        fileName: '营业执照11.jpg',
	                        type: 'jpg',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: 2,
	                        fileName: '公司入住服务猫展望111.pdf',
	                        type: 'pdf',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: 3,
	                        fileName: '公司入住服务猫展望222.pdf',
	                        type: 'pdf',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }, {
	                        fileId: 4,
	                        fileName: '公司入住服务猫展望333.pdf',
	                        type: 'pdf',
	                        path: 'images/tmp/u225.jpg',
	                        secretKey: '111dasdsak'
	                    }],
	                    agencyVerifyHistory:[{
	                        agencyCertId: 9,
	                        verifySubmitTime: new Date(),
	                        verifyResultFlg: '0',
	                        verifyResultDetail: '不通过001。',
	                        verifierId: 1001,
	                        verifierName: '平台001',
	                        verifieTime: new Date()
	                    },{
	                        agencyCertId: 8,
	                        verifySubmitTime: '2016-11-29 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过002。',
	                        verifierId: 1002,
	                        verifierName: '平台002',
	                        verifieTime: '2016-11-29 10:00:00'
	                    },{
	                        agencyCertId: 7,
	                        verifySubmitTime: '2016-11-28 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过003.',
	                        verifierId: 1003,
	                        verifierName: '平台003',
	                        verifieTime: '2016-11-28 10:00:00'
	                    },{
	                        agencyCertId: 6,
	                        verifySubmitTime: '2016-11-27 09:00:00',
	                        verifyResultFlg: '9',
	                        verifyResultDetail: '不通过004。',
	                        verifierId: 1003,
	                        verifierName: '平台004',
	                        verifieTime: '2016-11-27 10:00:00'
	                    },{
	                        companyCertId: 5,
	                        verifySubmitTime: '2016-11-26 09:00:00',
	                        verifyResultFlg: '1',
	                        verifyResultDetail: '不通过005。',
	                        verifierId: 1004,
	                        verifierName: '平台005',
	                        verifieTime: '2016-11-26 10:00:00'
	                    }],
	                    depositPayStatus: '1',
	                    informationVerifyStatus: '1',
	                    storeAgencyFlg: '1',
	                    agencyUpdateTime : '2017-01-02 13:00:00',
	                    capitalFlowMemo: '入驻保证金备注',
	                    depositUpdateTime : '2017-01-03 13:00:00',
	                    currencyType: 'RMB',
	                    depositAmount: '13000',
	                    agencyFlg: '0'
	                },
	                errorType: '',
	                errorMsg: ''
	            }
	        } // 成为服务商认证end
        }
    }
};