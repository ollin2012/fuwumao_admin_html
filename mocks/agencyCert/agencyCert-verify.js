/**
 * mock for 服务商认证审核
 * @type {Object}
 */
module.exports = {
    '/api/agencyCert/dataVerify': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
