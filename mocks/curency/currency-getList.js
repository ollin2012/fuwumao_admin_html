/**
 * mock for 平台规则设定变更
 * @type {Object}
 */
module.exports = {
    '/api/currency/getList': {
        POST: {
            data: {
                resultCd: 1,
                "results": {
                    "current": {
                        "settingStartDate": "2016-11-11 22:22:33.000",
                        "currencyList": [
                            "RMB",
                            "USD"
                        ]
                    },
                    "reserve": {
                        "settingStartDate": "2017-11-11 22:22:33.0",
                        "currencyList": [
                            "RMB",
                            "JPY"
                        ]
                    }
                }
            }
        }
    }
};
