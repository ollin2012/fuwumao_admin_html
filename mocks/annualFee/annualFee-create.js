/**
 * mock for 年费添加
 * @type {Object}
 *
 * params:{}
 */
module.exports = {
    '/api/annualFee/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
