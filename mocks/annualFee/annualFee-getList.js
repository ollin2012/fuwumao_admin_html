/**
 * mock for AGY051_年费缴纳记录一览取得
 * @type {Object}
 *
 * params:
 * {companyId: '1'}
 */
module.exports = {
    '/api/annualFee/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 3,
                    list: [{
                        id: '3',
                        agencyFlg: '1',
                        annualFeeGrade: '3',
                        realAnnualFee: '30000',
                        realCurrencyType: 'RMB',
                        payeeDate: '2017-01-03 00:00:00',
                        startDate: '2017-01-03 00:00:00.000',
                        endDate: '2017-02-28 00:00:00.000',
                        notes: '年费缴纳备注201702'
                    }, {
                        id: '2',
                        agencyFlg: '1',
                        annualFeeGrade: '2',
                        realAnnualFee: '20000',
                        realCurrencyType: 'RMB',
                        payeeDate: '2016-01-03 00:00:00',
                        startDate: '2016-01-03 00:00:00.000',
                        endDate: '2016-12-31 00:00:00.000',
                        notes: '年费缴纳备注2016'
                    }, {
                        id: '1',
                        agencyFlg: '1',
                        annualFeeGrade: '1',
                        realAnnualFee: '10000',
                        realCurrencyType: 'RMB',
                        payeeDate: '2015-01-03 00:00:00',
                        startDate: '2015-01-03 00:00:00.000',
                        endDate: '2015-12-31 00:00:00.000',
                        notes: '年费缴纳备注2015'
                    }]
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};