/**
 * mock for 用户登陆
 *
 * @type {Object}
 *
 * params: {name: "user", password: "123456"}
 */
module.exports = {
    '/api/adminUser/login': {
        POST: {
            default: {
                data: {
                    resultCd: 1,
                    results: {
                        id: '1',
                        userName: 'GH00001',
                        name: '管理员小李',
                        companyId: '0',
                        actionList: [
                            'user.view',
                            'user.edit',
                            'role.view',
                            'role.edit',
                            'company.view',
                            'company.edit',
                            'capital.view',
                            'capital.manage',
                            'refund.view',
                            'refund.manage',
                            'complaint.view',
                            'advert.manage',
                            'advert.view',
                            'siteContent.manage',
                            'recommendation.view',
                            'recommendation.manage'
                        ],
                        roleId: '1',
                        token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJjb21wYW55SWRcIjo5OTk5LFwidXNlcklkXCI6NjZ9IiwiaWF0IjoxNDkyNTg0Nzc3fQ.j3mk5WVDOTwtD_5v5L89w0i8_Oq0LM2Zj1k8eGRSDUf9-CKKfNvgeHAC4Rk_wyDa25X33V7EXWxbbBgLjtBfwg'
                    }
                }
            },
            error: {
                code: 400,
                data: {
                    resultCd: 0,
                    errorType: 'ERR_COM_NO_AUTH'
                }
            }
        }
    }
};
