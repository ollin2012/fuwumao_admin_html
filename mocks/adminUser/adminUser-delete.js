/**
 * mock for 角色删除
 * @type {Object}
 */
module.exports = {
    '/api/adminUser/delete': {
        POST: {
            data: {
                resultCd: 1,
                message: '删除角色成功！'
            }
        }
    }
};
