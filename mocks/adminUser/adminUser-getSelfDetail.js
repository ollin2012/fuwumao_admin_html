/**
 * mock for 用户变更
 * @type {Object}
 */
module.exports = {
    '/api/adminUser/getSelfDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: "9000000001",
                    number: "FWM0000000001",
                    name: "小猫一",
                    sex: "1",
                    mail: "fwm01@fuwumao.com",
                    mobile: "13800000001",
                    roleList: [{
                        roleId: "1",
                        roleName: "经理"
                    }, {
                        roleId: "2",
                        roleName: "用户及组织管理员"
                    }, {
                    }],
                    departmentName: "管理部",
                    status: "1",
                    notes: "备注",
                    updateTime: "2016/01/01 18:00:00"
                }
            }
        }
    }
};
