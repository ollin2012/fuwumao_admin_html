/**
 * mock for 用户变更
 * @type {Object}
 */
module.exports = {
    '/api/adminUser/personalInfoUpdate': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
