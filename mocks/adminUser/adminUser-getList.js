/**
 * mock for 角色列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/adminUser/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: 90001,
                        userName: 'GH00001',
                        name: '用户管理',
                        sex: '1',
                        mobile:"13333333333",
                        mail:"163@163.com",
                        roleList:['经理','超级管理员','服务人员','交易人员'],
                        departmentId:"0",
                        departmentName:"管理部",
                        status:"1"
                    }, {
                        id: 2,
                        name: '游客'
                    }]
                }
            }
        }
    }
};
