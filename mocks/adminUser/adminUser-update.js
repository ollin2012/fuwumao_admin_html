/**
 * mock for 角色变更
 * @type {Object}
 */
module.exports = {
    '/api/adminUser/update': {
        POST: {
            data: {
                resultCd: 1,
                message: '变更角色信息成功！'
            }
        }
    }
};
