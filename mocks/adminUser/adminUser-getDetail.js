/**
 * mock for 角色详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/adminUser/getDetail': {
        POST: {
            data: {
                resultCd: "1",
                results: {
                    adminUserId: "9000000001",
                    number: "FWM0000000001",
                    name: "小猫一",
                    sex: "0",
                    mail: "fwm01@fuwumao.com",
                    mobile: "13800000001",
                    roleList: [{
                        roleId: "0",
                        roleName: "超级管理员"
                    }, {
                        roleId: "1",
                        roleName: "服务交易流程总负责人"
                    }, {
                        roleId: "2",
                        roleName: "经理"
                    }],
                    departmentId: "5",
                    departmentName: "人事行政部",
                    status: "1",
                    notes: "备注",
                    updateTime: "2016/01/01 18:00:00"
                    }
            }


}
    }
};
