/**
 * mock for ADM022_后台用户刷新token
 *
 * @type {Object}
 */
module.exports = {
    '/api/adminUser/refreshToken': {
        POST: {
            default: {
                data: {
                    resultCd: 1,
                    results: {
                        token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJjb21wYW55SWRcIjo5OTk5LFwidXNlcklkXCI6NjZ9IiwiaWF0IjoxNDkyNTg0Nzc3fQ.j3mk5WVDOTwtD_5v5L89w0i8_Oq0LM2Zj1k8eGRSDUf9-CKKfNvgeHAC4Rk_wyDa25X33V7EXWxbbBgLjtBfwg'
                    }
                }
            }
        }
    }
};
