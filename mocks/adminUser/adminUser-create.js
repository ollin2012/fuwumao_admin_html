/**
 * mock for 角色新增
 * @type {Object}
 *
 * params:
 * {
 * 	name: "test1", 
 * }
 */
module.exports = {
    '/api/adminUser/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
}; 
