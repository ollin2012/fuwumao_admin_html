/**
 * mock for 保证金csv导出
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/agencyDeposit/export': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        agencyName: '服务猫管理有限公司一',
                        agencyCompanyNumber: 'QY00000000001',
                        tradNumber: null,
                        depositType: 1,
                        depositAmount: '10000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowNumber: 'CFN0000000001',
                        capitalChangeType: '1',
                        frozenFlg: '1',
                    }, {
                        agencyName: '服务猫管理有限公司一',
                        agencyCompanyNumber: 'QY00000000001',
                        tradNumber: 'JY00000000001',
                        depositType: 2,
                        depositAmount: '1300000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowNumber: 'CFN0000000005',
                        capitalChangeType: '1',
                        frozenFlg: '1',
                    }, {
                        agencyName: '服务猫管理有限公司二',
                        agencyCompanyNumber: 'QY00000000002',
                        tradNumber: 'JY00000000002',
                        depositType: 3,
                        depositAmount: '1300000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowNumber: 'CFN0000000002',
                        capitalChangeType: '2',
                        frozenFlg: '2',
                    }, {
                        agencyName: '服务猫管理有限公司三',
                        agencyCompanyNumber: 'QY00000000003',
                        tradNumber: 'JY00000000003',
                        depositType: 2,
                        depositAmount: '1300000000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowNumber: 'CFN0000000003',
                        capitalChangeType: '2',
                        frozenFlg: '2',
                    }, {
                        agencyName: '服务猫管理有限公司四',
                        agencyCompanyNumber: 'QY00000000004',
                        tradNumber: 'JY00000000004',
                        depositType: 2,
                        depositAmount: '1300000',
                        currencyType: 'RMB',
                        depositChangeTime: new Date(),
                        capitalFlowNumber: 'CFN0000000004',
                        capitalChangeType: '3',
                        frozenFlg: '2',
                    }, ]
                }
            }
        }
    }
};
