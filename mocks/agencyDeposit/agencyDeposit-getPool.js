/**
 * mock for AGY008_服务商保证金取得
 * @type {Object}
 *
 * params:{
 *  companyId: '1'
 * }
 */
module.exports = {
    '/api/agencyDeposit/getPool': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    enterDepositList: [{
                        totalDepositAmount: '100000.00'
                    }, {
                        totalDepositAmount: '200000.00'
                    }],
                    companyAccountList: [{
                        totalAmount: '2000000.02',
                        availableAmount: '800000.01',
                        frozenAmount: '1200000.00',
                        currencyType: 'RMB'
                    }, {
                        totalAmount: '3000000.00',
                        availableAmount: '900000.03',
                        frozenAmount: '1200000.00',
                        currencyType: 'USD'
                    }],
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};