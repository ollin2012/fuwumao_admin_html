/**
 * mock for 行业变更
 * @type {Object}
 */
module.exports = {
    '/api/industry/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
