/**
 * mock for industry_getList(行业查询)
 * 
 * @type {Object}
 * 
 * params: {}
 */
module.exports = {
    '/api/industry/getList': {
        POST: {
            default: {
                data: {
                    resultCd: '1',
                    results: {
                        list: [{
                            id:'100',
                            level:'1',
                            status:'2',
                            parentId:null
                        },
                            {
                                id:'101',
                                level:'2',
                                status:'2',
                                parentId:'100'
                            },
                            {
                                id:'102',
                                level:'2',
                                status:'2',
                                parentId:'100'
                            },
                            {
                                id:'103',
                                level:'2',
                                status:'2',
                                parentId:'100'
                            },
                            {
                                id:'104',
                                level:'2',
                                status:'2',
                                parentId:'100'
                            },{
                                id:'105',
                                level:'2',
                                status:'2',
                                parentId:'100'
                            },{
                            id:'200',
                            level:'1',
                            status:'2',
                            parentId:null
                        },
                            {
                                id:'201',
                                level:'2',
                                status:'2',
                                parentId:'200'
                            },
                            {
                                id:'202',
                                level:'2',
                                status:'2',
                                parentId:'200'
                            },
                            {
                                id:'203',
                                level:'2',
                                status:'2',
                                parentId:'200'
                            },
                            {
                                id:'204',
                                level:'2',
                                status:'2',
                                parentId:'200'
                            },
                            {
                                id: '300',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id:'301',
                                level:'2',
                                status:'2',
                                parentId:'300'
                            },

                            {
                                id:'302',
                                level:'2',
                                status:'2',
                                parentId:'300'
                            },

                            {
                                id:'303',
                                level:'2',
                                status:'2',
                                parentId:'300'
                            },

                            {
                                id:'304',
                                level:'2',
                                status:'2',
                                parentId:'300'
                            },

                            {
                                id: '400',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'401',
                                level:'2',
                                status:'2',
                                parentId:'400'
                            },

                            {
                                id:'402',
                                level:'2',
                                status:'2',
                                parentId:'400'
                            },

                            {
                                id:'403',
                                level:'2',
                                status:'2',
                                parentId:'400'
                            },

                            {
                                id:'404',
                                level:'2',
                                status:'2',
                                parentId:'400'
                            },

                            {
                                id:'405',
                                level:'2',
                                status:'2',
                                parentId:'400'
                            },

                            {
                                id:'406',
                                level:'2',
                                status:'2',
                                parentId:'400'
                            },

                            {
                                id: '500',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'501',
                                level:'2',
                                status:'2',
                                parentId:'500'
                            },

                            {
                                id:'502',
                                level:'2',
                                status:'2',
                                parentId:'500'
                            },

                            {
                                id:'503',
                                level:'2',
                                status:'2',
                                parentId:'500'
                            },

                            {
                                id:'504',
                                level:'2',
                                status:'2',
                                parentId:'500'
                            },

                            {
                                id:'505',
                                level:'2',
                                status:'2',
                                parentId:'500'
                            },

                            {
                                id: '600',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'601',
                                level:'2',
                                status:'2',
                                parentId:'600'
                            },

                            {
                                id:'602',
                                level:'2',
                                status:'2',
                                parentId:'600'
                            },

                            {
                                id:'603',
                                level:'2',
                                status:'2',
                                parentId:'600'
                            },

                            {
                                id:'604',
                                level:'2',
                                status:'2',
                                parentId:'600'
                            },

                            {
                                id:'605',
                                level:'2',
                                status:'2',
                                parentId:'600'
                            },

                            {
                                id: '700',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'701',
                                level:'2',
                                status:'2',
                                parentId:'700'
                            },

                            {
                                id:'702',
                                level:'2',
                                status:'2',
                                parentId:'700'
                            },

                            {
                                id:'703',
                                level:'2',
                                status:'2',
                                parentId:'700'
                            },

                            {
                                id: '800',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'801',
                                level:'2',
                                status:'2',
                                parentId:'800'
                            },

                            {
                                id:'802',
                                level:'2',
                                status:'2',
                                parentId:'800'
                            },

                            {
                                id:'803',
                                level:'2',
                                status:'2',
                                parentId:'800'
                            },

                            {
                                id: '900',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'901',
                                level:'2',
                                status:'2',
                                parentId:'900'
                            },

                            {
                                id:'902',
                                level:'2',
                                status:'2',
                                parentId:'900'
                            },

                            {
                                id:'903',
                                level:'2',
                                status:'2',
                                parentId:'900'
                            },

                            {
                                id:'904',
                                level:'2',
                                status:'2',
                                parentId:'900'
                            },

                            {
                                id:'905',
                                level:'2',
                                status:'2',
                                parentId:'900'
                            },

                            {
                                id: '1000',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'1001',
                                level:'2',
                                status:'2',
                                parentId:'1000'
                            },

                            {
                                id:'1002',
                                level:'2',
                                status:'2',
                                parentId:'1000'
                            },

                            {
                                id:'1003',
                                level:'2',
                                status:'2',
                                parentId:'1000'
                            },

                            {
                                id: '1100',
                                level:'1',
                                status:'2',
                                parentId:null
                            },

                            {
                                id:'1101',
                                level:'2',
                                status:'2',
                                parentId:'1100'
                            },

                            {
                                id:'1102',
                                level:'2',
                                status:'2',
                                parentId:'1100'
                            },

                            {
                                id:'1103',
                                level:'2',
                                status:'2',
                                parentId:'1100'
                            }]
                    },
                    errorType: '',
                    errorMsg: ''
                }
            },
            level1: {
                data: {
                    resultCd: '1',
                    results: {
                        list: [{
                            id: '100',
                            level: '1',
                            status: '2',
                            parentId: null
                        }, {
                            id:'200',
                            level:'1',
                            status:'2',
                            parentId:null
                        },
                            {
                                id: '300',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '400',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '500',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '600',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '700',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '800',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '900',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '1000',
                                level:'1',
                                status:'2',
                                parentId:null
                            },
                            {
                                id: '1100',
                                level:'1',
                                status:'2',
                                parentId:null
                            }]
                    },
                    errorType: '',
                    errorMsg: ''
                }
            }
        }
    }
};
