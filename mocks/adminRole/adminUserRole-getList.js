/**
 * mock for 角色列表
 * @type {Object}
 *
 */
module.exports = {
    '/api/adminUserRole/getList': {
        POST: {
            data: {
                resultCd: 1,
                "results": {
                    "count": "5",
                    "list": [{
                        "id": "0",
                        "roleName": "超级管理员",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "1",
                        "roleName": "服务交易流程总负责人",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "2",
                        "roleName": "经理",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "3",
                        "roleName": "用户及组织管理员",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "4",
                        "roleName": "服务审核管理员",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "5",
                        "roleName": "服务及交易管理员",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "6",
                        "roleName": "事务处理管理员",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }, {
                        "id": "7",
                        "roleName": "资金财务管理员",
                        "roleActionList": [
                            "user.view",
                            "user.manage",
                            "company.view",
                        ]
                    }]
                }
            }
        }
    }
};