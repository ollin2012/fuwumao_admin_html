/**
 * mock for 角色变更
 * @type {Object}
 */
module.exports = {
    '/api/adminUserRole/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
