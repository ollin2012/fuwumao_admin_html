/**
 * mock for 角色列表
 * @type {Object}
 *
 */
module.exports = {
    '/api/adminUserRole/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: 1,
                        roleName: '超级管理员',
                        roleActionList: ['adminUser.view', 'adminUser.manage', 'adminRole.view', 'adminRole.manage', 'adminDepartment.view', 'adminDepartment.manage']
                    }, {
                        id: 2,
                        roleName: '服务交易流程总负责人',
                        roleActionList: ['adminUser.view', 'adminUser.manage', 'adminRole.view', 'adminRole.manage', 'adminDepartment.view', 'adminDepartment.manage']
                    }, {
                        id: 3,
                        roleName: '经理',
                        roleActionList: ['adminUser.view', 'adminUser.manage', 'adminRole.view', 'adminRole.manage', 'adminDepartment.view', 'adminDepartment.manage']
                    }]
                }
            }
        }
    }
};