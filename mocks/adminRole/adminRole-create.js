/**
 * mock for 角色新增
 * @type {Object}
 *
 */
module.exports = {
    '/api/adminUserRole/create': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
