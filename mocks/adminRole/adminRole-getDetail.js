/**
 * mock for 角色详情
 * @type {Object}
 *
 */
module.exports = {
    '/api/adminUserRole/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: 1,
                    roleName: '管理员',
                    roleDescr: '整体系统的管理员，\n用户最高权限',
                    roleActionList: ['adminUser.view', 'adminUser.manage', 'adminRole.view', 'adminRole.manage', 'adminDepartment.view', 'adminDepartment.manage']
                }
            }
        }
    }
};
