/**
 * mock for 角色删除
 * @type {Object}
 */
module.exports = {
    '/api/adminUserRole/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
