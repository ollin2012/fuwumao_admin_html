/**
 * mock for 订单详情
 */
module.exports = {
    '/api/store/getOrderDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                orderDetail:
                {
					status: 7,
					orderId: '1',
					orderName: '2017新产品体验',
					price: '500000',
					executeFrom: '2017-02-22',
					executeTo: '2017-02-24',
					content: '订单说明订单说明订单说明订单说明订单说明',
					memo: '备注备注备注备注备注',
					pic: [{
                        id: '1',
                        path: '/images/tmp/u225.jpg'
                    }],
					attachedList: [{ 
						id: '一期说明文档.doc',
						path: '/images/tmp/u225.jpg',
						},{ 
						id: '二期说明文档.doc',
						path: '/images/tmp/u225.jpg',
						},{ 
						id: '三期说明文档.doc',
						path: '/images/tmp/u225.jpg',
						}],
					stageList: [{
                        stageId: '1',
                        stageName: '阶段1',
                        stagePrice: '200000',
						stageStartTime:'2017-02-22',
                        stageEndTime: '2017-02-23',
                        stageDetail: '阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1阶段1'
                    }, {
                        stageId: '2',
                        stageName: '阶段2',
                        stageOrder: '2',
                        stagePrice: '300000',
						stageStartTime:'2017-02-23',
                        stageEndTime: '2017-02-24',
                        stageDetail: '阶段2阶段2阶段2阶段2阶段2阶段2阶段2阶段2'
                    }],
					
                    
                },
                orderResults:
                [{
						type: '0',
						fileID: 'NULL',
						filePath: 'NULL',
						content: '验收请上传现场照片',
						name: 'NULL',
						companyUserName: '小猫',
						userType: '1',
						createTime: '2017-01-01',
						location:'left',
                    }, {
						type: '1',
						fileID: 'NULL',
						filePath: 'NULL',
						content: '门店上传文件1',
						name: '系统',
						companyUserName: 'NULL',
						userType: 'NULL',
						createTime: '2017-01-01',
						location:'',
                    }, {
						type: '1',
						fileID: 'NULL',
						filePath: 'NULL',
						content: '门店上传文件2',
						name: '系统',
						companyUserName: 'NULL',
						userType: 'NULL',
						createTime: '2017-01-01',
						location:'',
					}, {
						type: '1',
						fileID: 'NULL',
						filePath: 'NULL',
						content: '门店上传文件3',
						name: '系统',
						companyUserName: 'NULL',
						userType: 'NULL',
						createTime: '2017-01-01',
						location:'',
					}, {
						type: '0',
						fileID: 'NULL',
						filePath: 'NULL',
						content: '已全部上传文件',
						name: 'NULL',
						companyUserName: '门店',
						userType: '0',
						createTime: '2017-01-01',
						location:'right',
					}, {
						type: '2',
						fileID: '文件1.jpg',
						filePath: '/images/tmp/u225.jpg',
						content: '已全部上传文件',
						name: 'NULL',
						companyUserName: '门店',
						userType: '0',
						createTime: '2017-01-01',
						location:'right',
					}, {
						type: '2',
						fileID: '文件2.jpg',
						filePath: '/images/tmp/u225.jpg',
						content: '已全部上传文件',
						name: 'NULL',
						companyUserName: '门店',
						userType: '0',
						createTime: '2017-01-01',
						location:'right',
					}]
                }
            }
        }
    }
};
