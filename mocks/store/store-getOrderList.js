/**
 * mock for 订单列表
 */
module.exports = {
    '/api/store/getOrderList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 38,
                    list: [
                                {
                                    orderId: '1',
                                    orderName: '浙江巨力工贸有限公司需设计一套SI',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '彭女士',
                                    money: '500000',
                                    orderStatus: '0'
                                },
                                {
                                    orderId: '2',
                                    orderName: '（江浙沪）禅月建筑科技3D打印云智造平台',
                                    shopId: '2',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '薛女士',
                                    money: '500000',
                                    orderStatus: '7'
                                },
                                {
                                    orderId: '3',
                                    orderName: '知识产权公司商务拓展营销推广',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '陈先生',
                                    money: '500000',
                                    orderStatus: '2'
                                },
                                {
                                    orderId: '4',
                                    orderName: '非标精密机械自动化模块设计',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '吕女士',
                                    money: '500000',
                                    orderStatus: '3'
                                },
                                {
                                    orderId: '5',
                                    orderName: '全国微信群资源渠道合作',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '陆女士',
                                    money: '500000',
                                    orderStatus: '3'
                                },
                                {
                                    orderId: '6',
                                    orderName: '天津英雄联盟游戏代练',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '刘女士',
                                    money: '500000',
                                    orderStatus: '2'
                                },
                                {
                                    orderId: '7',
                                    orderName: '六安尼邦家政服务有限公司微信公众品台商城版开发',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '张先生',
                                    money: '500000',
                                    orderStatus: '5'
                                },
                                {
                                    orderId: '8',
                                    orderName: '（杭州）政府OA办公系统',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '彭女士',
                                    money: '500000',
                                    orderStatus: '5'
                                },
                                {
                                    orderId: '9',
                                    orderName: '紫檀木质汽车钥匙保护壳广告文案',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '黄女士',
                                    money: '500000',
                                    orderStatus: '3'
                                },
                                {
                                    orderId: '10',
                                    orderName: '中小学教辅资源网站的定制开发',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '田先生',
                                    money: '500000',
                                    orderStatus: '3'
                                }
                    ]
                }
            },
            query: {
                // Usage: /store_api/order/getList?type=store
                'type=store': {
                    data: {
                        resultCd: 1,
                        results: {
                            count: 31,
                            list: [
                                {
                                    orderId: '1',
                                    orderName: '国际服装展问卷收集',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '彭女士',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '2',
                                    orderName: '中国国际家用纺织品博览会协助',
                                    shopId: '2',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '薛女士',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '3',
                                    orderName: '轻松室内展示派单',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '陈先生',
                                    money: '500000',
                                    orderStatus: '6'
                                },
                                {
                                    orderId: '4',
                                    orderName: '羽绒服休闲裤业余时间拍摄',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '吕女士',
                                    money: '500000',
                                    orderStatus: '7'
                                },
                                {
                                    orderId: '5',
                                    orderName: '招展会活动+现场协助',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '陆女士',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '6',
                                    orderName: '照片后期制作',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '刘女士',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '7',
                                    orderName: '珠宝类企业官网设计',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '张先生',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '8',
                                    orderName: '现有OA二次开发（长期合作）',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '彭女士',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '9',
                                    orderName: '鑫玉粮油新品牌设计方案',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '黄女士',
                                    money: '500000',
                                    orderStatus: '1'
                                },
                                {
                                    orderId: '10',
                                    orderName: '全富B2B2C商城项目开发PC+APP',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '田先生',
                                    money: '500000',
                                    orderStatus: '1'
                                }
                            ]
                        }
                    }
                },
                // Usage: /store_api/order/getList?type=company
                'type=company': {
                    data: {
                        resultCd: 1,
                        results: {
                            count: 31,
                            list: [
                                {
                                    orderId: '1',
                                    orderName: '一带一路双创平台',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '彭女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '2',
                                    orderName: '百度钱包1分钱送6罐加多宝',
                                    shopId: '2',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '薛女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '3',
                                    orderName: '华泰证券APP下载＋注册',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '陈先生',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '4',
                                    orderName: '多用户打印系统开发项目',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '吕女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '5',
                                    orderName: '万达长白山国际度假区品牌全案',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '陆女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '6',
                                    orderName: '无锡合奥金属材料有限公司项目品牌全案设计',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '刘女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '7',
                                    orderName: '“牛浪汉”线上店铺运营',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '张先生',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '8',
                                    orderName: '城口山地鸡品牌形象升级方案',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '彭女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '9',
                                    orderName: '启航教育品牌升级全案策划',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '黄女士',
                                    money: '500000',
                                    orderStatus: '4'
                                },
                                {
                                    orderId: '10',
                                    orderName: '四川新渔—品类品牌设计',
                                    shopId: '1',
                                    shopName: '人民广场店',
                                    executeFrom: '2017-02-22',
                                    executeTo: '2017-02-24',
                                    sender: '田先生',
                                    money: '500000',
                                    orderStatus: '4'
                                }
                            ]
                        }
                    }
                }
            }
        }
    }
};
