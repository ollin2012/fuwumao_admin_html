/**
 * mock for 门店列表
 */

module.exports = {
    '/api/store/getList': {
        POST: {
            data: {
                resultCd: 1,
                errorMsg: "INF_COM_NO_SEARCH_RESULT",
                results: {
                    count: 35,
                    list: [{
                        id: '1',
                        storeName: '人民广场店',
                        area1: '上海',
                        area2: '上海',
                        area3: '黄浦区',
                        address: '人民广场1260号6楼',
                        status: '0',
                        name: '小猫'


                    }, {
                        id: '2',
                        storeName: '人民广场2店',
                        area1: '上海',
                        area2: '上海',
                        area3: '黄浦区',
                        address: '人民广场1260号1楼',
                        status: '1',
                        name: '大猫'
                    }, {
                        id: '3',
                        storeName: '人民广场3店',
                        area1: '上海',
                        area2: '上海',
                        area3: '黄浦区',
                        address: '人民广场1260号1楼',
                        status: '1',
                        name: '大猫'
                    }, {
                        id: '4',
                        storeName: '人民广场4店',
                        area1: '上海',
                        area2: '上海',
                        area3: '黄浦区',
                        address: '人民广场1260号1楼',
                        status: '1',
                        name: '大猫'
                    }]
                }
            }
        }
    }
};

