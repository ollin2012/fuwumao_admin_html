/**
 * mock for 门店详情
 */
module.exports = {
    '/api/store/getDetail': {
        POST: {
            data: {
                "resultCd": 1,
                "results": {
                    "ID":"1",
                    "mail":"fwm02@fuwumao.com",
                    "name":"小猫",
                    "mobile":"13800000002",
                    "sex":"0",
                    "storeName": "人民广场店",
                    "area1":"10800",
                    "area2":"10831",
                    "area3":"1083101",
                    "address": "人民广场1260号6楼",
                    "tel": "021-51368498",
                    "fax": "021-51368498",
                    "postCode": "200001",
                    "areas": "100.53",
					"personnels": "100",
                    "status": "0",
					"memo": "人民广场店 上海分部"
                    },
                "errorMsg":"",
                "errorType":""

            }
		}
    }
};
