/**
 * mock for store_getAreaList(取得门店地区)
 * 
 * @type {Object}
 * 
 * params: {}
 */
module.exports = {
    '/api/store/getAreaList': {
        POST: {
            data: {
                resultCd: 1,
                errorType: '',
                errorMsg: '',
                results: [
                    {
                        id: 130201,
                        name: '市辖区'
                    },
                    {
                        id: 130202,
                        name: '路南区'
                    },
                    {
                        id: 130203,
                        name: '路北区'
                    },
                    {
                        id: 130204,
                        name: '古冶区'
                    },
                    {
                        id: 130205,
                        name: '开平区'
                    },
                    {
                        id: 130207,
                        name: '丰南区'
                    },
                    {
                        id: 130208,
                        name: '丰润区'
                    },
                    {
                        id: 130223,
                        name: '滦　县'
                    },
                    {
                        id: 130224,
                        name: '滦南县'
                    },
                    {
                        id: 130225,
                        name: '乐亭县'
                    },
                    {
                        id: 130227,
                        name: '迁西县'
                    },
                    {
                        id: 130229,
                        name: '玉田县'
                    },
                    {
                        id: 130230,
                        name: '唐海县'
                    },
                    {
                        id: 130281,
                        name: '遵化市'
                    },
                    {
                        id: 130283,
                        name: '迁安市'
                    }
                ]
            }
        }
    }
};