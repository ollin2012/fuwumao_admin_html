/**
 * mock for 地区变更
 * @type {Object}
 */
module.exports = {
    '/api/area/updateStatus': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
