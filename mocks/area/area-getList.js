/**
 * mock for area_getList(地区查询)
 * 
 * @type {Object}
 * 
 * params: {}
 */
module.exports = {
    '/api/area/getList': {
        POST: {
            data: {
                resultCd: '1',
                results: {
                	count: '50',
                    list: [{
                        id:'10000',
                        level:'1',
                        status:'0',
                        parentId: null,
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10800',
                        level:'2',
                        status:'0',
                        parentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10801',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10802',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10803',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10804',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10805',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10806',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10807',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10808',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10809',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10810',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10811',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10812',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10813',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10814',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10815',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10816',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10817',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10818',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10819',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10820',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10821',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10822',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10823',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10824',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10825',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10826',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10827',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10828',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10829',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10830',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10831',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10832',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10833',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10834',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10835',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10836',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10837',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10838',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    },
                    {
                        id:'10839',
                        level:'3',
                        status:'0',
                        parentId:'10800',
                        parentParentId:'10000',
                        updateTime: '2017-01-01 00:00:00'
                    }]
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};
