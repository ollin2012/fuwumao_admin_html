/**
 * mock for 服务详情
 * @type {Object}
 *
 * params:
 * {
    serviceId: '1',
    serviceInfoDiv:'1',//null：服务情报(正式) 1：最新服务情报取得(可能履历)
}
 */
module.exports = {
    '/api/service/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    serviceHistoryId: '1',
                    serviceId: '1',
                    serviceCategoryId: '1',
                    categoryName: '服装',
                    serviceNumber: 'SVC201612000001',
                    serviceStatus: '3',
                    serviceName: '西装定制',
                    agencyId: '1',
                    agencyName: '服务商1',
                    agencyLogoFilePath: '',
                    serviceLanguage: '1',
                    industry1: '100',
                    industry2: '101',
                    area1: '10000',
                    area2: '10800',
                    area3: '10831',
                    serviceType: '1',
                    currencyType: 'RMB',
                    servicePrice: '888',
                    serviceDetail: '<h3 style="color:red">ckedit服务详细内容服务详细内容服务详细内容服务详细内容</h3>',
                    serviceProblemsJson: '服务商常见问题',
                    updateTime: '2016-12-18 11:30:30',
                    serviceTradeCount: '12',
                    serviceScore: '4',
                    serviceCollectedTimes: '10',
                    serviceStage: [{
                        stageName: '阶段1',
                        showOrder: '1',
                        stagePrice: '444',
                        stageDetail: '<h3 style="color:red">ckedit服务阶段详细内容1</h3>',
                        stageExpectTime: 4,
                        stageExpectTimeUnit: 'w',
                        updateTime: '2016-12-18 11:30:30',
                    }, {
                        stageName: '阶段2',
                        showOrder: '2',
                        stagePrice: '555',
                        stageDetail: '服务阶段详细内容2',
                        stageExpectTime: 15,
                        stageExpectTimeUnit: 'd',
                        updateTime: '2016-12-18 11:30:30',
                    }, ],
                    servicePic: [{
                        fileId: '1',
                        filePath: '/images/tmp/u225.jpg',
                        logoFlg: '1',
                        showOrder: '1',
                        updateTime: '2016-12-18 11:30:30',
                    }, ],
                    serviceEvaluationList: [{
                        companyIdEvaluting: '1',
                        companyNameEvaluting: '企业1',
                        companyLogoFilePath: 'http://www.fuwumao.com/pic/fuwumao_pic003.png',
                        evaluationScore: '5',
                        evaluationContent: '服务很热情！'
                    }, {
                        companyIdEvaluting: '2',
                        companyNameEvaluting: '企业2',
                        companyLogoFilePath: 'http://www.fuwumao.com/pic/fuwumao_pic004.png',
                        evaluationScore: '5',
                        evaluationContent: '合作愉快！'
                    }]
                }
            }
        }
    }
};
