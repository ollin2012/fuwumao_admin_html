/**
 * mock for 服务列表
 * @type {Object}
 *
 * params:
 * {
    id: '1',
    serviceNumber:'S001',
    serviceName:'服务名称1',
    agencyName:'服装商1',
    serviceStatus:'1',
    serviceType:'1',
    serviceLanguage:'1',
    servicePriceFrom:100,
    servicePriceTo:10000,
    currencyType:'RMB',
    industryName1:'1',
    industryName2:'2',
    areaName1:'1',
    areaName2:'2',
    areaName3:'1',
    page:1,
    count:10,
    }
 */
module.exports = {
    '/api/service/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        serviceId: '1',
                        serviceNumber: 'SVC201612000001',
                        categoryName: '分类1',
                        serviceStatus: '1',
                        serviceName: '西装定制',
                        serviceLanguage: '1',
                        agencyId: '1',
                        agencyName: '服务商1',
                        industry1: '1',
                        industry2: '101',
                        area1: '10000',
                        area2: '10800',
                        area3: '10831',
                        serviceType: '1',
                        currencyType: 'RMB',
                        servicePrice: '888',
                        serviceBetopFlg: '1',
                        serviceTradeCount: '12',
                        serviceScore: '4',
                        updateUserId: '1',
                        updateTime: '2016-12-18 11:30:30',
                        verifySubmitTime: '2016-12-10 11:30:30',
                        serviceLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/img.jpg'
                        },
                    }, {
                        serviceId: '2',
                        serviceNumber: 'SVC201612000002',
                        serviceStatus: '2',
                        serviceName: '西装定制',
                        serviceLanguage: '1',
                        agencyId: '1',
                        agencyName: '服务商2',
                        industry1: '2',
                        industry2: '201',
                        area1: '10000',
                        area2: '10800',
                        area3: '10807',
                        serviceType: '2',
                        currencyType: 'RMB',
                        servicePrice: '8888',
                        serviceTradeCount: '13',
                        serviceScore: '4',
                        updateUserId: '1',
                        updateTime: '2016-12-18 11:30:30',
                        verifySubmitTime: '2016-12-10 11:30:30',
                        serviceLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/img.jpg'
                        }
                    }, {
                        serviceId: '3',
                        serviceNumber: 'SVC201612000003',
                        serviceStatus: '3',
                        serviceName: '西装定制',
                        serviceLanguage: '1',
                        agencyId: '1',
                        agencyName: '服务商3',
                        industry1: '3',
                        industry2: '301',
                        area1: '10000',
                        area2: '10800',
                        area3: '10813',
                        serviceType: '3',
                        currencyType: 'RMB',
                        servicePrice: '88888',
                        serviceTradeCount: '123',
                        serviceScore: '4',
                        updateUserId: '1',
                        updateTime: '2016-12-18 11:30:30',
                        verifySubmitTime: '2016-12-10 11:30:30',
                        serviceLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/img.jpg'
                        }
                    }, {
                        serviceId: '4',
                        serviceNumber: 'SVC201612000003',
                        serviceStatus: '3',
                        serviceName: '西装定制si',
                        serviceLanguage: '1',
                        agencyId: '1',
                        agencyName: '服务商3',
                        industry1: '3',
                        industry2: '301',
                        area1: '10000',
                        area2: '10800',
                        area3: '10813',
                        serviceType: '3',
                        currencyType: 'RMB',
                        servicePrice: '88888',
                        serviceTradeCount: '123',
                        serviceScore: '4',
                        updateUserId: '1',
                        updateTime: '2016-12-18 11:30:30',
                        verifySubmitTime: '2016-12-10 11:30:30',
                        serviceHistoryId: '999',
                        serviceHistoryStatus: '2',
                        serviceLogo: {
                            fileId: '1',
                            filePath: '/images/tmp/img.jpg'
                        }
                    }]
                }
            }
        }
    }
};
