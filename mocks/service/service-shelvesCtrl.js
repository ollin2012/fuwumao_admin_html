/**
 * mock for 服务上下架控制
 * @type {Object}
 *
 * params: 
 * {
    serviceId: '1',
    updateTime:'2016-12-18 11:30:30',
    }
 */
module.exports = {
    '/api/service/shelvesCtrl': {
        POST: {
            data: {
                resultCd: 1,
            }
        }
    }
};
