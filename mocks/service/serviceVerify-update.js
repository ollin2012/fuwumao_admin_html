/**
 * mock for 服务审核
 * @type {Object}
 *
 * params: 
 * {
 *   //服务审核履历ID
 *   serviceVerifyId:'1',
 *   verifyResultFlg:'9',
 *   verifyResultDetail:'审核不通过',
 *   updateTime:'2017-01-18 11:30:30',
 * }
 */
module.exports = {
    '/api/serviceVerify/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
