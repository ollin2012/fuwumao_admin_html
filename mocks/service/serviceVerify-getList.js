/**
 * mock for 服务审核履历一览取得
 * @type {Object}
 *
 * params: 
 * {serviceId: '1',}
 */
module.exports = {
    '/api/serviceVerify/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 0,
                    list: [{
                        id: '1',
                        verifySubmitStatus: '2',
                        verifySubmitTime: '2016-11-18 11:30:30',
                        verifyResultFlg: '0',
                        verifyResultDetail: '审核结果详细详细',
                        verifierCompanyId: '1',
                        verifierCompanyName: '服务猫',
                        verifierId: '1',
                        verifierName: '管理猫1',
                        verifyTime: '2017-12-11 11:30:30',
                        updateTime: '2016-12-12 12:12:12',
                    }, {
                        id: '2',
                        verifySubmitStatus: '2',
                        verifySubmitTime: '2016-12-18 11:30:30',
                        verifyResultFlg: '1',
                        verifyResultDetail: '审核结果详细详细',
                        verifierCompanyId: '1',
                        verifierCompanyName: '服务猫',
                        verifierId: '2',
                        verifierName: '管理猫2',
                        verifyTime: '2017-12-21 11:30:30',
                        updateTime: '2016-12-12 12:12:12',
                    }, {
                        id: '3',
                        verifySubmitStatus: '2',
                        verifySubmitTime: '2016-12-18 11:30:30',
                        verifyResultFlg: '9',
                        verifyResultDetail: '审核结果详细详细',
                        verifierCompanyId: '1',
                        verifierCompanyName: '服务猫',
                        verifierId: '3',
                        verifierName: '管理猫3',
                        verifyTime: '2017-12-21 11:30:30',
                        updateTime: '2016-12-12 12:12:12',
                    }, ]
                }
            }
        }
    }
};
