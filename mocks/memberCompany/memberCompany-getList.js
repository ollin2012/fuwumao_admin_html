/**
 * mock for 成员企业列表
 * @type {Object}
 *
 * params: 
 * {
 *     page: "1", 
 *     count: "10", 
 *     name: "t", 
 * }
 */
module.exports = {
    '/api/companyInvite/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    count: 35,
                    list: [{
                        id: 1,
                        invitedCompanyId: 2,
                        invitedCompanyNumber: 'QY00000000002',
                        invitedCompanyName: '受邀企业一',
                        invitedCompanyStatus: '1',
                        agencyFlg: '0',
                        inviteTime: '2016-12-31 09:00:00',
                        invitedCompanyRegisterTime: '2017-01-01 00:00:01',
                        serviceCount: '13',
                        tradeCount: '33',
                        inviteCompanyId: 1,
                        inviteCompanyNumber: 'QY00000000001',
                        inviteCompanyName: '邀请企业一',
                        inviteStatus: '3',
                        updateTime: '2017-01-02 00:00:01',
                    }, {
                        id: 2,
                        invitedCompanyId: null,
                        invitedCompanyNumber: 'QY00000000004',
                        invitedCompanyName: '受邀企业二',
                        invitedCompanyStatus: '1',
                        agencyFlg: '1',
                        inviteTime: '2016-12-31 10:00:00',
                        invitedCompanyRegisterTime: '2017-01-01 01:00:01',
                        serviceCount: '1300',
                        tradeCount: '33000',
                        inviteCompanyId: 3,
                        inviteCompanyNumber: 'QY00000000003',
                        inviteCompanyName: '邀请企业二',
                        inviteStatus: '3',
                        updateTime: '2017-01-02 01:00:01',
                    }, ]
                }
            }
        }
    }
};
