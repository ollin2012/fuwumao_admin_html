/**
 * mock for tradeTimeline_getList(TRD020_交易动态取得)
 *
 * @type {Object}
 *
 * params: {
 *  count:'10'
 *  page:'1'
 *  tradeId:'11'
 *  sortCols:{operationTime:'desc'}
 * }
 */
module.exports = {
    '/api/tradeTimeline/getList': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    list: [{
                        id: '1',
                        operationTime: '2017-01-01 13:23:30',
                        operatorId: '001',
                        operatorName: '小猫A',
                        operationType: '接受订单',
                        msgType: '买家企业',
                        templateCode: 'INF_TRD_CREATE_NEW_ORDER',
                        templateParam: '["00001"]',
                    }, {
                        id: '2',
                        operationTime: '2017-01-01 13:23:30',
                        operatorId: '001',
                        operatorName: '小猫X',
                        operationType: '接受订单',
                        msgType: '买家企业',
                        fileId: 'f0001',
                        fileName: '文件1',
                        filePath: 'http://dafsdfa.asf.com/sdaf.txt',
                        templateCode: 'INF_TRD_ACCEPTED_ORDER',
                        templateParam: '["00001"]',
                    }, {
                        id: '3',
                        operationTime: '2017-01-01 13:23:30',
                        operatorId: '001',
                        operatorName: '小猫B',
                        operationType: '接受订单',
                        msgType: '买家企业',
                        fileId: 'f0001',
                        fileName: '文件1',
                        filePath: 'http://dafsdfa.asf.com/sdaf.txt',
                        templateCode: 'INF_TRD_TRADE_CANCEL',
                        templateParam: '["aaaa","企业bbbb"]',
                    }, {
                        id: '4',
                        operationTime: '2017-01-01 13:23:30',
                        operatorId: '001',
                        operatorName: '小猫C',
                        operationType: '接受订单',
                        msgType: '买家企业',
                        fileId: 'f0001',
                        fileName: '文件1',
                        filePath: 'http://dafsdfa.asf.com/sdaf.txt',
                        templateCode: 'INF_TRD_TRADE_TERMINATION',
                        templateParam: '["00001", "BOLSTRACT"]',
                    }, {
                        id: '5',
                        operationTime: '2017-01-01 13:23:30',
                        operatorId: '001',
                        operatorName: '小猫D',
                        operationType: '接受订单',
                        msgType: '买家企业',
                        templateCode: 'INF_TRD_TRADE_COMPLETED',
                        templateParam: '["00001"]',
                    }, ],
                },
                errorType: '',
                errorMsg: ''
            }
        }
    }
};
