'use strict';
/**
 * 收支 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.capital', {
        template: '<div ui-view></div>',
        url: '/capital',
        abstract: true
    })
    .state('main.capital.list', {
        url: '/',
        templateUrl: 'capital/views/capital-list.html',
        controller: 'CapitalListCtrl as vm',
    })    
    .state('main.capital.new', {
    	url: '/new',
        templateUrl: 'capital/views/capital-new.html',
        controller: 'CapitalNewCtrl as vm',
    })
    .state('main.capital.detail', {
        url: '/:id/detail',
        templateUrl: 'capital/views/capital-detail.html',
        controller: 'CapitalDetailCtrl as vm',
    })
    .state('main.capital.edit', {
        url: '/:id/edit',
        templateUrl: 'capital/views/capital-edit.html',
        controller: 'CapitalEditCtrl as vm',
    })
    .state('main.capital.delete', {
        url: '/:id/delete',
        templateUrl: 'capital/views/capital-detail.html',
        controller: 'CapitalDeleteCtrl as vm'
    });    
};
