/**
 * 控制器：收支详情
 */
'use strict';

function CapitalDetailCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'capitalFlow'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();
}

module.exports = {
    name: 'CapitalDetailCtrl',
    fn: CapitalDetailCtrl
};
