/**
 * 控制器：收支列表
 */
'use strict';



function CapitalListCtrl($controller, $filter, ApiSrv, MessageSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;
    vm.searchConditionBak = {};

    // 检索前处理
    function preSearchFn(apiParams) {
        apiParams.exportFlg = '0'; // 导出文件标志（0：不导出）
    	vm.searchConditionBak = angular.copy(vm.searchCondition);
    }

    let ctrlOpts = {
            modelName: 'capitalFlow',
            preSearchFn: preSearchFn
        };
    
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.export = function() {
        // ajax取导出报表数据
    	vm.searchConditionBak.exportFlg = '1'; // 导出文件标志（1：导出）
        return ApiSrv.exec('capitalFlow/getList', vm.searchConditionBak)
            .then(function(data) {
            	let exportData = [];
                if (data && data.list && data.list.length > 0) {
                    angular.forEach(data.list, function(capital, index) {
                    	let detail = {};
                    	detail.no = index + 1;
                    	detail.capitalFlowNumber = capital.capitalFlowNumber;
                    	detail.payerCompany = capital.payerCompanyName + '(' + capital.payerCompanyNumber + ')';
                    	detail.payeeCompany = capital.payeeCompanyName + '(' + capital.payeeCompanyNumber + ')';
                    	detail.capitalType = $filter('codeText')(capital.capitalType, 'capitalType');
                    	detail.tradeNumber = capital.tradeNumber;
                    	detail.bankCode = $filter('codeText')(capital.bankCode, 'bankCode');
                    	detail.amount = $filter('currency')(capital.amount, '');
                    	detail.currencyType = $filter('codeText')(capital.currencyType, 'currencyType');
                    	detail.status = $filter('codeText')(capital.status, 'flowStatus');
                    	detail.updateTime = $filter('dateFormat')(capital.updateTime);
                    	exportData.push(detail);
                    });
                    return exportData;
                } else {
                    MessageSrv.info('INF_COM_NO_SEARCH_RESULT');
                    return exportData;
                }
            });
    };
}

module.exports = {
    name: 'CapitalListCtrl',
    fn: CapitalListCtrl
};
