/**
 * 控制器：地区列表
 */
'use strict';

const _ = require('lodash');

function AreaListCtrl($scope, $controller, AreaSelectSrv, MessageSrv, ApiSrv, CodeList) {
    'ngInject';
    // 扩展自list控制器基类
    let vm = this;
    vm.areaList = [];
    
    // 地区一,二，三下拉框初期化
    AreaSelectSrv.initAreaList($scope, vm, 'searchCondition.level1', 'searchCondition.level2', null);
    
    function preSearchFn() {
    	vm.areaList = [];
    }
    
    function postSearchFn(data) {
        vm.areaListEdit(data);
    }
    
    let ctrlOpts = {
        modelName: 'area',
        preSearchFn: preSearchFn,
        postSearchFn: postSearchFn
    };
    
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
    
    vm.areaListEdit = function(list){
        list = list || [];
        _.map(list,function(item) {
            let singleArea = {};
            singleArea.id = item.id;
            singleArea.updateTime = item.updateTime;
            singleArea.status = item.status;
            singleArea.level = item.level;
            if (item.level === '3') {
                singleArea.threeLevelName = CodeList.area[item.id];
            } else if (item.level === '2') {
                singleArea.twoLevelName = CodeList.area[item.id];
            } else if (item.level === '1') {
                singleArea.oneLevelName = CodeList.area[item.id];
            }
            singleArea.updateTime = item.updateTime;
            vm.areaList.push(singleArea);
        });
    };
    
    let _areaStatusUpdate = function(area) {
        if (area.status === '0') {
        	area.status = '1';
        } else {
        	area.status = '0';
        }
        ApiSrv.exec('area/updateStatus', {areaId: area.id, status: area.status, updateTime: area.updateTime})
            .then(function() {
            	MessageSrv.success('message.updateOk');
            	vm.tableParams.reload();
            });
    };
    
    vm.areaStatusEdit = function(row){
        // 确认保存后执行
        MessageSrv.confirm('confirm.update').then(() => {
            // 执行保存
            _areaStatusUpdate(row);
	    });
    };
}

module.exports = {
    name: 'AreaListCtrl',
    fn: AreaListCtrl
};