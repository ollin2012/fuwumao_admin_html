'use strict';
/**
 * 地区 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.area', {
        template: '<div ui-view></div>',
        url: '/area',
        abstract: true
    })
    .state('main.area.list', {
        url: '/',
        templateUrl: 'area/views/area-list.html',
        controller: 'AreaListCtrl as vm',
    })    
    .state('main.area.new', {
    	url: '/new',
        templateUrl: 'area/views/area-new.html',
        controller: 'AreaNewCtrl as vm',
    })
    .state('main.area.detail', {
        url: '/:id/detail',
        templateUrl: 'area/views/area-detail.html',
        controller: 'AreaDetailCtrl as vm',
    })
    .state('main.area.edit', {
        url: '/:id/edit',
        templateUrl: 'area/views/area-edit.html',
        controller: 'AreaEditCtrl as vm',
    })
    .state('main.area.delete', {
        url: '/:id/delete',
        templateUrl: 'area/views/area-detail.html',
        controller: 'AreaDeleteCtrl as vm'
    });    
};
