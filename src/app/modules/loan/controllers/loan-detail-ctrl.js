/**
 * 控制器：放款详情
 */
'use strict';

function LoanDetailCtrl($controller, ApiSrv, MessageSrv) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'loan'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();

   	// 交易款平台支付 TODO
    vm.loan = function() {
    	// api参数设定
    	let loanParams = {
    			id: vm.model.id,
    			stagePrice: vm.model.stagePrice
    	};

        // ajax执行放款处理
        return ApiSrv.exec('tradeStage/adminPay', loanParams)
            .then(function() {             
                MessageSrv.success('message.loanOk');
                vm.back();
            });
    };
}

module.exports = {
    name: 'LoanDetailCtrl',
    fn: LoanDetailCtrl
};
