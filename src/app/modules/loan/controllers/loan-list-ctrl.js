/**
 * 控制器：放款列表
 */
'use strict';

function LoanListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'loan'
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));  
}

module.exports = {
    name: 'LoanListCtrl',
    fn: LoanListCtrl
};
