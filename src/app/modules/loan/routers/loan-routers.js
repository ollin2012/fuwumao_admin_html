'use strict';
/**
 * 放款 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.loan', {
        template: '<div ui-view></div>',
        url: '/loan',
        abstract: true
    })
    .state('main.loan.list', {
        url: '/',
        templateUrl: 'loan/views/loan-list.html',
        controller: 'LoanListCtrl as vm',
    })    
    .state('main.loan.new', {
    	url: '/new',
        templateUrl: 'loan/views/loan-new.html',
        controller: 'LoanNewCtrl as vm',
    })
    .state('main.loan.detail', {
        url: '/:id/detail',
        templateUrl: 'loan/views/loan-detail.html',
        controller: 'LoanDetailCtrl as vm',
    })
    .state('main.loan.edit', {
        url: '/:id/edit',
        templateUrl: 'loan/views/loan-detail.html',
        controller: 'LoanDetailCtrl as vm',
    })
    .state('main.loan.delete', {
        url: '/:id/delete',
        templateUrl: 'loan/views/loan-detail.html',
        controller: 'LoanDeleteCtrl as vm'
    });    
};
