'use strict';
/**
 * 聊天记录 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.adminConversation', {
        template: '<div ui-view></div>',
        url: '/adminConversation',
        abstract: true
    })
    .state('main.adminConversation.list', {
        url: '/',
        templateUrl: 'adminConversation/views/adminConversation-list.html',
        controller: 'AdminConversationListCtrl as vm',
    })
    .state('main.adminConversation.detail', {
        url: '/:id/detail',
        templateUrl: 'adminConversation/views/adminConversation-detail.html',
        controller: 'AdminConversationDetailCtrl as vm',
    });    
};
