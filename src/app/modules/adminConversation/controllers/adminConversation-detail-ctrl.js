/**
 * 控制器：聊天记录详情
 */
'use strict';

function AdminConversationDetailCtrl($stateParams, $controller, TableSrv) {
    'ngInject';

    let vm = this;
    let tableSrv;
    
    // 取得消息前处理
    let _preGetDetailFn = function(apiParams) {
    	apiParams.conversationId = $stateParams.id;
    	return apiParams;
    };

    // 检索前处理
    let _preSearchFn = function(apiParams) {
    	apiParams.conversationId = $stateParams.id;
    };

    let ctrlOptsDetail = {
            modelName: 'adminConversation',
            preGetDetailFn: _preGetDetailFn
    };

    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOptsDetail }));

   	vm.getDetail();

    vm.search = function() {
        vm.tableParams.page(1);
        vm.tableParams.reload();
    };

    // 初始化
    (function init() {
        vm.searchCondition = {};
        tableSrv = new TableSrv({
            searchCondition: vm.searchCondition,
            getDataUrl: 'adminConversation/getConversationMessage',
            preSearchFn:_preSearchFn,
            searchCount: '10',
            isNoHistory: true
        });
        vm.tableParams = tableSrv.create();
    })();

    // 取得当前索引
    vm.getIndex = function(index) {
        return (vm.tableParams.page() - 1) * vm.tableParams.count() + index + 1;
    };

    // 重置
    vm.reset = function() {
        vm.searchCondition = angular.copy({});
        tableSrv.reset(vm.searchCondition);
        vm.search();
    };
}

module.exports = {
    name: 'AdminConversationDetailCtrl',
    fn: AdminConversationDetailCtrl
};