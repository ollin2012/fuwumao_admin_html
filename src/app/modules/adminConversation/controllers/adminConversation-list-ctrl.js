/**
 * 控制器：聊天记录列表
 */
'use strict';

function AdminConversationListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'adminConversation'
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));  
}

module.exports = {
    name: 'AdminConversationListCtrl',
    fn: AdminConversationListCtrl
};
