'use strict';
/**
 * 后台组织 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.adminDepartment', {
            template: '<div ui-view></div>',
            url: '/adminDepartment',
            abstract: true
        })
        .state('main.adminDepartment.list', {
            url: '/',
            templateUrl: 'adminDepartment/views/adminDepartment-list.html',
            controller: 'AdminDepartmentListCtrl as vm',
        })
        .state('main.adminDepartment.new', {
            url: '/new',
            templateUrl: 'adminDepartment/views/adminDepartment-new.html',
            controller: 'AdminDepartmentNewCtrl as vm',
        })
        .state('main.adminDepartment.edit', {
            url: '/:id/edit',
            templateUrl: 'adminDepartment/views/adminDepartment-edit.html',
            controller: 'AdminDepartmentEditCtrl as vm',
        })
        .state('main.adminDepartment.delete', {
            url: '/:id/delete',
            templateUrl: 'adminDepartment/views/adminDepartment-detail.html',
            controller: 'AdminDepartmentDeleteCtrl as vm'
        })
        .state('main.adminDepartment.set', {
            url: '/:id/set',
            templateUrl: 'adminDepartment/views/adminDepartment-set.html',
            controller: 'AdminDepartmentSetCtrl as vm',
        });
};
