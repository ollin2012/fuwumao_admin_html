/**
 * 控制器：后台组织删除
 */
'use strict';

function AdminDepartmentDeleteCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminDepartment'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'AdminDepartmentDeleteCtrl',
    fn: AdminDepartmentDeleteCtrl
};
