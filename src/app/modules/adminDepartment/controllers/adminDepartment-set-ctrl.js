/**
 * 控制器：后台组织人员设定
 */
'use strict';

function AdminDepartmentSetCtrl($stateParams, $window, ApiSrv,MessageSrv) {
    'ngInject';
    let vm = this;
    vm.data = {};
    //当前组织id
    vm.data.dptId = $stateParams.id;
    //当前组织现有人员
    vm.dptUserNow = [];
    vm.dptUserNowSelected = [];
    //无组织人员列表
    vm.dptUserFree = [];
    vm.dptUserFreeSelected = [];
    // 初始化
    (function init() {
        //取得当前组织名
        ApiSrv.exec('adminDepartment/getDetail', {
                id: vm.data.dptId
            })
            .then(function(data) {
                if (data) {
                    vm.data.dptName = data.departmentName;
                }
            });
        //取得当前组织成员
        ApiSrv.exec('adminDepartmentUser/getList', {
        		departmentId: vm.data.dptId
            })
            .then(function(data) {
                if (data) {
                    vm.dptUserNow = data.list;
                }
            });
        //取得无组织成员
        ApiSrv.exec('adminUnDepartmentUser/getList', {})
            .then(function(data) {
                if (data) {
                    vm.dptUserFree = data.list;
                }
            });
    })();

    vm.add = function() {
        angular.forEach(vm.dptUserFreeSelected, function(user) {
            let idx = vm.dptUserFree.indexOf(user);
            if (idx !== -1) {
                vm.dptUserFree.splice(idx, 1);
                vm.dptUserNow.push(user);
            }
        });
    };

    vm.addAll = function() {
        angular.forEach(vm.dptUserFree, function(user) {
            vm.dptUserNow.push(user);
        });
        vm.dptUserFree.length = 0;
    };

    vm.remove = function() {
        angular.forEach(vm.dptUserNowSelected, function(user) {
            let idx = vm.dptUserNow.indexOf(user);
            if (idx !== -1) {
                vm.dptUserNow.splice(idx, 1);
                vm.dptUserFree.push(user);
            }
        });
    };

    vm.removeAll = function() {
        angular.forEach(vm.dptUserNow, function(user) {
            vm.dptUserFree.push(user);
        });
        vm.dptUserNow.length = 0;
    };

    vm.submit = function() {
        ApiSrv.exec('adminDepartmentUser/update', {
        		departmentId: vm.data.dptId,
                departmentUserList: vm.dptUserNow,
                unDepartmentUserList: vm.dptUserFree,
            })
            .then(function() {
                vm.back();
            });
    };
    vm.setUpdate = function(){
        //输入日期非空判断
        // 设定提示消息以及保存的函数
        let confirmMsg = 'confirm.update';
        // 确认保存后执行
        MessageSrv.confirm(confirmMsg).then(() => {
            // 执行保存
            vm.submit();
        });
    };
    vm.back = function() {
        $window.history.back();
    };
}

module.exports = {
    name: 'AdminDepartmentSetCtrl',
    fn: AdminDepartmentSetCtrl
};
