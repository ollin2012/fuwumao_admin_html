/**
 * 控制器：后台组织编辑
 */
'use strict';

function AdminDepartmentEditCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminDepartment',
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts}));

    vm.getDetail();
}

module.exports = {
    name: 'AdminDepartmentEditCtrl',
    fn: AdminDepartmentEditCtrl
};
