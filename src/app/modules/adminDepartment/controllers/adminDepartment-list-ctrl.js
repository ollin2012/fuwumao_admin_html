/**
 * 控制器：后台组织列表
 */
'use strict';

function AdminDepartmentListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'adminDepartment',
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
}

module.exports = {
    name: 'AdminDepartmentListCtrl',
    fn: AdminDepartmentListCtrl
};
