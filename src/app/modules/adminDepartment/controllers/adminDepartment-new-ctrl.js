/**
 * 控制器：后台组织新增
 */
'use strict';

function AdminDepartmentNewCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminDepartment',
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts }));
}

module.exports = {
    name: 'AdminDepartmentNewCtrl',
    fn: AdminDepartmentNewCtrl
};
