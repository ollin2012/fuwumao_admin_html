'use strict';
/**
 * 成员企业佣金 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.companyCommission', {
        template: '<div ui-view></div>',
        url: '/companyCommission',
        abstract: true
    })
    .state('main.companyCommission.list', {
        url: '/',
        templateUrl: 'companyCommission/views/companyCommission-list.html',
        controller: 'CompanyCommissionListCtrl as vm',
    })    
    .state('main.companyCommission.new', {
    	url: '/new',
        templateUrl: 'companyCommission/views/companyCommission-new.html',
        controller: 'CompanyCommissionNewCtrl as vm',
    })
    .state('main.companyCommission.detail', {
        url: '/:id/detail',
        templateUrl: 'companyCommission/views/companyCommission-detail.html',
        controller: 'CompanyCommissionDetailCtrl as vm',
    })
    .state('main.companyCommission.edit', {
        url: '/:id/edit',
        templateUrl: 'companyCommission/views/companyCommission-edit.html',
        controller: 'CompanyCommissionEditCtrl as vm',
    })
    .state('main.companyCommission.delete', {
        url: '/:id/delete',
        templateUrl: 'companyCommission/views/companyCommission-detail.html',
        controller: 'CompanyCommissionDeleteCtrl as vm'
    });    
};
