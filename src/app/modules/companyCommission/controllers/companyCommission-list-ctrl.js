/**
 * 控制器：成员企业佣金列表
 */
'use strict';

function CompanyCommissionListCtrl($controller, $filter, ApiSrv, MessageSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;
    vm.searchConditionBak = {};

    // 检索后处理
    function postSearchFn() {
    	vm.searchConditionBak = angular.copy(vm.searchCondition);
    }

    let ctrlOpts = {
            modelName: 'companyCommission',
            postSearchFn: postSearchFn,
            dataUrl: '/getDetail'
        };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.export = function() {
        // ajax取导出报表数据
        return ApiSrv.exec('companyCommission/export', vm.searchConditionBak)
            .then(function(data) {
            	let exportData = [];
                if (data && data.list && data.list.length > 0) {
                    angular.forEach(data.list, function(companyCommission, index) {
                    	let detail = {};
                    	detail.no = index + 1;
                    	detail.payedCompany = companyCommission.payedCompanyName + '(' + companyCommission.payedCompanyNumber + ')';
                    	detail.receivedCompany = companyCommission.name + '(' + companyCommission.number + ')';
                    	detail.tradeNumber = companyCommission.tradeName + '(' + companyCommission.tradeNumber + ')';
                    	detail.tradeStageName = companyCommission.tradeStageName;
                    	detail.commissionAmount = $filter('currency')(companyCommission.commissionAmount, '');
                    	detail.currencyType = $filter('codeText')(companyCommission.currencyType, 'currencyType');
                    	//detail.capitalFlowNumber = companyCommission.capitalFlowNumber;
                    	detail.happenTime = $filter('dateFormat')(companyCommission.happenTime, 'YYYY-MM-DD HH:mm');
                    	detail.commissionDiv = $filter('codeText')(companyCommission.commissionDiv, 'commissionDiv');
                    	detail.payFlg = $filter('codeText')(companyCommission.payFlg, 'payFlg');
                    	detail.payTime = $filter('dateFormat')(companyCommission.payTime, 'YYYY-MM-DD HH:mm');
                    	exportData.push(detail);
                    });
                    return exportData;
                } else {
                    MessageSrv.info('INF_COM_NO_SEARCH_RESULT');
                    return exportData;
                }
            });
    };
}

module.exports = {
    name: 'CompanyCommissionListCtrl',
    fn: CompanyCommissionListCtrl
};
