/**
 * 控制器：网站内容编辑
 */
'use strict';

function SiteContentEditCtrl($controller, CodeList, UploadSrv) {
    'ngInject';

    let vm = this;
    
    vm.siteContentShowTypeList = CodeList.siteContentShowType;

    // 设置上传组件
    let uploadFileParams = {fileType: '12'};
    vm.uploader = UploadSrv.createCkUploader(uploadFileParams);
    
    // 取得详细后处理
    let _postGetDetailFn = function(data) {
    	data.contentLanguage = data.language;
    	return data;
    };
    
    // 新增处理前处理
    let _preSaveFn = function() {
        if (vm.model.showType === '1') {
            if (!vm.model.content || vm.model.content === '') {
                return false;
            }
            vm.model.links = null;
            
        //选外链时只上传链接
        } else {
        	vm.model.content = null;
        }
        return true;
    };

    let ctrlOpts = {
        modelName: 'content',
        postGetDetailFn: _postGetDetailFn,
        preSaveFn: _preSaveFn
    };

    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'SiteContentEditCtrl',
    fn: SiteContentEditCtrl
};
