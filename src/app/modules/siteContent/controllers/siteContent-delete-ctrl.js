/**
 * 控制器：网站内容删除
 */
'use strict';

function SiteContentDeleteCtrl($controller) {
    'ngInject';

    let vm = this;
    let ctrlOpts = {
        modelName: 'content'
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'SiteContentDeleteCtrl',
    fn: SiteContentDeleteCtrl
};
