/**
 * 控制器：网站内容详情
 */
'use strict';

function SiteContentDetailCtrl($controller) {
    'ngInject';

    let vm = this;
    let ctrlOpts = {
        modelName: 'content'
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
    
   	vm.getDetail();
}

module.exports = {
    name: 'SiteContentDetailCtrl',
    fn: SiteContentDetailCtrl
};
