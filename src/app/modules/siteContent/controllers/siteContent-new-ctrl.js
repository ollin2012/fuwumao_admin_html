/**
 * 控制器：网站内容新增
 */
'use strict';

function SiteContentNewCtrl($scope, $controller, CodeList, UploadSrv) {
    'ngInject';

    let vm = this;

    // 设置上传组件
    let uploadFileParams = {fileType: '12'};
    vm.uploader = UploadSrv.createCkUploader(uploadFileParams);
    
    // 新增处理前处理
    let _preSaveFn = function() {
    	if (!vm.saveFlg) {
    		vm.saveFlg = true;
    	}
        if (vm.model.showType === '1') {
            if (!vm.model.content || vm.model.content === '') {
                return false;
            }
            vm.model.links = null;
            
        //选外链时只上传链接
        } else {
        	vm.model.content = null;
        }
        return true;
    };

    let ctrlOpts = {
        modelName: 'content',
        preSaveFn: _preSaveFn
    };

    vm.saveFlg = false;
    vm.siteContentShowTypeList = CodeList.siteContentShowType;
    vm.model = {};

    $scope.$watch('vm.model', function() {
        if(vm.model){
        	vm.model.showType = '1';
        }
    });
    
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
}

module.exports = {
    name: 'SiteContentNewCtrl',
    fn: SiteContentNewCtrl
};
