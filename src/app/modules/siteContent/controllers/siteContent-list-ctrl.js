/**
 * 控制器：网站内容列表
 */
'use strict';

function SiteContentListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;

    let ctrlOpts = {
            modelName: 'content'
    };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
}

module.exports = {
    name: 'SiteContentListCtrl',
    fn: SiteContentListCtrl
};
