'use strict';
/**
 * 网站内容 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.content', {
            template: '<div ui-view></div>',
            url: '/siteContent',
            abstract: true
        })
        .state('main.content.list', {
            url: '/',
            templateUrl: 'siteContent/views/siteContent-list.html',
            controller: 'SiteContentListCtrl as vm',
        })   
        .state('main.content.detail', {
            url: '/:id/detail',
            templateUrl: 'siteContent/views/siteContent-detail.html',
            controller: 'SiteContentDetailCtrl as vm',
        })
        .state('main.content.new', {
            url: '/new',
            templateUrl: 'siteContent/views/siteContent-new.html',
            controller: 'SiteContentNewCtrl as vm',
        })
        .state('main.content.edit', {
            url: '/:id/edit',
            templateUrl: 'siteContent/views/siteContent-edit.html',
            controller: 'SiteContentEditCtrl as vm',
        })
        .state('main.content.delete', {
            url: '/:id/delete',
            templateUrl: 'siteContent/views/siteContent-detail.html',
            controller: 'SiteContentDeleteCtrl as vm'
        });
};
