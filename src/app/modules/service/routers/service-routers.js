'use strict';
/**
 * 服务 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.service', {
        template: '<div ui-view></div>',
        url: '/service',
        abstract: true
    })
    .state('main.service.list', {
        url: '/',
        templateUrl: 'service/views/service-list.html',
        controller: 'ServiceListCtrl as vm',
    })
    .state('main.service.unhandledList', {
        url: '/unhandledList',
        templateUrl: 'service/views/service-list.html',
        controller: 'ServiceListCtrl as vm',
    })
    .state('main.service.detail', {
        url: '/:id/detail',
        templateUrl: 'service/views/service-detail.html',
        controller: 'ServiceDetailCtrl as vm',
    })
    .state('main.service.verify', {
        url: '/:id/verify/:serviceInfoDiv',
        templateUrl: 'service/views/service-detail.html',
        controller: 'ServiceDetailCtrl as vm',
    });
};
