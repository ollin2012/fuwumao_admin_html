/**
 * 控制器：服务详情
 */
'use strict';

const _ = require('lodash');

function ServiceDetailCtrl($state, $stateParams, $uibModal, $window, ApiSrv, MessageSrv, CommonConstants) {
    'ngInject';

    let vm = this;
    vm.model = {};

    // 根据迁移元画面取得当前处理的Action类别
    vm.action = _.last(_.split($state.current.name, '.'));

    //服务详情取得
    ApiSrv.exec('service/getDetail', {
    		serviceId: $stateParams.id,
            serviceInfoDiv: $stateParams.serviceInfoDiv //null:服务情报(正式) 1:最新服务情报取得(可能履历)
        })
        .then(function(data) {
            vm.model = data;
        });

    //服务审核履历一览取得
    ApiSrv.exec('serviceVerify/getList', { serviceId: $stateParams.id })
        .then(function(data) {
            vm.serviceVerifyList = data.list;
        });

    //快速选择审核结果
    vm.serviceVerifyResult = {};
    vm.serviceResult = CommonConstants.serviceResult;
    vm.selectServiceResult = function(idx) {
        vm.serviceVerifyResult.verifyResultDetail = CommonConstants.serviceResultDetail[idx];
    };

    //提交审核结果
    vm.submitServiceVerify = function() {
        if (!vm.serviceVerifyList || !vm.serviceVerifyList.length) {
            return;
        }
        //弹出框确认
        MessageSrv.confirm('confirm.verify').then(function() {
            ApiSrv.exec('serviceVerify/update', {
                serviceVerifyId: vm.serviceVerifyList[0].id,
                verifyResultFlg: vm.serviceVerifyResult.verifyResultFlg,
                verifyResultDetail: vm.serviceVerifyResult.verifyResultDetail,
                updateTime: vm.serviceVerifyList[0].updateTime,
            }).then(function() {
                MessageSrv.success('message.verifyOk');
                $state.reload();
            });
        });
    };

    //回退
    vm.back = function() {
        if ($window.history.length > 1) {
            $window.history.back();
        } else {
            $state.go('main.service.list');
        }
    };

    //查看交易
    vm.goTradeList = function() {
        $state.go('main.trade.list', {
            serviceId: vm.model.serviceId,
        });
    };

    // 服务下架
    vm.shelvesModal = function(vm) {
		let modalParams = {
				serviceId: vm.model.serviceId,
				serviceName: vm.model.serviceName,
				updateTime: vm.model.updateTime
		};

		let shelvesModel = $uibModal.open({
			templateUrl : 'service/views/service-shelves.html',
			size: 'lg',
			controller: 'ServiceShelvesCtrl as vmShelves',
			resolve: {
				modalParams: function(){
					return modalParams;
				}
			}
		});

		shelvesModel.result.then(function() {
            MessageSrv.success('message.shelveOk');
            // 画面刷新
            $state.reload();
        }, function(){});
    };
}

module.exports = {
    name: 'ServiceDetailCtrl',
    fn: ServiceDetailCtrl
};
