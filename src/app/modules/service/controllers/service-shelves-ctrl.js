/**
 * 控制器：企业下架
 */
'use strict';

function ServiceShelvesCtrl(ApiSrv, MessageSrv, modalParams, $uibModalInstance) {
    'ngInject';

    let vmShelves = this;
    
    // 画面初期化
    vmShelves.serviceId = modalParams.serviceId;
    vmShelves.serviceName = modalParams.serviceName;
    vmShelves.updateTime = modalParams.updateTime;
    
    // 确认按钮按下(服务下架)
    vmShelves.offShelves = function() {
        // 弹出框确认
        MessageSrv.confirm('confirm.shelve').then(function() {
            ApiSrv.exec('service/shelvesCtrl', {
                serviceId: vmShelves.serviceId,
                updateTime: vmShelves.updateTime,
                offShelvesReason: vmShelves.offShelvesReason
            }).then(function() {
            	$uibModalInstance.close();
            });
        });
    };
}

module.exports = {
    name: 'ServiceShelvesCtrl',
    fn: ServiceShelvesCtrl
};