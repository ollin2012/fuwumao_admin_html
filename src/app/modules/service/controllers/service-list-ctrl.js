/**
 * 控制器：服务列表
 */
'use strict';

const _ = require('lodash');

function ServiceListCtrl($scope, $state, $controller, IndustrySelectSrv, AreaSelectSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'service',
        };

    // 取得当前处理的Action类别
    vm.action = _.last(_.split($state.current.name, '.'));

    // 待处理服务设定检索前处理
    if (vm.action === 'unhandledList') {
        ctrlOpts.preSearchFn = function(apiParams) {
            apiParams.serviceStatus = '2'; //待审核
            apiParams.dealDiv = '1'; //0：非待处理服务一览；1：待处理服务一览
            vm.searchCondition.serviceStatus = '2';
        };
    // 服务设定检索前处理
    } else {
        ctrlOpts.preSearchFn = function(apiParams) {
            apiParams.dealDiv = '0'; //0：非待处理服务一览；1：待处理服务一览
        };
    }

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    // 行业一,二下拉框初期化
    IndustrySelectSrv.initIndustryList($scope, vm, 'searchCondition.industryId1', 'searchCondition.industryId2');
    // 地区一,二，三下拉框初期化
    AreaSelectSrv.initAreaList($scope, vm, 'searchCondition.areaId1', 'searchCondition.areaId2', 'searchCondition.areaId3');
}

module.exports = {
    name: 'ServiceListCtrl',
    fn: ServiceListCtrl
};
