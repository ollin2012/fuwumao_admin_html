/**
 * 控制器：投诉列表
 */
'use strict';

function ComplaintListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'tradeComplaint'
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));  
}

module.exports = {
    name: 'ComplaintListCtrl',
    fn: ComplaintListCtrl
};
