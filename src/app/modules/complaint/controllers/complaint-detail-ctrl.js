/**
 * 控制器：投诉详情
 */
'use strict';

function ComplaintDetailCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'tradeComplaint'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();
}

module.exports = {
    name: 'ComplaintDetailCtrl',
    fn: ComplaintDetailCtrl
};
