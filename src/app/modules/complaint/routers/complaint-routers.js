'use strict';
/**
 * 投诉 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.complaint', {
        template: '<div ui-view></div>',
        url: '/complaint',
        abstract: true
    })
    .state('main.complaint.list', {
        url: '/',
        templateUrl: 'complaint/views/complaint-list.html',
        controller: 'ComplaintListCtrl as vm',
    })    
    .state('main.complaint.new', {
    	url: '/new',
        templateUrl: 'complaint/views/complaint-new.html',
        controller: 'ComplaintNewCtrl as vm',
    })
    .state('main.complaint.detail', {
        url: '/:id/detail',
        templateUrl: 'complaint/views/complaint-detail.html',
        controller: 'ComplaintDetailCtrl as vm',
    })
    .state('main.complaint.edit', {
        url: '/:id/edit',
        templateUrl: 'complaint/views/complaint-edit.html',
        controller: 'ComplaintEditCtrl as vm',
    })
    .state('main.complaint.delete', {
        url: '/:id/delete',
        templateUrl: 'complaint/views/complaint-detail.html',
        controller: 'ComplaintDeleteCtrl as vm'
    });    
};
