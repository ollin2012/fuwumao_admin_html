/**
 * 控制器：行业列表
 */
'use strict';

const _ = require('lodash');

function IndustryListCtrl($scope, $controller, IndustrySelectSrv, CodeList, MessageSrv, ApiSrv) {
    'ngInject';

    let vm = this;
    vm.industryList = [];
    
    // 行业一,二下拉框初期化
    IndustrySelectSrv.initIndustryList($scope, vm, 'searchCondition.level', null);
    
    // 检索前处理
    function preSearchFn() {
    	vm.industryList = [];
    }
    
    // 检索后处理
    function postSearchFn(data) {
        vm.industryListEdit(data);
    }
    
    let ctrlOpts = {
            modelName: 'industry',
            preSearchFn: preSearchFn,
            postSearchFn: postSearchFn
    };
    
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
    
    vm.industryListEdit = function(list){
        list = list || [];
        _.map(list,function(item) {
            let singleIndustry = {};
            singleIndustry.id = item.id;
            singleIndustry.updateTime = item.updateTime;
            singleIndustry.status = item.status;
            singleIndustry.level = item.level;
            if (item.level === '2') {
                singleIndustry.twoLevelName = CodeList.industry[item.id];
            } else if (item.level === '1') {
                singleIndustry.oneLevelName = CodeList.industry[item.id];
            }
            singleIndustry.updateTime = item.updateTime;
            vm.industryList.push(singleIndustry);
        });
    };
    
    let _industryStatusUpdate = function(industry){
        if (industry.status === '0') {
        	industry.status = '1';
        } else {
        	industry.status = '0';
        }
        ApiSrv.exec('industry/update', {industryId: industry.id, status: industry.status, updateTime: industry.updateTime})
            .then(function() {
            	MessageSrv.success('message.updateOk');
            	vm.tableParams.reload();
            });
    };
    
    vm.industryStatusEdit = function(row){
        // 确认保存后执行
        MessageSrv.confirm('confirm.update').then(() => {
            // 执行保存
            _industryStatusUpdate(row);
	    });
    };
}

module.exports = {
    name: 'IndustryListCtrl',
    fn: IndustryListCtrl
};
