'use strict';
/**
 * 行业 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.industry', {
        template: '<div ui-view></div>',
        url: '/industry',
        abstract: true
    })
    .state('main.industry.list', {
        url: '/',
        templateUrl: 'industry/views/industry-list.html',
        controller: 'IndustryListCtrl as vm',
    })    
    .state('main.industry.new', {
    	url: '/new',
        templateUrl: 'industry/views/industry-new.html',
        controller: 'IndustryListCtrl as vm',
    })
    .state('main.industry.detail', {
        url: '/:id/detail',
        templateUrl: 'industry/views/industry-detail.html',
        controller: 'IndustryListCtrl as vm',
    })
    .state('main.industry.edit', {
        url: '/:id/edit',
        templateUrl: 'industry/views/industry-edit.html',
        controller: 'IndustryListCtrl as vm',
    })
    .state('main.industry.delete', {
        url: '/:id/delete',
        templateUrl: 'industry/views/industry-detail.html',
        controller: 'industryDeleteCtrl as vm'
    });    
};
