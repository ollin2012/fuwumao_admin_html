/**
 * 控制器：保证金列表
 */
'use strict';

function DepositListCtrl($controller, $filter, ApiSrv, MessageSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;
    vm.searchConditionBak = {};

    // 检索前处理
    function preSearchFn() {
    	vm.searchConditionBak = angular.copy(vm.searchCondition);
    }

    let ctrlOpts = {
            modelName: 'agencyDeposit',
            preSearchFn: preSearchFn
        };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.export = function() {
        // ajax取导出报表数据
        return ApiSrv.exec('agencyDeposit/export', vm.searchConditionBak)
            .then(function(data) {
            	let exportData = [];
                if (data && data.list && data.list.length > 0) {
                    angular.forEach(data.list, function(deposit, index) {
                    	let detail = {};
                    	detail.no = index + 1;
                    	detail.agencyName = deposit.agencyName;
                    	detail.agencyCompanyNumber = deposit.agencyCompanyNumber;
                    	detail.tradNumber = deposit.tradNumber;
                    	detail.depositType = $filter('codeText')(deposit.depositType, 'depositType');
                    	detail.depositAmount = $filter('currency')(deposit.depositAmount, '');
                    	detail.currencyType = $filter('codeText')(deposit.currencyType, 'currencyType');
                    	detail.depositChangeTime = $filter('dateFormat')(deposit.depositChangeTime, 'YYYY-MM-DD HH:mm');
                    	detail.capitalFlowNumber = deposit.capitalFlowNumber;
                    	detail.capitalChangeType = $filter('codeText')(deposit.capitalChangeType, 'capitalChangeType');
                    	detail.frozenFlg = $filter('codeText')(deposit.frozenFlg, 'frozenFlg');
                    	exportData.push(detail);
                    });
                    return exportData;
                } else {
                    MessageSrv.info('INF_COM_NO_SEARCH_RESULT');
                    return exportData;
                }
            });
    };
}

module.exports = {
    name: 'DepositListCtrl',
    fn: DepositListCtrl
};
