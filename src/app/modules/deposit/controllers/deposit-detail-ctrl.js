/**
 * 控制器：保证金详情
 */
'use strict';

function DepositDetailCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'agencyDeposit'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();
}

module.exports = {
    name: 'DepositDetailCtrl',
    fn: DepositDetailCtrl
};
