'use strict';
/**
 * 保证金 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.deposit', {
        template: '<div ui-view></div>',
        url: '/deposit',
        abstract: true
    })
    .state('main.deposit.list', {
        url: '/',
        templateUrl: 'deposit/views/deposit-list.html',
        controller: 'DepositListCtrl as vm',
    })    
    .state('main.deposit.new', {
    	url: '/new',
        templateUrl: 'deposit/views/deposit-new.html',
        controller: 'DepositNewCtrl as vm',
    })
    .state('main.deposit.detail', {
        url: '/:id/detail',
        templateUrl: 'deposit/views/deposit-detail.html',
        controller: 'DepositDetailCtrl as vm',
    })
    .state('main.deposit.edit', {
        url: '/:id/edit',
        templateUrl: 'deposit/views/deposit-edit.html',
        controller: 'DepositEditCtrl as vm',
    })
    .state('main.deposit.delete', {
        url: '/:id/delete',
        templateUrl: 'deposit/views/deposit-detail.html',
        controller: 'DepositDeleteCtrl as vm'
    });    
};
