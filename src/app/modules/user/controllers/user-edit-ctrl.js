/**
 * 控制器：用户编辑
 */
'use strict';

function UserEditCtrl($controller, UserSrv,ApiSrv,MessageSrv,$state) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'user',
            postGetDetailFn: UserSrv.postGetDetailFn(vm)
        };
    //angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts}));
    vm.prePassword = '';
    vm.getSelfDetail = function() {
        ApiSrv.exec('adminUser/getSelfDetail', {})
            .then(function(data) {
                if (ctrlOpts.postGetDetailFn) {
                    vm.model = ctrlOpts.postGetDetailFn(data);
                }  else {
                    vm.model = data;
                }
                vm.prePassword = vm.model.password;
                vm.preMobile = vm.model.mobile;
            });
    };
    vm.passWordFlg = false;
    vm.rePassword = '';
    vm.passWordUpdate = function(){
        if(vm.passWordFlg===false) {
            vm.passWordFlg = true;
        }else{
            vm.passWordFlg = false;
            vm.model.password = vm.prePassword;
            vm.rePassword = '';
        }
    };
    vm.userUpdate = function(){
        //输入日期非空判断
        let noUpdate = 'message.noUpdate';
        if (vm.model.password === vm.prePassword && vm.model.mobile === vm.preMobile) {
            MessageSrv.error(noUpdate);
            return true;
        }
        let confirmMsg;
        // 设定提示消息以及保存的函数
        confirmMsg = 'confirm.update';
        // 确认保存后执行
        MessageSrv.confirm(confirmMsg).then(() => {
            // 前处理hook
            if (ctrlOpts.preSaveFn) {
            (ctrlOpts.preSaveFn()).then(result => {
                if (result === undefined || result) {
                UserSrv.userSaveForEdit(vm,ctrlOpts);
                $state.reload();
            }
        });
        } else {
            // 执行保存
            UserSrv.userSaveForEdit(vm,ctrlOpts);
            $state.reload();
        }
        });
    };
    UserSrv.initDetail(vm);
    vm.getSelfDetail();
    UserSrv.initChooseRoleModal(vm);
}

module.exports = {
    name: 'UserEditCtrl',
    fn: UserEditCtrl
};
