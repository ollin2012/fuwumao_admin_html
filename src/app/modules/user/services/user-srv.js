'use strict';

const _ = require('lodash');
/**
 * 管理员用户
 */
function UserSrv(CodeList, $uibModal, ApiSrv,MessageSrv) {
    'ngInject';
    //一览画面角色显示
    function convRoleForListView(roleList) {
        return _.map(roleList, 'roleName').join(',');
    }

    // 取得详情后处理
    function postGetDetailFn(vm) {  
        function _fn(model) {
            vm.fullRoleName = convRoleForListView(model.roleList);
            model.roleList = _.map(model.roleList, 'roleId');
            return model;            
        }
        return _fn;
    }
    function _apiParamEdit(vm){
        vm.persenalInfo = {};
        vm.persenalInfo.mobile = vm.model.mobile;
        vm.persenalInfo.password = vm.model.password;
        vm.persenalInfo.updateTime =vm.model.updateTime;
    }
    //全部门名称列表以及全角色列表初期取得
    function initDetail(vm) {
        // 下拉框数据取得
        // 角色
        ApiSrv.exec('adminUserRole/getList', {})
            .then(function(data) {
                if (data) {
                    vm.allRolesList = data.list;
                }
            });

        // 部门
        ApiSrv.exec('adminDepartment/getList', {})
            .then(function(data) {
                if (data) {
                    vm.allDepartmentsList = data.list;
                }
            });     
    }
    function userSaveForEdit(vm,ctrlOpts) {
        _apiParamEdit(vm);
        ApiSrv.exec('adminUser/personalInfoUpdate', vm.persenalInfo)
            .then(function() {
                if (ctrlOpts.postSaveFn) {
                    ctrlOpts.postSaveFn();
                } else {
                    MessageSrv.success('message.updateOk');
                }
            });
    }
    //角色选择弹出多选框处理
    function initChooseRoleModal(vm) {
        vm.roleChoose = function() {
            var roleChooseModel = $uibModal.open({
                templateUrl: 'adminUser/views/adminUser-chooseRole.html',
                controller: function($scope) {
                    $scope.roleList = _.cloneDeep(vm.allRolesList);
                    let selectedRoleList = vm.model.roleList || [];
                    _.forEach($scope.roleList, role => {
                        if (selectedRoleList.indexOf(role.roleId) >= 0) {
                            role.value = true;
                        }
                    });            

                    $scope.selectAll = function() {
                        for (let idx in $scope.roleList) {
                            $scope.roleList[idx].value = $scope.isSeletedAll;
                        }                        
                    };
                }
            });
            roleChooseModel.result.then(function(roleList) {
                let selectedRole = _.filter(roleList, 'value');
                vm.model.roleList = _.map(selectedRole, 'roleId');
                vm.fullRoleName = convRoleForListView(selectedRole);
            }, function(){});
        };
    }

    return {
        convRoleForListView,
        initChooseRoleModal,
        postGetDetailFn,
        initDetail,
        userSaveForEdit
    };
}
module.exports = {
    name: 'UserSrv',
    fn: UserSrv
};
