/**
 * 控制器：交易评价编辑
 */
'use strict';

function TradeEvaluationEditCtrl($window, tradeEvaluationSrv) {
    'ngInject';

    let vm = this;
    vm.model = {};
    //是否编辑画面
    vm.isEdit = true;
    //企业评价是否可编辑
    vm.isEditCompany = false;
    //服务商评价是否可编辑
    vm.isEditAgency = false;

    //点击画面编辑按钮
    vm.startEditCompany = function() {
        vm.isEditCompany = true;
        vm.companyEvaBackup = angular.copy(vm.companyEvaluation);
    };
    vm.startEditAgency = function() {
        vm.isEditAgency = true;
        vm.agencyEvaBackup = angular.copy(vm.agencyEvaluation);
    };
    //取消编辑
    vm.cancelEditCompany = function() {
        vm.isEditCompany = false;
        vm.companyEvaluation = angular.copy(vm.companyEvaBackup);
    };
    vm.cancelEditAgency = function() {
        vm.isEditAgency = false;
        vm.agencyEvaluation = angular.copy(vm.agencyEvaBackup);
    };

    vm.back = function() {
        $window.history.back();
    };

    vm.update = function() {
        tradeEvaluationSrv.update(vm);
    };

    tradeEvaluationSrv.getDetail(vm);
}

module.exports = {
    name: 'TradeEvaluationEditCtrl',
    fn: TradeEvaluationEditCtrl
};
