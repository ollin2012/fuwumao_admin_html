/**
 * 控制器：交易评价详情
 */
'use strict';

function TradeEvaluationDetailCtrl($window, tradeEvaluationSrv) {
    'ngInject';

    let vm = this;
    vm.model = {};
    vm.isEdit = false;
    
    vm.back = function() {
        $window.history.back();
    };

    tradeEvaluationSrv.getDetail(vm);
}

module.exports = {
    name: 'TradeEvaluationDetailCtrl',
    fn: TradeEvaluationDetailCtrl
};
