/**
 * 控制器：交易评价列表
 */
'use strict';

function TradeEvaluationListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'tradeEvaluation'
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));  
}

module.exports = {
    name: 'TradeEvaluationListCtrl',
    fn: TradeEvaluationListCtrl
};
