'use strict';
/**
 * 交易评价 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.tradeEvaluation', {
        template: '<div ui-view></div>',
        url: '/tradeEvaluation',
        abstract: true
    })
    .state('main.tradeEvaluation.list', {
        url: '/',
        templateUrl: 'tradeEvaluation/views/tradeEvaluation-list.html',
        controller: 'TradeEvaluationListCtrl as vm',
    })
    .state('main.tradeEvaluation.detail', {
        url: '/:id/detail',
        templateUrl: 'tradeEvaluation/views/tradeEvaluation-detail.html',
        controller: 'TradeEvaluationDetailCtrl as vm',
    })
    .state('main.tradeEvaluation.edit', {
        url: '/:id/edit',
        templateUrl: 'tradeEvaluation/views/tradeEvaluation-detail.html',
        controller: 'TradeEvaluationEditCtrl as vm',
    });
};
