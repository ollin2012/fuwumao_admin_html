'use strict';

//const _ = require('lodash');
/**
 * 交易评价服务
 */
function tradeEvaluationSrv($stateParams, ApiSrv, MessageSrv) {
    'ngInject';

    function updateFn(evaluation) {
        return ApiSrv.exec('tradeEvaluation/update', {
            id: evaluation.id,
            evaluationScore: evaluation.evaluationScore,
            evaluationContent: evaluation.evaluationContent,
            evaluationReplyContent: evaluation.evaluationReplyContent,
            updateTime: evaluation.updateTime,
        });
    }

    // 更新
    function update(vm) {
        // 确认是否保存
        MessageSrv.confirm('confirm.update').then(function() {
            if (vm.isEditCompany) {
                updateFn(vm.companyEvaluation).then(function() {
                    MessageSrv.success('message.updateOk');
                    vm.back();
                });
            }
            if (vm.isEditAgency) {
                updateFn(vm.agencyEvaluation).then(function() {
                    MessageSrv.success('message.updateOk');
                    vm.back();
                });
            }
        });
    }

    //取得详情
    function getDetail(vm) {
        vm.trade = {};
        vm.companyEvaluation = {};
        vm.agencyEvaluation = {};
        //取得交易详情
        ApiSrv.exec('trade/getDetail', { tradeId: $stateParams.id }).then(function(data) {
            vm.trade = data;
        });
        //取得交易评价
        ApiSrv.exec('trade/getEvaluation', { tradeId: $stateParams.id }).then(function(data) {
            angular.forEach(data.list, function(eva) {
                //企业评价evaluationType='1'
                if (eva.evaluationType === '1') {
                    vm.companyEvaluation = eva;
                }
                if (eva.evaluationType === '2') {
                    vm.agencyEvaluation = eva;
                }
            });
        });
    }
    return {
        update,
        getDetail,
    };
}

module.exports = {
    name: 'tradeEvaluationSrv',
    fn: tradeEvaluationSrv
};
