'use strict';

const _ = require('lodash');
/**
 * 角色服务
 */
function AdminRoleSrv(CodeList) {
    'ngInject';

    function convActionForView(actions) {
        actions = actions || [];
        return _.map(CodeList.roleAction, function(value, key) {
            let selected = (actions.indexOf(key) >= 0);
            return {
                name: key,
                text: value,
                selected: selected
            };
        });
    }

    function convActionForSave(actionList) {
        return _
            .chain(actionList)
            .filter(o => o.selected)
            .map(o => o.name)
            .value();
    }

    function convActionForListView(actions) {
        actions = actions || [];
        if (actions.length > 4) {
        	actions = _.dropRight(actions, actions.length - 4);
        	let judgeActions = _.map(actions, key => CodeList.roleAction[key]).join();
        	return judgeActions + '...';
        } else {
            return _.map(actions, key => CodeList.roleAction[key])
            .join();
        }
    }

    function convAllActionForMultiSelect() {
        return _.map(CodeList.roleAction, function(value, key) {
            return {
                id: key,
                label: value
            };
        });
    }    

    return {
        convActionForView,
        convActionForSave,
        convActionForListView,
        convAllActionForMultiSelect
    };
}

module.exports = {
    name: 'AdminRoleSrv',
    fn: AdminRoleSrv
};
