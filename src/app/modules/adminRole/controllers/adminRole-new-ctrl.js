/**
 * 控制器：角色新增
 */
'use strict';

function AdminRoleNewCtrl($controller, AdminRoleSrv) {
    'ngInject';

    let vm = this;

    // 更新前处理
    function preSaveFn() {
        // 编辑权限列表成为字符串数组形式
        vm.model.roleActionList = AdminRoleSrv.convActionForSave(vm.actionList);
    }

    let ctrlOpts = {
        modelName: 'adminUserRole',
        preSaveFn
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts }));
    vm.selectAll = function() {
        for (let idx in vm.actionList) {
            vm.actionList[idx].selected = vm.isSeletedAll;
        }
    };
    // 编辑权限列表成为画面表示形式
    vm.actionList = AdminRoleSrv.convActionForView();
}

module.exports = {
    name: 'AdminRoleNewCtrl',
    fn: AdminRoleNewCtrl
};
