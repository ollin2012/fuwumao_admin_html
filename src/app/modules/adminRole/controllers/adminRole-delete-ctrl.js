/**
 * 控制器：角色删除
 */
'use strict';

function AdminRoleDeleteCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminUserRole'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'AdminRoleDeleteCtrl',
    fn: AdminRoleDeleteCtrl
};
