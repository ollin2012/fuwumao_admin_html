/**
 * 控制器：角色列表
 */
'use strict';

let _ = require('lodash');

function AdminRoleListCtrl($controller, AdminRoleSrv, MultiselectSrv) {
    'ngInject';

    let vm = this;
    vm.resetFlg = false;
    // 检索前处理
    function preSearchFn(apiParams) {
        if(!vm.resetFlg){
            apiParams.roleActionList = MultiselectSrv.toApi(vm.roleActionList);
        }else {
            vm.roleActionList = [];
        }
    }

    // 检索后处理
    function postSearchFn(list) {
    	_.forEach(list, row => {
    		row.actionNameList = AdminRoleSrv.convActionForListView(row.roleActionList);
    	});
        vm.resetFlg = false;
    }

    // 扩展自list控制器基类
    let ctrlOpts = {
            modelName: 'adminUserRole',
            preSearchFn: preSearchFn,
            postSearchFn: postSearchFn
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));  

    // 初始化
    // 选择的权限多选框
    vm.roleActionList = MultiselectSrv.toVm(vm.searchCondition.roleActionList);

    // 所有的权限列表
    vm.allActionList = AdminRoleSrv.convAllActionForMultiSelect();
}

module.exports = {
    name: 'AdminRoleListCtrl',
    fn: AdminRoleListCtrl
};
