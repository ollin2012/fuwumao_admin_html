/**
 * 控制器：角色详情
 */
'use strict';

function AdminRoleDetailCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminUserRole'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();
}

module.exports = {
    name: 'AdminRoleDetailCtrl',
    fn: AdminRoleDetailCtrl
};
