/**
 * 控制器：角色编辑
 */
'use strict';

function AdminRoleEditCtrl($controller, AdminRoleSrv) {
    'ngInject';

    let vm = this;

    // 取得详情后处理
    function postGetDetailFn(data) {
        vm.actionList = AdminRoleSrv.convActionForView(data.roleActionList);
        return data;
    }

    // 更新前处理
    function preSaveFn() {
        // 编辑权限列表成为字符串数组形式
        vm.model.roleActionList = AdminRoleSrv.convActionForSave(vm.actionList);
    }

    // 扩展基类
    let ctrlOpts = {
            modelName: 'adminUserRole',
            postGetDetailFn,
            preSaveFn            
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts}));
    vm.selectAll = function() {
        for (let idx in vm.actionList) {
            vm.actionList[idx].selected = vm.isSeletedAll;
        }
    };
    // 取得详情
    vm.getDetail();
}

module.exports = {
    name: 'AdminRoleEditCtrl',
    fn: AdminRoleEditCtrl
};
