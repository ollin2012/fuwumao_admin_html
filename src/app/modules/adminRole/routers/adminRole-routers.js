'use strict';
/**
 * 角色 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.adminUserRole', {
        template: '<div ui-view></div>',
        url: '/adminRole',
        abstract: true
    })
    .state('main.adminUserRole.list', {
        url: '/',
        templateUrl: 'adminRole/views/adminRole-list.html',
        controller: 'AdminRoleListCtrl as vm',
    })    
    .state('main.adminUserRole.new', {
    	url: '/new',
        templateUrl: 'adminRole/views/adminRole-new.html',
        controller: 'AdminRoleNewCtrl as vm',
    })
    .state('main.adminUserRole.detail', {
        url: '/:id/detail',
        templateUrl: 'adminRole/views/adminRole-detail.html',
        controller: 'AdminRoleDetailCtrl as vm',
    })
    .state('main.adminUserRole.edit', {
        url: '/:id/edit',
        templateUrl: 'adminRole/views/adminRole-edit.html',
        controller: 'AdminRoleEditCtrl as vm',
    })
    .state('main.adminUserRole.delete', {
        url: '/:id/delete',
        templateUrl: 'adminRole/views/adminRole-detail.html',
        controller: 'AdminRoleDeleteCtrl as vm'
    });    
};
