'use strict';
/**
 * 关键字 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.keywords', {
        template: '<div ui-view></div>',
        url: '/keywords',
        abstract: true
    })
    .state('main.keywords.list', {
        url: '/',
        templateUrl: 'keywords/views/keywords-list.html',
        controller: 'KeywordsListCtrl as vm',
    });
};
