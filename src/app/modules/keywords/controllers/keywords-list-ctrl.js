/**
 * 控制器：关键字列表
 */
'use strict';

function KeywordsListCtrl($controller, $scope) {
    'ngInject';

    //默认按日搜索
    let defaultCondition = {
    		displayType: '1'
    };

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'keywords',
            //检索前加日期参数
            preSearchFn: function(apiParams) {
                if (vm.searchDate.year && vm.searchDate.year !== '') {
                    apiParams.searchDate = '' + vm.searchDate.year;
                    if (vm.searchDate.month && vm.searchDate.month !== '') {
                        if (vm.searchDate.month < 10) {
                            apiParams.searchDate += '-0' + vm.searchDate.month;
                        } else {
                        	apiParams.searchDate += '-' + vm.searchDate.month;
                        }
                        if (vm.searchCondition.displayType === '1' && vm.searchDate.day && vm.searchDate.day !== '') {
                            if (vm.searchDate.day < 10) {
                                apiParams.searchDate += '-0' + vm.searchDate.day;
                            } else {
                            	apiParams.searchDate += '-' + vm.searchDate.day;
                            }
                        }
                    }
                }
            },
            defaultCondition: defaultCondition
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    //默认按日搜索
    vm.searchCondition.displayType = '1';
    vm.searchDate = {};
    //初始化下拉框选项
    vm.searchDateOptions = {
        year: [],
        month: [],
        day: [],
    };
    for (let i = 2012; i <= 2022; i++) {
        vm.searchDateOptions.year.push(i);
    }

    //取得月份最大日数
    let maxDayArr = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    function getMaxDay(year, month) {
        if (month === 2 && year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) {
            return 29;
        } else {
            return maxDayArr[month - 1];
        }
    }

    function setMonthOption() {
        vm.searchDateOptions.month.length = 0;
        if (!vm.searchDate.year || vm.searchDate.year === '') {
            return;
        }
        for (let i = 1; i <= 12; i++) {
            vm.searchDateOptions.month.push(i);
        }
    }

    function setDayOption() {
        vm.searchDateOptions.day.length = 0;
        if (!vm.searchDate.month || vm.searchDate.month === '') {
            return;
        }
        let maxDay = getMaxDay(vm.searchDate.year, vm.searchDate.month);
        for (let i = 1; i <= maxDay; i++) {
            vm.searchDateOptions.day.push(i);
        }
    }

    //监控年月选项
    $scope.$watch('vm.searchDate.year', function(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        setMonthOption();
        setDayOption();
    });
    $scope.$watch('vm.searchDate.month', function(newVal, oldVal) {
        if (newVal === oldVal || vm.searchCondition.displayType === '2') {
            return;
        }
        setDayOption();
    });

    vm.myReset = function() {
        vm.searchDate = {};
        delete vm.searchCondition.targetType;
        delete vm.searchCondition.keyword;
        vm.searchCondition.displayType = '1';
        vm.tableParams.page(1);
        vm.tableParams.reload();
    };
}

module.exports = {
    name: 'KeywordsListCtrl',
    fn: KeywordsListCtrl
};
