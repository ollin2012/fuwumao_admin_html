/**
 * 控制器：平台规则设定编辑
 */
'use strict';

function AgencyDepositSettingCtrl($controller, AdminRegulationSrv, MessageSrv, DateCompareSrv) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminRegulation'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts:ctrlOpts}));

    AdminRegulationSrv.initAgencyDepositSetting(vm);
    vm.reserveUpdate = function(){
        //输入日期非空判断
        let dateNonFlg = 'error.dateNon-existent';
        if (!vm.reserveSetting.settingStartDate) {
            MessageSrv.error(dateNonFlg);
            return true;
        }

        // 判断输入日期是否是未来日期
        let dateCompParams = {
            targetDate: vm.reserveSetting.settingStartDate,
            compareDate: new Date()
        };
        let messageParams = ['实际开始时间', '当日'];
        if (DateCompareSrv.dateCompare(dateCompParams, messageParams)) {
            return true;
        }
        // 设定提示消息以及保存的函数
        let confirmMsg = 'confirm.update';
            // 确认保存后执行
        MessageSrv.confirm(confirmMsg).then(() => {
            // 执行保存
            AdminRegulationSrv.agencyDepositSettingSaveForEdit(vm);
        });
    };
}

module.exports = {
    name: 'AgencyDepositSettingCtrl',
    fn: AgencyDepositSettingCtrl
};
