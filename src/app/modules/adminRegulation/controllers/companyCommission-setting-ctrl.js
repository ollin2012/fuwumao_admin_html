/**
 * 控制器：平台规则设定编辑
 */
'use strict';

function CompanyCommissionSettingCtrl($controller, AdminRegulationSrv, MessageSrv, DateCompareSrv) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'adminRegulation'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts:ctrlOpts}));

    AdminRegulationSrv.initCompanyCommissionSetting(vm);
    vm.reserveUpdate = function(){
        //输入日期非空判断
        let dateNonFlg = 'error.dateNon-existent';
        if (!vm.settingStartDate) {
            MessageSrv.error(dateNonFlg);
            return true;
        }

        // 判断输入日期是否是未来日期
        let dateCompParams = {
            targetDate: vm.settingStartDate,
            compareDate: new Date()
        };
        let messageParams = ['实际开始时间', '当日'];
        if (DateCompareSrv.dateCompare(dateCompParams, messageParams)) {
            return true;
        }
        // 设定提示消息以及保存的函数
        let confirmMsg = 'confirm.update';
        MessageSrv.confirm(confirmMsg).then(() => {
            // 执行保存
            AdminRegulationSrv.companyCommissionSettingSaveForEdit(vm);
        });
        // 确认保存后执行
    };
}

module.exports = {
    name: 'CompanyCommissionSettingCtrl',
    fn: CompanyCommissionSettingCtrl
};
