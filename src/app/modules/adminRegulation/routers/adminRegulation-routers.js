'use strict';
/**
 * 平台规则设定 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.adminRegulation', {
        template: '<div ui-view></div>',
        url: '/adminRegulation',
        abstract: true
    })
    .state('main.adminRegulation.currencySetting', {
        url: '/currencySetting',
        templateUrl: 'adminRegulation/views/currency-setting.html',
        controller: 'CurrencySettingCtrl as vm',
    })    
    .state('main.adminRegulation.agencyDepositSetting', {
    	url: '/agencyDepositSetting',
        templateUrl: 'adminRegulation/views/agencyDeposit-setting.html',
        controller: 'AgencyDepositSettingCtrl as vm',
    })
    .state('main.adminRegulation.companyCommissionSetting', {
        url: '/companyCommissionSetting',
        templateUrl: 'adminRegulation/views/companyCommission-setting.html',
        controller: 'CompanyCommissionSettingCtrl as vm',
    })
    .state('main.adminRegulation.adminBrokerageSetting', {
        url: '/adminBrokerageSetting',
        templateUrl: 'adminRegulation/views/adminBrokerage-setting.html',
        controller: 'AdminBrokerageSettingCtrl as vm',
    });
};
