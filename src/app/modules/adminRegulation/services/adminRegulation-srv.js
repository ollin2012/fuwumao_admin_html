'use strict';

const _ = require('lodash');
/**
 * 平台规则设定
 */
function AdminRegulationSrv(ApiSrv, CodeList, MessageSrv, DateSrv) {
    'ngInject';
    //平台币种设定画面初始化
    function _convCurrencyForSelect(currencyList) {
        currencyList = currencyList || [];
        return _.map(CodeList.currencyType, function(value, key) {
            let selected = (currencyList.indexOf(key) >= 0);
            return {
                key: key,
                //text: value,
                support: selected
            };
        });
    }

    function _convCurrencyForDisplay(currencyList) {
        currencyList = currencyList || [];
        let currencyNameList = [];
        let currencyValueList = [];
        currencyNameList = _.map(currencyList, 'currencyType');
        currencyValueList = _.map(currencyList, 'depositAmount');
        return _.map(CodeList.currencyType, function(value, key) {
            let selected = (currencyNameList.indexOf(key) >= 0);
            let currencyValue = '0';
            if (selected) {
                currencyValue = currencyValueList[currencyNameList.indexOf(key)];
            }
            return {
                text: value,
                value: currencyValue,
                key: key
            };
        });
    }

    function _convCurrencyForListView(currencyList) {
        currencyList = currencyList || [];
        return _.map(currencyList, key => CodeList.currencyName[key])
            .join(' ');
    }

    function _futureCurrencyListEdit(currencyList) {
        currencyList = currencyList || [];
        let array = [];
        _.forEach(currencyList, function(currency) {
            if (currency.support) {
                array.push(currency.key);
            }
        });
        return array;
    }

    function _agencyDepositListEdit(currencyList) {
        currencyList = currencyList || [];
        return _.map(currencyList, function(obj) {
            return {
                currencyType: obj.key,
                depositAmount: obj.value
            };
        });
    }

    function currencyParamenterEdit(vm) {
        vm.model.future = {};
        vm.model.future.currencyList = _futureCurrencyListEdit(vm.futureCurrencyList);
        vm.model.future.settingStartDate = vm.futureSetting.settingStartDate;
    }

    function agencyDepositParamenterEdit(vm) {
        vm.model.reserve = {};
        //vm.reserveCurrencyList = _currencyCodeEditForUpdate(vm.reserveCurrencyList);
        vm.model.reserve.depositSettingList = _agencyDepositListEdit(vm.reserveCurrencyList);
        //vm.model.reserve.settingList = _currencyCodeEditForUpdate(vm.model.reserve.settingList);
        vm.model.reserve.settingStartDate = vm.reserveSetting.settingStartDate;
    }

    function currencySettingSaveForEdit(vm) {
        currencyParamenterEdit(vm);
        ApiSrv.exec('currency/update', vm.model.future)
            .then(function() {
                MessageSrv.success('message.updateOk');
            });
    }

    function agencyDepositSettingSaveForEdit(vm) {
        agencyDepositParamenterEdit(vm);
        ApiSrv.exec('depositSetting/update', vm.model.reserve)
            .then(function() {
                MessageSrv.success('message.updateOk');
            });
    }

    function companyCommissionSettingSaveForEdit(vm) {
        vm.reserveSetting.settingStartDate = vm.settingStartDate;
        ApiSrv.exec('commissionSetting/update', vm.reserveSetting)
            .then(function() {
                MessageSrv.success('message.updateOk');
            });
    }

    //平台手续费保存
    function adminBrokerageSettingSaveForEdit(vm) {
        ApiSrv.exec('platformFee/setting', {
                settingList: vm.reserveSetting.map(function(level) {
                    return {
                        //百分比转换 实际画面表示2位整数，api需要小数
                        buyerFeePer: parseFloat(level.buyerFeePer) / 100,
                        sellerFeePer: parseFloat(level.sellerFeePer) / 100,
                        companyLevel: level.companyLevel,
                    };
                }),
                //预定开始日期
                settingStartDate: vm.reserveSetting[0].settingStartDate
            })
            .then(function() {
                MessageSrv.success('message.updateOk');
            });
    }

    function initCurrencySetting(vm) {
        ApiSrv.exec('currency/getList', { reserveFlg: '1' })
            .then(function(data) {
                if (data) {
                    vm.currentSetting = data.current;
                    vm.futureSetting = data.reserve;
                    if (vm.futureSetting && vm.futureSetting.length > 0) {
                        vm.futureSetting.settingStartDate = DateSrv.toDate(vm.futureSetting.settingStartDate);
                    }
                    vm.futureCurrencyList = _convCurrencyForSelect(vm.futureSetting.currencyList);
                    vm.currentCurrencyList = _convCurrencyForListView(vm.currentSetting.currencyList);
                }
            });
    }

    function initAgencyDepositSetting(vm) {
        ApiSrv.exec('depositSetting/getList', { reserveFlg: '1' }).then(function(data) {
            if (data) {
                vm.currentSetting = data.current;
                vm.reserveSetting = data.reserve;
                if (vm.reserveSetting && vm.reserveSetting.length > 0) {
                    vm.reserveSetting.settingStartDate = DateSrv.toDate(vm.reserveSetting[0].settingStartDate);
                }
                vm.currentSetting.settingStartDate = vm.currentSetting[0].settingStartDate;
                vm.reserveCurrencyList = _convCurrencyForDisplay(vm.reserveSetting);
                vm.currentCurrencyList = _convCurrencyForDisplay(vm.currentSetting);
            }
        });
    }

    function initCompanyCommissionSetting(vm) {
        ApiSrv.exec('commissionSetting/getList', { reserveFlg: '1' }).then(function(data) {
            if (data) {
                vm.currentSetting = data.current;
                vm.reserveSetting = data.reserve;
                if (vm.reserveSetting && vm.reserveSetting.length > 0) {
                    vm.reserveSetting.settingStartDate = DateSrv.toDate(vm.reserveSetting.settingStartDate);
                    vm.settingStartDate = vm.reserveSetting.settingStartDate;
                }
            }
        });
    }

    //平台手续费设定初始化
    function initAdminBrokerageSetting(vm) {
        ApiSrv.exec('platformFee/getList', { reserveFlg: '1' }).then(function(data) {
            if (data) {
                //现有记录
                vm.currentSetting = [];
                //预定变更
                vm.reserveSetting = [];
                //初始化现有记录 3条
                for (let i = 0; i <= 2; i++) {
                    if (data.current && data.current[i]) {
                        //百分比转换 api返回字符串小数，实际画面表示2位整数
                        vm.currentSetting[i] = {
                            buyerFeePer: parseFloat(data.current[i].buyerFeePer) * 100 + '',
                            sellerFeePer: parseFloat(data.current[i].sellerFeePer) * 100 + '',
                            settingStartTime: DateSrv.toDate(data.current[i].settingStartTime),
                            companyLevel: data.current[i].companyLevel,
                        };
                    } else {
                        //不足3条的话补足空白记录
                        vm.currentSetting[i] = {
                            buyerFeePer: 0,
                            sellerFeePer: 0,
                            companyLevel: '' + (i + 1),
                        };
                    }
                }
                //初始化预定变更 3条
                for (let i = 0; i <= 2; i++) {
                    if (data.reserve && data.reserve[i]) {
                        //百分比转换 api返回字符串小数，实际画面表示2位整数
                        vm.reserveSetting[i] = {
                            buyerFeePer: parseFloat(data.reserve[i].buyerFeePer) * 100 + '',
                            sellerFeePer: parseFloat(data.reserve[i].sellerFeePer) * 100 + '',
                            settingStartTime: DateSrv.toDate(data.reserve[i].settingStartTime),
                            companyLevel: data.reserve[i].companyLevel,
                        };
                    } else {
                        //不足3条的话 复制现有记录
                        vm.reserveSetting[i] = {
                            buyerFeePer: vm.currentSetting[i].buyerFeePer,
                            sellerFeePer: vm.currentSetting[i].sellerFeePer,
                            companyLevel: '' + (i + 1),
                        };
                    }
                }
            }
        });
    }
    return {
        initCurrencySetting,
        currencySettingSaveForEdit,
        initAgencyDepositSetting,
        agencyDepositSettingSaveForEdit,
        initCompanyCommissionSetting,
        companyCommissionSettingSaveForEdit,
        initAdminBrokerageSetting,
        adminBrokerageSettingSaveForEdit,
        currencyParamenterEdit
    };
}
module.exports = {
    name: 'AdminRegulationSrv',
    fn: AdminRegulationSrv
};
