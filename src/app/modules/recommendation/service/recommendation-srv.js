'use strict';

/**
 * 平台规则设定
 */
function RecommendationSrv($uibModal,ApiSrv) {
    'ngInject';
    function initChooseServiceModal(vm) {
        vm.serviceChoose = function() {
            let serviceChooseModel = $uibModal.open({
                templateUrl: 'recommendation/views/recommendation-service-list.html',
                controller: function($scope) {
                    // 检索
                    $scope.search = function() {
                        $scope.tableParams.page(1);
                        $scope.tableParams.reload();
                    };
                    // 取得当前索引
                    $scope.getIndex = function(index) {
                        return ($scope.tableParams.page() - 1) * $scope.tableParams.count() + index + 1;
                    };
                    ApiSrv.exec('service/getList', {
                        agencyNumber: vm.model.companyNumber,
                        dealDiv: '0', // 0：非待处理服务一览
                        page: '1',
                        count: '10000'
                    }).then(function(data){
                        if(data){
                            $scope.serviceList = data;
                        }
                    });
                    $scope.model = vm.model;

                },size: 'lg'
            });
            serviceChooseModel.result.then(function(row) {
                vm.model.serviceNumber = row.serviceNumber;
                vm.model.serviceName = row.serviceName;
                vm.model.targetId = row.serviceId;
            }, function(){});
        };
    }

    return {
        initChooseServiceModal
    };
}
module.exports = {
    name: 'RecommendationSrv',
    fn: RecommendationSrv
};
