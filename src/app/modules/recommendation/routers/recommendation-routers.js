'use strict';
/**
 * 推荐管理 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.recommendation', {
        template: '<div ui-view></div>',
        url: '/recommendation',
        abstract: true
    })
    .state('main.recommendation.list', {
        url: '/',
        templateUrl: 'recommendation/views/recommendation-list.html',
        controller: 'RecommendationListCtrl as vm',
    })    
    .state('main.recommendation.new', {
    	url: '/new',
        templateUrl: 'recommendation/views/recommendation-new.html',
        controller: 'RecommendationNewCtrl as vm',
    })
    .state('main.recommendation.detail', {
        url: '/:id/detail',
        templateUrl: 'recommendation/views/recommendation-detail.html',
        controller: 'RecommendationDetailCtrl as vm',
    })
    .state('main.recommendation.edit', {
        url: '/:id/edit',
        templateUrl: 'recommendation/views/recommendation-edit.html',
        controller: 'RecommendationEditCtrl as vm',
    })
    .state('main.recommendation.delete', {
        url: '/:id/delete',
        templateUrl: 'recommendation/views/recommendation-detail.html',
        controller: 'RecommendationDeleteCtrl as vm'
    });    
};
