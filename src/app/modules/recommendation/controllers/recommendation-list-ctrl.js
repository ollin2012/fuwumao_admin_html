/**
 * 控制器：推荐管理列表
 */
'use strict';

function RecommendationListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'recommendation'
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));  
}

module.exports = {
    name: 'RecommendationListCtrl',
    fn: RecommendationListCtrl
};
