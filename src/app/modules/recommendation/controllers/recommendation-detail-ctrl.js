/**
 * 控制器：推荐管理详情
 */
'use strict';

function RecommendationDetailCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'recommendation'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();
}

module.exports = {
    name: 'RecommendationDetailCtrl',
    fn: RecommendationDetailCtrl
};
