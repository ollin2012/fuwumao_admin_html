/**
 * 控制器：推荐管理编辑
 */
'use strict';

function RecommendationEditCtrl($stateParams, $window, $state, ApiSrv, MessageSrv) {
    'ngInject';

    let vm = this;
    vm.model = {};
    
    // 确定
    vm.save = function() {
        MessageSrv.confirm('confirm.update').then(() => {
            let saveData = {
            		id: $stateParams.id,
            		weight: vm.model.weight,
            		recommendationStatus: vm.model.recommendationStatus,
            		updateTime: vm.model.updateTime,
            };
            // 执行保存
            ApiSrv.exec('recommendation/update', saveData)
                .then(function() {
                    MessageSrv.success('message.updateOk');
                    $state.go('main.recommendation.list');
                });
        });
    };

    /**
     * 回退
     */
    vm.back = function() {
        if ($window.history.length > 1) {
            $window.history.back();
        } else {
            $state.go('main.recommendation.list');
        }
    };

    // 详细取得
    ApiSrv.exec('recommendation/getDetail', {id: $stateParams.id})
	    .then(function(data) {
	        if (data) {
	            vm.model = data;
	        }
	    });
}

module.exports = {
    name: 'RecommendationEditCtrl',
    fn: RecommendationEditCtrl
};
