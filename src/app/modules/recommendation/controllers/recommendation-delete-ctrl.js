/**
 * 控制器：推荐管理删除
 */
'use strict';

function RecommendationDeleteCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'recommendation'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'RecommendationDeleteCtrl',
    fn: RecommendationDeleteCtrl
};
