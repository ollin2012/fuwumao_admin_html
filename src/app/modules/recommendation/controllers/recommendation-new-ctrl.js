/**
 * 控制器：推荐管理新增
 */
'use strict';

function RecommendationNewCtrl($state, $window, RecommendationSrv, MessageSrv, ApiSrv) {
    'ngInject';

    let vm = this;
    vm.model = {};
    
    vm.selectButton = true;
    vm.model.targetTypeFlg = true;
    vm.model.targetType = '1';
    
    vm.radioClick = function(kbn) {
    	if (kbn === vm.model.targetType) {
    		return;
    	}
    	
        if (vm.model.targetTypeFlg) {
            vm.model.targetType = '1';
            vm.model.serviceNumber = '';
            vm.model.serviceName = '';
            vm.selectButton = true;
            vm.model.targetId = null;
        } else {
            vm.model.targetType = '2';
            if (vm.model.companyName) {
            	vm.selectButton = false;
            }
        }
    };
    
    vm.companyNumberBlur = function() {
        if (vm.model.targetTypeFlg) {
            vm.selectButton = true;
            if (vm.model.companyNumber) {
                ApiSrv.exec('company/getName', {companyNumber: vm.model.companyNumber}).then(function(data) {
                    if (data) {
                        vm.model.companyName = data.companyName;
                        vm.model.targetId = data.companyId;
                    } else {
                        vm.model.companyName = null;
                        vm.model.targetId = null;
                    }
                });
            } else {
                vm.model.companyName = null;
                vm.model.targetId = null;
            }
        } else {
            if (vm.model.companyNumber) {
                ApiSrv.exec('company/getName', {companyNumber: vm.model.companyNumber}).then(function(data) {
                    if (data) {
                        vm.model.companyName = data.companyName;
                    } else {
                    	vm.model.companyName = null;
                    }
                    vm.selectButton = false;
                });
            } else {
            	vm.model.companyName = null;
                vm.selectButton = true;
            }
        }
    };
    
    // 确定
    vm.save = function() {
    	if (!vm.model.targetId) {
    		if (vm.model.targetTypeFlg) {
        		MessageSrv.error('error.required', '有效的服务商编号');
    		} else {
        		MessageSrv.error('error.required', '有效的服务编号');
    		}
    		return;
    	}
        MessageSrv.confirm('confirm.create').then(() => {
            let saveData = {
            		targetType: vm.model.targetType,
            		targetId: vm.model.targetId,
            		weight: vm.model.weight,
            		recommendationStatus: vm.model.recommendationStatus
            };
            // 执行保存
            ApiSrv.exec('recommendation/create', saveData)
                .then(function() {
                    MessageSrv.success('message.createOk');
                    $state.go('main.recommendation.list');
                });
        });
    };

    /**
     * 回退
     */
    vm.back = function() {
        if ($window.history.length > 1) {
            $window.history.back();
        } else {
            $state.go('main.recommendation.list');
        }
    };
    
    RecommendationSrv.initChooseServiceModal(vm);
    
    vm.radioClick();
}

module.exports = {
    name: 'RecommendationNewCtrl',
    fn: RecommendationNewCtrl
};
