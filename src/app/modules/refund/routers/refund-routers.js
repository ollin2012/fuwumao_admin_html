'use strict';
/**
 * 退款 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.refund', {
        template: '<div ui-view></div>',
        url: '/refund',
        abstract: true
    })
    .state('main.refund.list', {
        url: '/',
        templateUrl: 'refund/views/refund-list.html',
        controller: 'RefundListCtrl as vm',
    })    
    .state('main.refund.new', {
    	url: '/new',
        templateUrl: 'refund/views/refund-new.html',
        controller: 'RefundNewCtrl as vm',
    })
    .state('main.refund.detail', {
        url: '/:id/detail',
        templateUrl: 'refund/views/refund-detail.html',
        controller: 'RefundDetailCtrl as vm',
    })
    .state('main.refund.edit', {
        url: '/:id/edit',
        templateUrl: 'refund/views/refund-edit.html',
        controller: 'RefundEditCtrl as vm',
    })
    .state('main.refund.delete', {
        url: '/:id/delete',
        templateUrl: 'refund/views/refund-detail.html',
        controller: 'RefundDeleteCtrl as vm'
    });    
};
