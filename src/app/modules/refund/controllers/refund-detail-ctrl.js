/**
 * 控制器：退款详情
 */
'use strict';

function RefundDetailCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'refund'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

   	vm.getDetail();
}

module.exports = {
    name: 'RefundDetailCtrl',
    fn: RefundDetailCtrl
};
