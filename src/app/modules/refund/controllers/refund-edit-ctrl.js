/**
 * 控制器：退款编辑
 */
'use strict';

function RefundEditCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'refund',
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts}));

    vm.getDetail();
}

module.exports = {
    name: 'RefundEditCtrl',
    fn: RefundEditCtrl
};
