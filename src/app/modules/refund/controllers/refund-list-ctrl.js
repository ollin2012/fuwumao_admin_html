/**
 * 控制器：退款列表
 */
'use strict';

const _ = require('lodash');

function RefundListCtrl($controller, $filter, ApiSrv, MessageSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;
    vm.searchConditionBak = {};

    // 检索前处理
    function preSearchFn(apiParams) {
        apiParams.exportFlg = '0'; // 导出文件标志（0：不导出）
    }

    // 检索后处理
    function postSearchFn() {
    	vm.searchConditionBak = _.cloneDeep(vm.searchCondition);
    }

    let ctrlOpts = {
            modelName: 'refund',
            preSearchFn: preSearchFn,
            postSearchFn: postSearchFn
        };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.export = function() {
        vm.searchConditionBak.exportFlg = '1'; // 导出文件标志（1：导出）
        vm.searchConditionBak.page = 1;
        vm.searchConditionBak.count = 99999;

        // ajax取导出报表数据
        return ApiSrv.exec('refund/getList', vm.searchConditionBak)
            .then(function(data) {
            	let exportData = [];
                if (data && data.list && data.list.length > 0) {
                    angular.forEach(data.list, function(refund, index) {
                    	let detail = {};
                    	detail.no = index + 1;
                    	detail.capitalFloNumber = refund.capitalFloNumber;
                    	detail.company = refund.company;
                    	detail.companyNumber = refund.companyNumber;
                    	detail.refundType = $filter('codeText')(refund.refundType, 'refundType');
                    	detail.tradeNumber = refund.tradeNumber;
                    	detail.tradeStatus = $filter('codeText')(refund.tradeStatus, 'tradeStatus');
                    	detail.verifySubmitTime = $filter('dateFormat')(refund.verifySubmitTime);
                    	detail.refundAmount = $filter('currency')(refund.refundAmount, '');
                    	detail.currencyType = $filter('codeText')(refund.currencyType, 'currencyType');
                    	detail.refundStatus = $filter('codeText')(refund.refundStatus, 'refundStatus');
                    	detail.refundDate = $filter('dateFormat')(refund.refundDate);
                    	exportData.push(detail);
                    });
                    return exportData;
                } else {
                    MessageSrv.info('INF_COM_NO_SEARCH_RESULT');
                    return exportData;
                }
            });
    };
}

module.exports = {
    name: 'RefundListCtrl',
    fn: RefundListCtrl
};
