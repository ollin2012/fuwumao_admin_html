/**
 * 控制器：广告详情
 */
'use strict';

function AdvertDetailCtrl($controller) {
    'ngInject';

    let vm = this;
    vm.adverImage = [];
    let ctrlOpts = {
        modelName: 'advert',
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'AdvertDetailCtrl',
    fn: AdvertDetailCtrl
};
