/**
 * 控制器：广告新增
 */
'use strict';

let advertConstants = require('../constants/advert-constants');

function AdvertNewCtrl($controller, $window, UploadSrv, DateCompareSrv, MessageSrv) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'advert',
            preSaveFn: function() {
                //将上传文件的ID数组给api
                if (vm.uploader) {
                    vm.model.advertImages = vm.uploader.getUploadedFileIDs();
                }
                return true;
            }
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts }));

    vm.advertSave = function() {
    	// 广告状态是“公开”的场合
    	if (vm.model.openStatus === '1') {
    		// 公开开始日期与公开结束日期必填
    		if (!vm.model.openStartDate) {
    			let errStart = ['投放公开日'];
            	MessageSrv.error('error.required', errStart);
                $window.scrollTo(0, 0);
                return true;
    		}

    		if (!vm.model.openEndDate) {
    			let errEnd = ['投放结束日'];
            	MessageSrv.error('error.required', errEnd);
                $window.scrollTo(0, 0);
                return true;
    		}
    		
    	}

        // 开始日必须大于结束日，调DateCompareSrv
        let dateCompParams = {
            targetDate: vm.model.openEndDate,
            compareDate: vm.model.openStartDate
        };
        let messageParams = ['投放结束日', '投放公开日'];
        if (vm.model.openStartDate && vm.model.openEndDate && DateCompareSrv.dateCompare(dateCompParams, messageParams)) {
            $window.scrollTo(0, 0);
            return;
        }

        vm.model.advertImages = vm.uploader.getUploadedFileIDs();
        
        // 广告图片的必须check
        if (!vm.model.advertImages || vm.model.advertImages.length === 0) {
			let errPic = ['广告图片'];
        	MessageSrv.error('error.required', errPic);
            $window.scrollTo(0, 0);
            return true;
        }

        vm.save();
    };

    // 设置上传组件
    let uploadFileParams = {
        fileType: advertConstants.fileType
    };
    vm.uploader = UploadSrv.createImageUploader(uploadFileParams);
}

module.exports = {
    name: 'AdvertNewCtrl',
    fn: AdvertNewCtrl
};
