/**
 * 控制器：广告删除
 */
'use strict';

function AdvertDeleteCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'advert'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'AdvertDeleteCtrl',
    fn: AdvertDeleteCtrl
};
