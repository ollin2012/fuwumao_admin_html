/**
 * 控制器：广告编辑
 */
'use strict';

let advertConstants = require('../constants/advert-constants');

function AdvertEditCtrl($controller, $window, $filter, uibDateParser, UploadSrv, DateCompareSrv, MessageSrv) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'advert',
            postGetDetailFn: function(data) {
            	// 去掉广告费小数部分
            	data.contractAmount = $filter('moneyFormat')(data.contractAmount);
                //将api返回的img加入预览
                if (data.adverImage && data.adverImage.length && vm.uploader) {
                    for (let i in data.adverImage) {
                        // 将api返回的图片加入已上传的文件
                        vm.uploader.addUploadedFiles({
                            id: data.adverImage[i].id,
                            url: data.adverImage[i].path,
                        });
                    }
                    delete data.adverImage;
                }
                //api返回的日期转换成date型
                if (data.openStartDate && typeof data.openStartDate === 'string' && data.openStartDate.length >= 10) {
                    data.openStartDate = uibDateParser.parse(data.openStartDate.substring(0, 10), 'yyyy-MM-dd');
                }
                if (data.openEndDate && typeof data.openEndDate === 'string' && data.openEndDate.length >= 10) {
                    data.openEndDate = uibDateParser.parse(data.openEndDate.substring(0, 10), 'yyyy-MM-dd');
                }
                //取名不统一 转换一下
                data.advertLanguage = data.language;
                return data;
            }
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts }));

    vm.getDetail();

    vm.advertSave = function() {
    	// 广告状态是“公开”的场合
    	if (vm.model.openStatus === '1') {
    		// 公开开始日期与公开结束日期必填
    		if (!vm.model.openStartDate) {
    			let errStart = ['投放公开日'];
            	MessageSrv.error('error.required', errStart);
                $window.scrollTo(0, 0);
                return true;
    		}

    		if (!vm.model.openEndDate) {
    			let errEnd = ['投放结束日'];
            	MessageSrv.error('error.required', errEnd);
                $window.scrollTo(0, 0);
                return true;
    		}
    		
    	}

        // 开始日必须大于结束日，调DateCompareSrv
        let dateCompParams = {
            targetDate: vm.model.openEndDate,
            compareDate: vm.model.openStartDate
        };
        let messageParams = ['投放结束日', '投放公开日'];
        if (vm.model.openStartDate && vm.model.openEndDate && DateCompareSrv.dateCompare(dateCompParams, messageParams)) {
            $window.scrollTo(0, 0);
            return true;
        }

        //将上传文件的ID数组给api
        if (vm.uploader) {
            vm.model.advertImages = vm.uploader.getUploadedFileIDs();
        }
        // 广告图片的必须check
        if (!vm.model.advertImages || vm.model.advertImages.length === 0) {
			let errPic = ['广告图片'];
        	MessageSrv.error('error.required', errPic);
            $window.scrollTo(0, 0);
            return true;
        }
        vm.save();
    };

    // 设置上传组件
    let uploadFileParams = {
        fileType: advertConstants.fileType
    };
    vm.uploader = UploadSrv.createImageUploader(uploadFileParams);
}

module.exports = {
    name: 'AdvertEditCtrl',
    fn: AdvertEditCtrl
};
