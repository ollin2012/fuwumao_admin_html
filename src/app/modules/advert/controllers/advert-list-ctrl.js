/**
 * 控制器：广告列表
 */
'use strict';

function AdvertListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'advert',
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

}

module.exports = {
    name: 'AdvertListCtrl',
    fn: AdvertListCtrl
};
