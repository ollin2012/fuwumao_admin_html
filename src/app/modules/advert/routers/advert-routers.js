'use strict';
/**
 * 广告 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.advert', {
        template: '<div ui-view></div>',
        url: '/advert',
        abstract: true
    })
    .state('main.advert.list', {
        url: '/',
        templateUrl: 'advert/views/advert-list.html',
        controller: 'AdvertListCtrl as vm',
    })    
    .state('main.advert.new', {
    	url: '/new',
        templateUrl: 'advert/views/advert-new.html',
        controller: 'AdvertNewCtrl as vm',
    })
    .state('main.advert.detail', {
        url: '/:id/detail',
        templateUrl: 'advert/views/advert-detail.html',
        controller: 'AdvertDetailCtrl as vm',
    })
    .state('main.advert.edit', {
        url: '/:id/edit',
        templateUrl: 'advert/views/advert-edit.html',
        controller: 'AdvertEditCtrl as vm',
    })
    .state('main.advert.delete', {
        url: '/:id/delete',
        templateUrl: 'advert/views/advert-detail.html',
        controller: 'AdvertDeleteCtrl as vm'
    });    
};
