/**
 * 控制器：成员企业关联
 */
'use strict';

function MemberCompanyRelateCtrl($controller, ApiSrv, MessageSrv, modalParams, $uibModalInstance) {
    'ngInject';

    let vmMember = this;

    // 画面初期化
    vmMember.companyInviteId = modalParams.companyInviteId;
    vmMember.inviteCompanyId = modalParams.inviteCompanyId;
    vmMember.inviteCompanyNumber = modalParams.inviteCompanyNumber;
    vmMember.inviteCompanyName = modalParams.inviteCompanyName;
    vmMember.relateUpdateTime = modalParams.relateUpdateTime;
    vmMember.invitedCompanyId = null;

    // 受邀企业编号失去焦点
    vmMember.getCompanyName = function() {
        // ajax企业名称取得
        return ApiSrv.exec('company/getName', {companyNumber: vmMember.invitedCompanyNumber})
            .then(function(data) {
            	if (data) {
            	    vmMember.invitedCompanyId = data.companyId;
            		vmMember.invitedCompanyName = data.companyName;
            	}
            });
    };

    // 关联成员企业确定
    vmMember.memberConfirm = function() {
        MessageSrv.confirm('confirm.verify').then(() => {
	    	let relateData = {
	    			id: vmMember.companyInviteId,
	    			inviteCompanyId: vmMember.inviteCompanyId,
	    			invitedCompanyId: vmMember.invitedCompanyId,
	    			updateTime: vmMember.relateUpdateTime
	    	};

	        // ajax关联成员企业
	        return ApiSrv.exec('companyInvite/relate', relateData)
	            .then(function() {
	            	$uibModalInstance.close();
	            });
        });
    };
}

module.exports = {
    name: 'MemberCompanyRelateCtrl',
    fn: MemberCompanyRelateCtrl
};
