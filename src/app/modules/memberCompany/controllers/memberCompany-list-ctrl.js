/**
 * 控制器：成员企业列表
 */
'use strict';

function MemberCompanyListCtrl($controller, $uibModal, $state, MessageSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'companyInvite'
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    //关联成员企业模态框
    vm.companyInviteRelate = function(companyData) {
		let modalParams = {
				companyInviteId: companyData.id,
				inviteCompanyId: companyData.inviteCompanyId,
				inviteCompanyNumber: companyData.inviteCompanyNumber,
				inviteCompanyName: companyData.inviteCompanyName,
				relateUpdateTime: companyData.updateTime
		};

		let relateModel = $uibModal.open({
			templateUrl : 'memberCompany/views/memberCompany-relate.html',
			size: 'lg',
			controller: 'MemberCompanyRelateCtrl as vmMember',
			resolve: {
				modalParams: function(){
					return modalParams;
				}
			}
		});

		relateModel.result.then(function() {
            MessageSrv.success('message.relateOk');
            // 画面刷新
            $state.reload();
        }, function(){});
    }; 
}

module.exports = {
    name: 'MemberCompanyListCtrl',
    fn: MemberCompanyListCtrl
};
