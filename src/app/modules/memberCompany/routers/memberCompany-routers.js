'use strict';
/**
 * 成员企业 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.memberCompany', {
        template: '<div ui-view></div>',
        url: '/memberCompany',
        abstract: true
    })
    .state('main.memberCompany.list', {
        url: '/',
        templateUrl: 'memberCompany/views/memberCompany-list.html',
        controller: 'MemberCompanyListCtrl as vm',
    })    
    .state('main.memberCompany.new', {
    	url: '/new',
        templateUrl: 'memberCompany/views/memberCompany-new.html',
        controller: 'MemberCompanyNewCtrl as vm',
    })
    .state('main.memberCompany.detail', {
        url: '/:id/detail',
        templateUrl: 'memberCompany/views/memberCompany-detail.html',
        controller: 'MemberCompanyDetailCtrl as vm',
    })
    .state('main.memberCompany.edit', {
        url: '/:id/edit',
        templateUrl: 'memberCompany/views/memberCompany-edit.html',
        controller: 'MemberCompanyEditCtrl as vm',
    })
    .state('main.memberCompany.delete', {
        url: '/:id/delete',
        templateUrl: 'memberCompany/views/memberCompany-detail.html',
        controller: 'MemberCompanyDeleteCtrl as vm'
    });    
};
