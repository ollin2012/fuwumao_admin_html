/**
 * 控制器：交易文件一览
 */
'use strict';

function TradeFileListCtrl($state, $stateParams, TableSrv) {
    'ngInject';

    let vm = this;
    let tableSrv;

    function _preSearchFn(apiParams) {
        apiParams.tradeId = $stateParams.tradeId;
    }

    vm.search = function() {
        vm.tableParams.page(1);
        vm.tableParams.reload();
    };

    vm.clickFileLink = function(item) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', item.path, true);
        xhr.responseType = 'blob';

        xhr.onload = function() {
            if (this.status == 200) {
                var blob = new Blob([this.response], { type: 'file' });
                let url = (window.URL || window.webkitURL).createObjectURL(blob);

                var downloadLink = angular.element('<a></a>');
                downloadLink.attr('href', url);
                downloadLink.attr('download', item.name);
                downloadLink[0].click();
            }
        };

        xhr.send();
    };

    // 初始化
    (function init() {
        vm.searchCondition = {};
        tableSrv = new TableSrv({
            searchCondition: vm.searchCondition,
            getDataUrl: 'tradeFile/getList',
            preSearchFn:_preSearchFn,
            searchCount: 25,
            isNoHistory: true
        });
        vm.tableParams = tableSrv.create();
    })();
}

module.exports = {
    name: 'TradeFileListCtrl',
    fn: TradeFileListCtrl
};
