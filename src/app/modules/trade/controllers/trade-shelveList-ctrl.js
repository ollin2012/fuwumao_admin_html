/**
 * 控制器：交易列表
 */
'use strict';

function TradeShelveListCtrl($controller) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this,
        ctrlOpts = {
            modelName: 'trade',
            preSearchFn: function(apiParams) {
                //交易搁置查询
                apiParams.tradeDelayFlg = '1';
                // 待处理交易标志 - 0：不需要查询待处理交易
                apiParams.dealFlg = '0';
            },
        };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
}

module.exports = {
    name: 'TradeShelveListCtrl',
    fn: TradeShelveListCtrl
};
