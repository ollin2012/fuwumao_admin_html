/**
 * 控制器：交易列表
 */
'use strict';

const _ = require('lodash');

function TradeListCtrl($state, $controller, $stateParams) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;

    // 根据迁移元画面取得当前处理的Action类别
    vm.action = _.last(_.split($state.current.name, '.'));
    let defaultCondition = {
        tradeDelayFlg: '0', // 交易搁置查询标志： 不需要
        dealFlg: (vm.action === 'unhandledList') ? '1' : '0'    // 待处理交易标志
    };

    // 根据action设定检索前处理
    function preSearchFn(apiParams) {
        //该画面总是不需要交易搁置查询
        apiParams.tradeDelayFlg = '0'; // 0：不需要；1：需要
        //交易一览
        if (vm.action === 'list') {
            //是否从服务详细画面来
            if ($stateParams.serviceId) {
                apiParams.serviceId = $stateParams.serviceId;
                $stateParams.serviceId = null;
            }
        }
    }

    let _postSearchFn = function(list) {
    	_.forEach(list, row => {
            let stageList = row.tradeStageInfo;
            // 当交易状态为“交易内容审核中”时，显示“处理”link。
            if (row.tradeStatus === '70') {
                row.dealViewFlg = true;
            } else if (row.tradeStatus === '80' && _.find(stageList, {'stageStatus':'10'})) {
                // 当交易状态为“交易进行中”并且 交易阶段中有阶段状态为“待支付预付款（卖方）”时，显示“处理”link。
                row.dealViewFlg = true;
            } else if (row.tradeStatus === '90' &&
                        _.find(stageList, (stage) =>  stage.stageStatus === '40' || stage.stageStatus === '45')) {
                // 当交易状态为“交易完成” 并且 交易阶段中阶段状态为（“阶段完成（待平台付款）（平台） 或者 阶段完成（平台付款中）（平台）)”时，显示“处理”link。
                row.dealViewFlg = true;
            } else {
                // 上记以外的场合
                row.dealViewFlg = false;
            }
    	});
    };

    let ctrlOpts = {
        modelName: 'trade',
        preSearchFn: preSearchFn,
        postSearchFn: _postSearchFn,
        defaultCondition,
        defaultSortCols: {
            tradeNumber: 'desc'
        }
    };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
}

module.exports = {
    name: 'TradeListCtrl',
    fn: TradeListCtrl
};
