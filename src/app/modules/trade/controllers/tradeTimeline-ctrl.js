/**
 * 控制器：交易操作记录一览
 */
'use strict';

function TradeTimelineCtrl($state, $stateParams, ApiSrv, TableSrv, MessageSrv) {
    'ngInject';

    let vm = this;
    // 交易ID
    vm.tradeId = $stateParams.tradeId;

    let tableSrv;

    function _preSearchFn(apiParams) {
        //参数 交易id 取自url
        apiParams.tradeId = vm.tradeId;
        //固定按时间降序排列
        apiParams.sortCols = {
            operationTime: 'desc'
        };
    }

    //msg的参数  如字符串'{"p0":"交易a","p1":"企业b"}'
    vm.getTmpParam = function(paramStr) {
        return MessageSrv.getMessageParams(paramStr);
    };

    // 初始化
    (function init() {
        vm.searchCondition = {};
        tableSrv = new TableSrv({
            searchCondition: vm.searchCondition,
            getDataUrl: 'tradeTimeline/getList',
            preSearchFn: _preSearchFn,
            searchCount: 25,
            isNoHistory: true
        });
        vm.tableParams = tableSrv.create();
    })();
}

module.exports = {
    name: 'TradeTimelineCtrl',
    fn: TradeTimelineCtrl
};
