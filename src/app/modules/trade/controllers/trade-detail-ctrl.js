/**
 * 控制器：交易详情
 */
'use strict';

const _ = require('lodash');

function TradeDetailCtrl($controller, $state, $stateParams, ApiSrv, MessageSrv, CommonConstants, $uibModal) {
    'ngInject';
    
    let _preGetDetailFn = function(apiParams) {
    	apiParams.tradeId = $stateParams.tradeId;
    	apiParams.queryOpts = {
    			includeTradeVerifyHistory: '1'
    	};
    	return apiParams;
    };
    
    let _postGetDetailFn = function(data) {
    	if (data) {
    		if (data.tradeStageList) {
        		for (let i=0; i<data.tradeStageList.length; i++) {
            		if (data.tradeStageList[i].stageStatus === '10' && (data.tradeStageList[i].capitalFlowStatus === '1' || data.tradeStageList[i].capitalFlowStatus === '11')) {
            			data.tradeStageList[i].stagePrePayMoney = data.tradeStageList[i].capitalFlowStatus;
            			data.tradeStageList[i].stagePrePayMoneyViewFlg = true;
            			data.tradeStageList[i].adminPayedStatusViewFlg = false;
            		}
            		//else {
            		//	data.tradeStageList[i].stagePrePayMoneyViewFlg = false;
            		//	data.tradeStageList[i].adminPayedStatusViewFlg = true;
            		//}
            		
            		if ((data.tradeStageList[i].stageStatus === '40' || data.tradeStageList[i].stageStatus === '45') && (data.tradeStageList[i].capitalFlowStatus === '3' || data.tradeStageList[i].capitalFlowStatus === '4' || data.tradeStageList[i].capitalFlowStatus === '12')) {
            			data.tradeStageList[i].adminPayedStatus = data.tradeStageList[i].capitalFlowStatus;
            			data.tradeStageList[i].stagePrePayMoneyViewFlg = false;
            			data.tradeStageList[i].adminPayedStatusViewFlg = true;
            		}
            		//else {
            		//	data.tradeStageList[i].stagePrePayMoneyViewFlg = true;
            		//	data.tradeStageList[i].adminPayedStatusViewFlg = false;
            		//}
        		}
    		}
    	}
    	return data;
    };

    let vm = this,
        ctrlOpts = {
            modelName: 'trade',
            preGetDetailFn: _preGetDetailFn,
            postGetDetailFn: _postGetDetailFn
        };

    // 根据迁移元画面取得当前处理的Action类别
    vm.action = _.last(_.split($state.current.name, '.'));

    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();

    //快速选择审核结果
    vm.tradeVerifyResult = {};
    vm.tradeResult = CommonConstants.tradeResult;
    vm.selectTradeResult = function(idx) {
        vm.tradeVerifyResult.verifyResultDetail = CommonConstants.tradeResultDetail[idx];
    };

    //提交审核结果
    vm.submitTradeVerify = function() {
        if (!vm.model.tradeVerifyHistoryList || !vm.model.tradeVerifyHistoryList.length) {
            return;
        }
        //弹出框确认
        MessageSrv.confirm('confirm.verify').then(function() {
            ApiSrv.exec('trade/adminVerify', {
                //tradeVerifyId: vm.model.tradeVerifyHistoryList[0].tradeVerifyId,
                tradeId: vm.model.id,
                verifyResultFlg: vm.tradeVerifyResult.verifyResultFlg,
                verifyResultDetail: vm.tradeVerifyResult.verifyResultDetail,
                capitalFlowMemo: vm.tradeVerifyResult.capitalFlowMemo
                //updateTime: vm.model.tradeVerifyHistoryList[0].updateTime,
            }).then(function() {
                MessageSrv.success('message.verifyOk');
                $state.reload();
            });
        });
    };

    //提交阶段审核结果（预付款）
    vm.submitStageVerify = function(stage) {
        //弹出框确认
        MessageSrv.confirm('confirm.verify').then(function() {
            ApiSrv.exec('tradeStage/prepaySuccess', {
            	tradeStageId: stage.stageId,
            	capitalFlowMemo: stage.stageMoneyMemo,
                updateTime: stage.updateTime
            }).then(function() {
                MessageSrv.success('message.verifyOk');
                $state.reload();
            });
        });
    };

    //提交阶段审核结果（平台放款）
    vm.submitAdminStageVerify = function(stage) {
        //弹出框确认
        MessageSrv.confirm('confirm.verify').then(function() {
            ApiSrv.exec('tradeStage/adminPaySuccess', {
            	tradeStageId: stage.stageId,
            	capitalFlowMemo: stage.stageMoneyMemo,
                updateTime: stage.updateTime
            }).then(function() {
                MessageSrv.success('message.verifyOk');
                $state.reload();
            });
        });
    };

    //初始化评价一览
    ApiSrv.exec('trade/getEvaluation', {
        tradeId: $stateParams.tradeId,
    }).then(function(data) {
        if (data && data.list && data.list.length) {
            vm.evaluationList = data.list;
        }
    });

    //交易终止modal初始化
    vm.initTermination = function() {
        let terminationModal = $uibModal.open({
            //modal模板
            templateUrl: 'trade/views/trade-stop.html',
            controller: function($scope) {
                //modal中显示交易号
                $scope.tradeNumber = vm.model.tradeNumber;
            },
        });
        //点击确定按钮
        terminationModal.result.then(function(stopReason) {
            //弹出框确认
            MessageSrv.confirm('confirm.stop').then(function() {
                //交易终止api
                ApiSrv.exec('trade/termination', {
                    tradeId: $stateParams.tradeId,
                    stopReason: stopReason,
                    tradeUpdatetime: vm.model.updateTime,
                }).then(function() {
                    //刷新画面
                    MessageSrv.success('message.stopOk');
                    $state.reload();
                });
            });
        }, function() {});
    };
}

module.exports = {
    name: 'TradeDetailCtrl',
    fn: TradeDetailCtrl
};
