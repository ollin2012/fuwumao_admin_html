'use strict';
/**
 * 交易 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.trade', {
        template: '<div ui-view></div>',
        url: '/trade',
        abstract: true
    })
    .state('main.trade.list', {
        url: '/',
        templateUrl: 'trade/views/trade-list.html',
        controller: 'TradeListCtrl as vm',
        params: {
            serviceId: null,
        },
    })
    .state('main.trade.unhandledList', {
        url: '/unhandledList',
        templateUrl: 'trade/views/trade-list.html',
        controller: 'TradeListCtrl as vm',
    })
    .state('main.trade.shelveList', {
        url: '/shelve',
        templateUrl: 'trade/views/trade-shelveList.html',
        controller: 'TradeShelveListCtrl as vm',
    })
    .state('main.trade.detail', {
        url: '/:tradeId/detail',
        templateUrl: 'trade/views/trade-detail.html',
        controller: 'TradeDetailCtrl as vm',
    })
    .state('main.trade.verify', {
        url: '/:tradeId/verify',
        templateUrl: 'trade/views/trade-detail.html',
        controller: 'TradeDetailCtrl as vm',
    });
};
