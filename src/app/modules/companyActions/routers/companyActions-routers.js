'use strict';
/**
 * 企业行为 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.companyActions', {
        template: '<div ui-view></div>',
        url: '/companyActions',
        abstract: true
    })
    .state('main.companyActions.list', {
        url: '/',
        templateUrl: 'companyActions/views/companyActions-list.html',
        controller: 'CompanyActionsListCtrl as vm',
    });    
};
