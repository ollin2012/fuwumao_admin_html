/**
 * 控制器：企业行为列表
 */
'use strict';

function CompanyActionsListCtrl($controller, $scope) {
    'ngInject';

    let vm = this;

    //默认按日搜索
    let defaultCondition = {
        mdFlg: '1'
    };

    // 扩展自list控制器基类
    let ctrlOpts = {
            //API的url为companyAct/getList
            modelName: 'companyAct',
            //检索前加日期参数
            preSearchFn: function(apiParams) {
                if (vm.searchDate.year && vm.searchDate.year !== '') {
                    apiParams.actionsTime = '' + vm.searchDate.year;
                    if (vm.searchDate.month && vm.searchDate.month !== '') {
                        if (vm.searchDate.month < 10) {
                            apiParams.actionsTime += '-0' + vm.searchDate.month;
                        } else {
                        	apiParams.actionsTime += '-' + vm.searchDate.month;
                        }
                        if (vm.searchCondition.mdFlg === '1' && vm.searchDate.day && vm.searchDate.day !== '') {
                            if (vm.searchDate.day < 10) {
                                apiParams.actionsTime += '-0' + vm.searchDate.day;
                            } else {
                            	apiParams.actionsTime += '-' + vm.searchDate.day;
                            }
                        }
                    }
                }
            },
            defaultCondition: defaultCondition
        };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.searchDate = {};
    //初始化下拉框选项
    vm.searchDateOptions = {
        year: [],
        month: [],
        day: [],
    };
    for (let i = 2012; i <= 2022; i++) {
        vm.searchDateOptions.year.push(i);
    }

    //取得月份最大日数
    let maxDayArr = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    function getMaxDay(year, month) {
        if (month === 2 && year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) {
            return 29;
        } else {
            return maxDayArr[month - 1];
        }
    }

    function setMonthOption() {
        vm.searchDateOptions.month.length = 0;
        if (!vm.searchDate.year || vm.searchDate.year === '') {
            return;
        }
        for (let i = 1; i <= 12; i++) {
            vm.searchDateOptions.month.push(i);
        }
    }

    function setDayOption() {
        vm.searchDateOptions.day.length = 0;
        if (!vm.searchDate.month || vm.searchDate.month === '') {
            return;
        }
        let maxDay = getMaxDay(vm.searchDate.year, vm.searchDate.month);
        for (let i = 1; i <= maxDay; i++) {
            vm.searchDateOptions.day.push(i);
        }
    }

    //监控年月选项
    $scope.$watch('vm.searchDate.year', function(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        setMonthOption();
        setDayOption();
    });
    $scope.$watch('vm.searchDate.month', function(newVal, oldVal) {
        if (newVal === oldVal || vm.searchCondition.mdFlg === '2') {
            return;
        }
        setDayOption();
    });

    vm.myReset = function() {
        vm.searchDate = {};
        vm.reset();
    };
}

module.exports = {
    name: 'CompanyActionsListCtrl',
    fn: CompanyActionsListCtrl
};
