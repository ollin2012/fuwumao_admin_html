/**
 * 控制器：管理员用户列表
 */
'use strict';

let _ = require('lodash');

function AdminUserListCtrl($controller, AdminUserSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;

    // 检索后处理
    function postSearchFn(list) {
    	_.forEach(list, row => {
    		if (row.roleList) {
    			if (row.roleList.length > 4) {
                	let roles = _.dropRight(row.roleList, row.roleList.length - 4);
            		row.fullRoleName = _.map(roles).join(',') + '...';
    			} else {
            		row.fullRoleName = _.map(row.roleList).join(',');
    			}
    		}
    	});
    }
    
    let ctrlOpts = {
        modelName: 'adminUser',
        postSearchFn: postSearchFn
    };
    
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
    
    AdminUserSrv.initDetail(vm);
}

module.exports = {
    name: 'AdminUserListCtrl',
    fn: AdminUserListCtrl
};
