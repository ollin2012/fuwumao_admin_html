/**
 * 控制器：管理员用户新增
 */
'use strict';

function AdminUserNewCtrl($scope, $controller, AdminUserSrv) {
    'ngInject';

    let vm = this;

    let ctrlOpts = {
        modelName: 'adminUser'
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
    vm.passWordFlg = false;
    vm.rePassword = '';
    vm.passWordUpdate = function(){
        if(vm.passWordFlg===false) {
            vm.passWordFlg = true;
        }else{
            vm.passWordFlg = false;
        }
    };
    AdminUserSrv.initDetail(vm);
    AdminUserSrv.initChooseRoleModal(vm);
}
module.exports = {
    name: 'AdminUserNewCtrl',
    fn: AdminUserNewCtrl
};
