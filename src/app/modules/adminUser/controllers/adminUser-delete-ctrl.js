/**
 * 控制器：管理员用户删除
 */
'use strict';

function AdminUserDeleteCtrl($controller, AdminUserSrv) {
    'ngInject';
    let vm = this;
    let ctrlOpts = {
        modelName: 'adminUser',
        postGetDetailFn: AdminUserSrv.postGetDetailFn(vm)
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'AdminUserDeleteCtrl',
    fn: AdminUserDeleteCtrl
};
