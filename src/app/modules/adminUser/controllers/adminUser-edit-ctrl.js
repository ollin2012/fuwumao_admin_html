/**
 * 控制器：管理员用户编辑
 */
'use strict';

function AdminUserEditCtrl($controller, AdminUserSrv) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
        modelName: 'adminUser',
        postGetDetailFn: AdminUserSrv.postGetDetailFn(vm)
    };
    //基类扩展
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));
    AdminUserSrv.initDetail(vm);
    vm.prePassword = '';
    vm.getDetail();
    vm.firstFlg = false;
    vm.passWordFlg = false;
    vm.rePassword = '';
    vm.passWordUpdate = function(){
        if (!vm.firstFlg){
            vm.prePassword = vm.model.password;
            vm.firstFlg = true;
        }
        if(vm.passWordFlg===false) {
            vm.passWordFlg = true;
        }else{
            vm.passWordFlg = false;
            vm.model.password = vm.prePassword;
            vm.rePassword = '';
        }
    };
    vm.passWordSet = function(){
        if(vm.passWordFlg === false){
            vm.model.password = '1';
        }
    };
    AdminUserSrv.initChooseRoleModal(vm);
}

module.exports = {
    name: 'AdminUserEditCtrl',
    fn: AdminUserEditCtrl
};
