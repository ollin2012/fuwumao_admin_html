'use strict';
/**
 * 管理员用户 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.adminUser', {
        template: '<div ui-view></div>',
        url: '/adminUser',
        abstract: true
    })
    .state('main.adminUser.list', {
        url: '/',
        templateUrl: 'adminUser/views/adminUser-list.html',
        controller: 'AdminUserListCtrl as vm',
    })    
    .state('main.adminUser.new', {
    	url: '/new',
        templateUrl: 'adminUser/views/adminUser-new.html',
        controller: 'AdminUserNewCtrl as vm',
    })
    .state('main.adminUser.detail', {
        url: '/:id/detail',
        templateUrl: 'adminUser/views/adminUser-detail.html',
        controller: 'AdminUserDetailCtrl as vm',
    })
    .state('main.adminUser.edit', {
        url: '/:id/edit',
        templateUrl: 'adminUser/views/adminUser-edit.html',
        controller: 'AdminUserEditCtrl as vm',
    })
    .state('main.adminUser.delete', {
        url: '/:id/delete',
        templateUrl: 'adminUser/views/adminUser-detail.html',
        controller: 'AdminUserDeleteCtrl as vm'
    });
};
