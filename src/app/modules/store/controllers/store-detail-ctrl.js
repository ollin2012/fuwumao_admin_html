/**
 * 控制器：门店详情
 */
'use strict';

function StoreDetailCtrl($controller, $stateParams, ApiSrv) {
    'ngInject';

    let vm = this;

    let ctrlOpts = {
        modelName: 'store'
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    // 自定义详情取得
    vm.getDetail = function () {
        let apiParams = {};
        apiParams.storeId = $stateParams.id;
        ApiSrv.exec('store/getStoreDetail', apiParams)
            .then(function (data) {
                vm.model = data;
            });
    };

    vm.getDetail();
}

module.exports = {
    name: 'StoreDetailCtrl',
    fn: StoreDetailCtrl
};
