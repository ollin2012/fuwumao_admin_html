/**
 * 控制器：门店列表
 */
'use strict';

function StoreListCtrl($controller, $scope, StoreAreaSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;

    let ctrlOpts = {
        modelName: 'store',
        dataUrl: '/getStoreList'
    };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    // 地区list 初期化
    vm.areaList1 = [];
    vm.areaList2 = [];
    vm.areaList3 = [];

    // 地区一,二，三下拉框初期化
    StoreAreaSrv.initAreaList($scope, vm, 'searchCondition.area1', 'searchCondition.area2', 'searchCondition.area3');
}

module.exports = {
    name: 'StoreListCtrl',
    fn: StoreListCtrl
};
