/**
 * 控制器：订单详情
 */
'use strict';

function StoreOrderDetailCtrl($controller, $stateParams, ApiSrv) {
    'ngInject';

    let vm = this;
    let ctrlOpts = {
        modelName: 'store'
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    // 自定义详情取得
    vm.getDetail = function () {
        let apiParams = {};
        apiParams.orderId = $stateParams.id;
        ApiSrv.exec('store/getOrderDetail', apiParams)
            .then(function (data) {
                vm.model = data;
            });
    };

    vm.getDetail();
}

module.exports = {
    name: 'StoreOrderDetailCtrl',
    fn: StoreOrderDetailCtrl
};
