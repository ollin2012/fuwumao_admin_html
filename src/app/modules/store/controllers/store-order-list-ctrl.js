/**
 * 控制器：订单列表
 */
'use strict';

function StoreOrderListCtrl($controller, MultiselectSrv, StoreSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;

    // 处理订单状态复选参数
    function preSearchFn(apiParams) {
        apiParams.orderStatusList = [];

        for (var key in vm.statusList) {
            apiParams.orderStatusList.push(vm.statusList[key].id);
        }
    }

    let ctrlOpts = {
        modelName: 'store',
        dataUrl: '/getOrderList',
        preSearchFn: preSearchFn
    };

    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    // 已选的订单状态
    vm.statusList = MultiselectSrv.toVm(vm.searchCondition.status);

    // 所有订单状态
    vm.allStatusList = StoreSrv.convAllStatusForMultiSelect(true);

    // 重置
    vm.resetCondition = function () {
        vm.reset();
        vm.statusList = [];
    };
}

module.exports = {
    name: 'StoreOrderListCtrl',
    fn: StoreOrderListCtrl
};
