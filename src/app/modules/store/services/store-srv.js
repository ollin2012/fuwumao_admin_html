'use strict';

/**
 * 门店服务
 */

const _ = require('lodash');

function StoreSrv($stateParams, ApiSrv, AppConfigs,CodeList) {
    'ngInject';

    // 获取门店 地区Master信息 
    function getAllAreasFromStore(vm) {

        vm.allAreas = [];

        // 门店API
        let apiUrl = AppConfigs.API_STORE_URL + '/area/getList';
        ApiSrv.exec(apiUrl)
            .then(function (data) {
                if (data) {
                    vm.allAreas = data;
                }
            });
    }

    // 生成订单状态下拉多选项
    function convAllStatusForMultiSelect() {

        let codes = CodeList.orderStatusCom;

        return _.map(codes, function (value, key) {
            return {
                id: key,
                label: value
            };
        });
    }

    return {
        getAllAreasFromStore,
        convAllStatusForMultiSelect

    };
}

module.exports = {
    name: 'StoreSrv',
    fn: StoreSrv
};