'use strict';

let _ = require('lodash');

/**
 * @class StoreAreaSrv 地区相关组件（门店）
 * @alias module:common/services.StoreAreaSrv
 * 
 */

function StoreAreaSrv($q, $translate, ApiSrv) {
    'ngInject';

    /**
     * @method module:common/services.AreaSrv#getAreaList 
     * @description 根据parentId取得地区列表
     * @memberOf AreaSrv
     * @param {String} [parentId]  父ID
     * @return Promise
     */
    let _getAreaList = function (parentId) {
        let d = $q.defer();

        // 取得地区列表
        ApiSrv.exec('store/getAreaList', { parentId: parentId })
            .then(function (results) {
                return d.resolve(results);
            });
        return d.promise;
    };

    /**
     * @method module:common/services.AreaSrv#setArea2List 
     * @description 取得二级地区下来框选择内容
     * @memberOf AreaSrv
     * @param {String} parentId     上级地区ID（vm中的地区二选择对象ID）
     * @param {Object} vm   控制器的value model
     * @return void
     */
    let _setArea2List = function (_parentId, vm) {
        // 取得二级地区
        _getAreaList(_parentId).then(function (data) {
            vm.areaList2 = data;
        });
    };

    /**
     * @method module:common/services.AreaSrv#setArea3List 
     * @description 取得三级地区下来框选择内容
     * @memberOf AreaSrv
     * @param {String} parentId     上级地区ID（vm中的地区二选择对象ID）
     * @param {Object} vm   控制器的value model
     * @return void
     */
    let _setArea3List = function (_parentId, vm) {
        // 取得三级地区
        _getAreaList(_parentId).then(function (data) {
            vm.areaList3 = data;
        });
    };

    /**
     * @public
     * @method module:common/services.AreaSrv#initAreaList
     * @description 初期化一级，二级，三级地区下来框选择内容
     * @example
     *    控制层： 
     *       // 地区一,二下拉框初期化 
     *       // 'searchCondition.industry1' 为地区一绑定的key 注意去掉vm
     *       // 'searchCondition.industry2' 为地区一绑定的key 注意去掉vm
     *       IndustrySelectSrv.initIndustryList(vm, 'searchCondition.industry1', 'searchCondition.industry2');
     *    显示层： 
     *        <!--地区一-->
     *        <!--ng-model 绑定可以是vm里的任意值， ng-options,ng-change,ng-click 写法固定-->
     *        <select name="companyArea1" class="form-control"
     *                 ng-model="vm.searchCondition.companyArea1" 
     *                 ng-options="'' + area.id as area.name  for area in vm.areaList1">
     *             <option value=""></option>
     *         </select>
     *         <!--地区二-->
     *         <select name="companyArea2" class="form-control"
     *                 ng-model="vm.searchCondition.companyArea2" 
     *                 ng-options="'' + area.id as area.name  for area in vm.areaList2">
     *             <option value=""></option>
     *         </select>
     *         <!--地区三-->
     *         <select name="companyArea3" class="form-control"
     *                 ng-model="vm.searchCondition.companyArea3" 
     *                 ng-options="'' + area.id as area.name  for area in vm.areaList3">
     *             <option value=""></option>
     *         </select>
     * @param {Object} vm 	控制器的value model
     * @param {String} area1Key 	一级地区选择项绑定的vm对象的key(例子'vm.searchCondition.area1' 
     * 								入力'searchCondition.area1')
     * @param {String} area2Key 	二级级地区选择项绑定的vm对象的key(例子'vm.searchCondition.area2' 
     * 								入力'searchCondition.area2')
     * @param {String} area3Key 	三级级地区选择项绑定的vm对象的key(例子'vm.searchCondition.area3' 
     * 								入力'searchCondition.area3')
     * @return void
     */
    let _initAreaList = function ($scope, vm, area1Key, area2Key, area3Key) {
        if (!vm) {
            vm = {};
        }
        // 地区list 初期化
        vm.areaList1 = [];
        vm.areaList2 = [];
        vm.areaList3 = [];


        // 取得所有地区
        _getAreaList().then(function (result) {
            // 取得一级地区下来框选择内容
            vm.areaList1 = result;

            // 一级地区选择项不为空时
            let area1 = _.at(vm, area1Key)[0];
            if (area1) {
                // 取得二级地区下来框选择内容
                _setArea2List(area1, vm);
            }
            // 二级地区选择项不为空时
            let area2 = _.at(vm, area2Key)[0];
            if (area2) {
                // 取得二级地区下来框选择内容
                _setArea3List(area2, vm);
            }

            // 一级地区选项变更时
            let changeArea1 = function () {
                // 地区一选择项为空白时
                let _parentId = _.at(vm, area1Key)[0];
                let _parentIdStr = (_parentId === null || _parentId === undefined) ? '' : _parentId.toString();

                if (_.isEmpty(_parentIdStr)) {
                    // 地区二下拉内容清空
                    vm.areaList2 = [];
                    // 地区三下拉内容清空
                    vm.areaList3 = [];
                } else {
                    // 取得二级地区下来框选择内容
                    _setArea2List(_parentId, vm);
                    // 地区三下拉内容清空
                    vm.areaList3 = [];
                }
                // 地区二 选择项清空
                _.set(vm, area2Key, null);
                // 地区三 选择项清空
                _.set(vm, area3Key, null);
            };
            let area1ValueChange = $scope.$watch(function () {
                return _.at(vm, area1Key)[0];
            }, function (oldVal, newVal) {
                if (newVal !== oldVal) {
                    changeArea1();
                }
            });

            // 二级地区选项变更时
            let changeArea2 = function () {
                // 地区一选择项为空白时
                let _parentId = _.at(vm, area2Key)[0];

                let _parentIdStr = _parentId === null ? '' : _parentId.toString();

                if (_.isEmpty(_parentIdStr)) {

                    // 地区三下拉内容清空
                    vm.areaList3 = [];
                } else {
                    // 取得三级地区下来框选择内容
                    _setArea3List(_parentId, vm);
                }
                // 地区三 选择项清空
                _.set(vm, area3Key, null);
            };
            let area2ValueChange = $scope.$watch(function () {
                return _.at(vm, area2Key)[0];
            }, function (oldVal, newVal) {
                if (newVal !== oldVal) {
                    changeArea2();
                }
            });

            $scope.$on('$destroy', function () {
                area1ValueChange();
                area2ValueChange();
            });
        });
    };

    return {
        initAreaList: _initAreaList,
        getAreaList: _getAreaList
    };
}

module.exports = {
    name: 'StoreAreaSrv',
    fn: StoreAreaSrv
};