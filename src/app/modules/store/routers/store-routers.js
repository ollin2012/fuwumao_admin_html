'use strict';
/**
 * 门店 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.store', {
        template: '<div ui-view></div>',
        url: '/store',
        abstract: true
    })
    .state('main.store.list', {
        url: '/',
        templateUrl: 'store/views/store-list.html',
        controller: 'StoreListCtrl as vm',
    })
    .state('main.store.detail', {
        url: '/:id/detail',
        templateUrl: 'store/views/store-detail.html',
        controller: 'StoreDetailCtrl as vm',
    })
    .state('main.store.orderList', {
        url: '/orderList',
        templateUrl: 'store/views/store-order-list.html',
        controller: 'StoreOrderListCtrl as vm',
    })
    .state('main.store.orderDetail', {
        url: '/:id/orderDetail',
        templateUrl: 'store/views/store-order-detail.html',
        controller: 'StoreOrderDetailCtrl as vm',
    });
};
