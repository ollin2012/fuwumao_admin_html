/**
 * 控制器：主画面左侧边栏
 */
'use strict';
let _ = require('lodash');

function MainSidebarCtrl($interval, SessionSrv) {
	'ngInject';

	let vm = this;
	vm.menus = {};
	vm.currentUser = SessionSrv.getCurrentUser();

	/**
	 * 点击菜单
	 * @param  {string} name 菜单名
	 */
	vm.clickMenu = function(name) {
		_.forIn(vm.menus, function(menu, key) {
  			if (key === name) {
  				menu.isOpen = !menu.isOpen;
  			} else {
  				menu.isOpen = false;
  			}
		});
	};

	/**
	 * 菜单是否打开
	 * @param  {string}  name 菜单名
	 * @return {Boolean}      菜单是否打开
	 */
	vm.isOpen = function(name) {
		let menu = vm.menus[name] = (vm.menus[name] || {});
		return menu.isOpen;
	};

//  function callAtInterval() {
//  	// 【待处理企业数轮循】
//  	let companyParams = {
//  			page: 1,
//  			count: 10,
//  			shelveCompanyFlg: '1'
//  	};
//  	ApiSrv.exec('company/getList', companyParams)
//      .then(function(data) {
//          if (data && data.count > 0) {
//          	// 未读消息数标记
//              vm.unhandleCompanyCount = ' (' + data.count + ')';
//          }
//      });
//
//  	// 【待处理服务数轮循】
//  	let serviceParams = {
//  			page: 1,
//  			count: 10,
//  			serviceStatus: '2' // 待审核
//  	};
//  	ApiSrv.exec('service/getList', serviceParams)
//      .then(function(data) {
//          if (data && data.count > 0) {
//          	// 未读消息数标记
//              vm.unhandleServiceCount = ' (' + data.count + ')';
//          }
//      });
//
//  	// 【待处理交易数轮循】
//  	let tradeParams = {
//  			page: 1,
//  			count: 10,
//  			tradeStatus: '70' // 待审核
//  	};
//  	ApiSrv.exec('trade/getList', tradeParams)
//      .then(function(data) {
//          if (data && data.count > 0) {
//          	// 未读消息数标记
//              vm.unhandleTradeCount = ' (' + data.count + ')';
//          }
//      });
//  }
//
//  // 【轮循处理】
//  $interval(callAtInterval, 300000);
//
//  // 画面初期，表示各未处理数
//  callAtInterval();
}

module.exports = {
    name: 'MainSidebarCtrl',
    fn: MainSidebarCtrl
};
