/**
 * 控制器：欢迎
 */
'use strict';

function MainWelcomeCtrl($state) {
    'ngInject';

    // let vm = this;

    $state.go('main.message.list');
    // // 地区的例子
    // // 地区（从数据库抽出）
    // vm.areaDb = [{
    //     area1: '中国',
    //     area2: '浙江',
    //     area3: '杭州'
    // }, {
    //     area1: '中国',
    //     area2: '浙江',
    //     area3: '苏州'
    // }, {
    //     area1: '中国',
    //     area2: '上海'
    // }, {
    //     area1: '美国'
    // }];

    // // 地区（转化成画面表示）
    // vm.areaView = [{
    //     item: '中国',
    //     itemChilds: [
    //         '浙江 > 杭州 苏州',
    //         '上海'
    //     ]
    // }, {
    //     item: '美国'
    // }];

    // vm.date = new Date();
    // vm.dateString = '2017-01-13 12:13:14';
}

module.exports = {
    name: 'MainWelcomeCtrl',
    fn: MainWelcomeCtrl
};
