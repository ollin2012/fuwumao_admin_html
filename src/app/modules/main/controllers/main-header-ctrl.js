/**
 * 控制器：主画面头部
 */
'use strict';

function MainHeaderCtrl($state, $uibModal, $scope, $rootScope, $interval, $filter,
                        ApiSrv, SessionSrv, MessageSrv) {
    'ngInject';

    let vm = this;
    vm.currentUser = SessionSrv.getCurrentUser();
    vm.unreadMessagesCount = 0;

    let MAX_MESSAGES = 5;

    vm.hasMoreUserMessages = false;

    function callAtInterval() {
        vm.userMessages = [];
    	let messageParams = {
    			page: 1,
    			count: 5,
    			userId: vm.currentUser.id,
    			readFlg: '0'
    	};
    	ApiSrv.exec('message/getList', messageParams)
	        .then(function(data) {
                if (data && data.list && data.list.length > 0) {
                	// 未读消息数标记
                    vm.unreadMessagesCount = data.count;

                    // 设置循环处理次数
                	if (MAX_MESSAGES < data.list.length) {
                	    vm.hasMoreUserMessages = true;
                	}

                	// 编辑消息内容
                	let messageList = data.list;
                	for (let i=0; i<MAX_MESSAGES; i++) {
                        vm.userMessages.push({
                        	id: messageList[i].id,
                            sendTime: $filter('dateFormat')(messageList[i].sendTime, 'YYYY-MM-DD HH:mm'),
                            templateCode: messageList[i].templateCode,
                            templateParam: MessageSrv.getMessageParams(messageList[i].templateParam),
                            links: messageList[i].links + '({id:' + messageList[i].receiverId + '})'
                        });
                	}
                }
	        });
    }

    // 当消息通知到达时， 更新未读消息件数
    $interval(callAtInterval, 300000);

    // 点击消息后，将该消息的状态更新为已读
    vm.messageReaded = function(messageId) {
    	ApiSrv.exec('message/updateFlg', {messageId: messageId})
    	.then(function() {
        	callAtInterval();
    	});
    };

    // // 监听消息一览画面的消息已读变化状态
    // $rootScope.$on('msgNotice', function() {
	// 	callAtInterval();
	// })

    // 用户登出
    vm.logout = function() {
        function _proc() {
            SessionSrv.clearCurrentUser();
            $state.go('login');
        }
        MessageSrv.confirm('confirm.logout').then(_proc);
    };

    // 修改密码
    vm.changePassword = function() {
        $uibModal.open({
            templateUrl: 'user/views/user-changePassword.html',
            controller: 'UserChangePasswordCtrl as vm'
        });
    };

    // 画面初期，取得未读消息
    callAtInterval();
}

module.exports = {
    name: 'MainHeaderCtrl',
    fn: MainHeaderCtrl
};
