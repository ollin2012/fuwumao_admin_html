'use strict';
/**
 * 企业 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.company', {
        template: '<div ui-view></div>',
        url: '/company',
        abstract: true
    })
    .state('main.company.list', {
        url: '/',
        templateUrl: 'company/views/company-list.html',
        controller: 'CompanyListCtrl as vm',
    })
    .state('main.company.unhandleList', {
        url: '/unhandleList',
        templateUrl: 'company/views/company-list.html',
        controller: 'CompanyListCtrl as vm',
    })
    .state('main.company.agencyList', {
        url: '/agencyList',
        templateUrl: 'company/views/company-list.html',
        controller: 'CompanyListCtrl as vm',
    })
    .state('main.company.detail', {
        url: '/:id/detail',
        templateUrl: 'company/views/company-detail.html',
        controller: 'CompanyDetailCtrl as vm',
    })
    .state('main.company.companyCert', {
        url: '/:id/companyCert',
        templateUrl: 'company/views/company-detail.html',
        controller: 'CompanyCertCtrl as vm',
    })
    .state('main.company.agencyProfileCert', {
        url: '/:id/agencyProfileCert',
        templateUrl: 'company/views/company-detail.html',
        controller: 'CompanyAgencycertCtrl as vm'
    })
    .state('main.company.agencyDepositCert', {
        url: '/:id/agencyDepositCert',
        templateUrl: 'company/views/company-detail.html',
        controller: 'CompanyAgencycertCtrl as vm'
    })
    .state('main.company.agencyBecomeCert', {
        url: '/:id/agencyBecomeCert',
        templateUrl: 'company/views/company-detail.html',
        controller: 'CompanyAgencycertCtrl as vm'
    })
    .state('main.company.annualFeeCert', {
        url: '/:id/annualFeeCert',
        templateUrl: 'company/views/company-detail.html',
        controller: 'AnnualFeeCertCtrl as vm'
    });    
};
