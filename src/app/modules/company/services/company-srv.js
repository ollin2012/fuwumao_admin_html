'use strict';

/**
 * 企业服务
 */

function CompanySrv($stateParams, $uibModal, $state, $filter, ApiSrv, MessageSrv, uibDateParser, AppConfigs, DateSrv) {
    'ngInject';

    /* 企业详细信息取得 */

    function initDetail(vm) {
        // 服务商情报初期化
        vm.agencyInfo = {};
        // 保证金情报初期化
        vm.agencyDeposit = {};
        // 企业认证情报初期化
        vm.companyCertInfo = {};
        // 服务商证情报初期化
        vm.agencyCertInfo = {};

        // 企业详细取得
        vm.getCompanyDetail = function() {
            ApiSrv.exec('company/getDetail', {
                id: $stateParams.id,
                publishLanguage: null
            }).then(function(data) {
                vm.model = data;
                
                let languageList = vm.model.languageList;
                let thisLanguageList = [];
                for (let x=0; x<languageList.length; x++) {
                	let lang = {
                			id: languageList[x].language,
                			name: $filter('codeText')(languageList[x].language, 'language')
                	};
                	thisLanguageList.push(lang);
                }
                vm.model.languageList = [];
                vm.model.languageList = thisLanguageList;
                
                // 默认语言设定
                vm.publishLanguage = vm.model.preferredLanguage;

                // 地区行业编辑（转化成画面表示）
                vm.editView(vm);

                // 服务商的场合
                if (String(vm.model.agencyFlg) === '1') {
                	// 保证金取得（初期化的场合）
                    //vm.getAgencyDeposit();
                    
                    // 前台链接地址做成
                    vm.frontCompanyInfoUrl = AppConfigs.FRONT_BASE_URL + 'company/' + data.id;
                    
                // 企业的场合
                } else {
                    // 前台链接地址做成
                    vm.frontCompanyInfoUrl = AppConfigs.FRONT_BASE_URL + 'shop/' + data.id + '/companyinfo';
                }
                
                // 【取得企业认证情报】
                vm.getCompanyCertInfo();
            });
        };
        vm.getCompanyDetail(null);

        // 企业详细取得
        vm.changeLanguage = function() {
            ApiSrv.exec('company/getDetail', {
                id: $stateParams.id,
                publishLanguage: vm.publishLanguage
            }).then(function(data) {
                vm.model = data;
                
                let languageList = vm.model.languageList;
                let thisLanguageList = [];
                for (let x=0; x<languageList.length; x++) {
                	let lang = {
                			id: languageList[x].language,
                			name: $filter('codeText')(languageList[x].language, 'language')
                	};
                	thisLanguageList.push(lang);
                }
                vm.model.languageList = [];
                vm.model.languageList = thisLanguageList;

                // 地区行业编辑（转化成画面表示）
                vm.editView(vm);
            });
        };

        // 地区行业编辑
        vm.editView = function(vm) {
            // 地区编辑（转化成画面表示）
            vm.model.areaView = vm.areaViewEdit([{
                area1: vm.model.companyArea1
            }, {
                area1: vm.model.companyArea2
            }, {
                area1: vm.model.companyArea3
            }]);

            // 行业编辑
            vm.model.industryView = vm.industryViewEdit([{
                industry1: vm.model.industry1,
            }, {
                industry1: vm.model.industry2,
            }]);
        };

        // 服务商保证金取得
        vm.getAgencyDeposit = function() {
            ApiSrv.exec('agencyDeposit/getPool', {
                companyId: $stateParams.id
            }).then(function(data) {
                vm.agencyDeposit = data;
            });
        };

        // 服务商情报取得
        vm.getAgencyInfo = function() {
            ApiSrv.exec('agency/getDetail', {
                companyId: $stateParams.id
            }).then(function(data) {
            	if (data) {
                    vm.agencyInfo = data;

                    // 地区编辑（转化成画面表示）
                    vm.agencyInfo.areaView = vm.areaViewEdit(vm.agencyInfo.agencyAreaList);

                    // 行业编辑
                    vm.agencyInfo.industryView = vm.industryViewEdit(vm.agencyInfo.agencyIndustryList);
            	}
                
                // 【取得服务商认证情报】
                vm.getAgencyCertInfo();
            });
        };

        // 地区编辑（转化成画面表示）
        vm.areaViewEdit = function(areaList) {
            let areaView = [];
            for (let i = 0; i < areaList.length; i++) {
                let areaViewItem = {};
                areaViewItem.other = areaList[i].areaOther;
                areaViewItem.item = areaList[i].area1;
                if (areaList[i].area2 && areaList[i].area3) {
                    areaViewItem.itemChilds = [];
                    areaViewItem.itemChilds.push(areaList[i].area2);
                    areaViewItem.itemChilds.push(areaList[i].area3);
                } else if (areaList[i].area2) {
                    areaViewItem.itemChilds = [];
                    areaViewItem.itemChilds.push(areaList[i].area2);
                }
                areaView.push(areaViewItem);
            }
            return areaView;
        };

        // 行业编辑（转化成画面表示）
        vm.industryViewEdit = function(industryList) {
            let industryView = [];
            for (let i = 0; i < industryList.length; i++) {
                let industryViewItem = {};
                industryViewItem.other = industryList[i].industryOther;
                industryViewItem.item = industryList[i].industry1;
                if (industryList[i].industry2) {
                    industryViewItem.itemChilds = [];
                    industryViewItem.itemChilds.push(industryList[i].industry2);
                }
                industryView.push(industryViewItem);
            }
            return industryView;
        };

        // 企业认证情报取得
        vm.getCompanyCertInfo = function() {
            ApiSrv.exec('companyCert/getList', {
                companyId: $stateParams.id
            }).then(function(data) {
                vm.companyCertInfo = data;

                // 认证情报编辑
                // let certContent = JSON.parse(vm.companyCertInfo.certContentJson);
                // let certContentlist = [];
                // for (let key in certContent) {
                //     certContentlist.push(key + ': ' + certContent[key]);
                // }
                // vm.companyCertInfo.certContentlist = certContentlist;
                
                // 【取得服务商情报】
                vm.getAgencyInfo();
            });
        };

        // 服务商认证情报取得
        vm.getAgencyCertInfo = function() {
            ApiSrv.exec('agencyCert/getList', {
                companyId: $stateParams.id
            }).then(function(data) {
            	if (data) {
                    vm.agencyCertInfo = data;
                    
                    if (vm.agencyCertInfo.certContentJson) {
                        let contentCert = JSON.parse(vm.agencyCertInfo.certContentJson);
                        let contentCertList = [];
                        for(let key in contentCert) {
                        	let element = {};
                        	element.title = key;
                        	element.content = contentCert[key];
                        	contentCertList.push(element);
                        }
                        
                        if (vm.agencyCertInfo.verifyFile && vm.agencyCertInfo.verifyFile.length > 0) {
                        	for (let i=0; i < vm.agencyCertInfo.verifyFile.length; i++) {
                        		for (let j=0; j < contentCertList.length; j++) {
                        			if (contentCertList[j].title === 'businessCardsfileList' && contentCertList[j].content.length > 0) { // 三证扫描件
                        				for (let k=0; k < contentCertList[j].content.length; k++) {
                                			if (vm.agencyCertInfo.verifyFile[i].fileId === contentCertList[j].content[k].fileId) {
                                				contentCertList[j].content[k].fileName = vm.agencyCertInfo.verifyFile[i].fileName;
                                			}
                        				}
                        			} else if (contentCertList[j].title === 'idCardsfileList' && contentCertList[j].content.length > 0) { // 法人身份证明
                        				for (let k=0; k < contentCertList[j].content.length; k++) {
                                			if (vm.agencyCertInfo.verifyFile[i].fileId === contentCertList[j].content[k].fileId) {
                                				contentCertList[j].content[k].fileName = vm.agencyCertInfo.verifyFile[i].fileName;
                                			}
                        				}
                        			} else {
                        				continue;
                        			}
                        		}
                        	}
                        }
                        
                        vm.agencyCertInfo.contentCertList = contentCertList;
                    }
            	}
                
                // 【年费缴纳情报取得】
                vm.getAnnualFeeInfo();
            });
        };

        // 年费缴纳情报取得
        vm.getAnnualFeeInfo = function() {
        	vm.annualFeeInfo = {};
            ApiSrv.exec('annualFee/getList', {
                companyId: $stateParams.id
            }).then(function(data) {
            	if (data && data.list && data.list.length > 0) {
            		for (let i=0; i<data.list.length; i++) {
            			if (data.list[i].agencyFlg === vm.model.agencyFlg) {
                    		vm.annualFeeInfo.annualFeePayStatus = '0'; //未支付
                    		
                            let startDate = DateSrv.toDate(data.list[0].startDate);
                            let endDate = DateSrv.toDate(data.list[0].endDate);
                            let nowDate = new Date();
                            let leftMilliseconds = nowDate.getTime() - startDate.getTime();
                            let rightMilliseconds = nowDate.getTime() - endDate.getTime();
                    		if (leftMilliseconds > 0 && rightMilliseconds < 0) {
                    			vm.annualFeeInfo.annualFeePayStatus = '1';  //已支付
                    		}
                    		break;
            			} else {
                    		vm.annualFeeInfo.annualFeePayStatus = '0'; //未支付
            			}
            		}
            		vm.annualFeeInfo.payeeDate = data.list[0].payeeDate;
            		vm.annualFeeInfo.agencyFlg = data.list[0].agencyFlg;
            		vm.annualFeeInfo.list = data.list;
            	} else {
            		vm.annualFeeInfo.annualFeePayStatus = '0'; //未支付
            	}
            });
        };
    }

    /* 企业冻结解冻 */
    function dealFreezeModal(vm) {
		let modalParams = {
				freezeCompanyId: vm.model.id,
				freezeCompanyName: vm.model.companyName,
				freezeCompanyStatus: vm.model.companyStatus,
				freezeCompanyUpdateTime: vm.model.updateTime
		};

		let freezeModel = $uibModal.open({
			templateUrl : 'company/views/company-freeze.html',
			size: 'lg',
			controller: 'CompanyFreezeCtrl as vmFreeze',
			resolve: {
				modalParams: function(){
					return modalParams;
				}
			}
		});

		freezeModel.result.then(function() {
            MessageSrv.success('message.freezeOk');
            // 画面刷新
            $state.reload();
        }, function(){});
    }

    return {
        initDetail,
        dealFreezeModal
    };
}

module.exports = {
    name: 'CompanySrv',
    fn: CompanySrv
};