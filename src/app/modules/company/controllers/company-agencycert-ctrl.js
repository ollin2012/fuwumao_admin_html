/**
 * 控制器：企业认证
 */
'use strict';

function CompanyAgencycertCtrl($controller, CompanySrv, ApiSrv, SessionSrv, CodeList, CommonConstants, MessageSrv) {
    'ngInject';

    let vm = this, ctrlOpts = {
        modelName: 'company'
    };
    
    vm.storeAgencyFlg = '0'; // 初期设定为“非门店类服务商”
    
    angular.extend(this, $controller('BaseCrudCtrl', {
        vm: vm,
        ctrlOpts: ctrlOpts
    }));

    // 企业详细情报取得
    CompanySrv.initDetail(vm);

    // 门店类服务商区分设定
    vm.storeAgencyFlgList = [];
    for (let key in CodeList.storeAgencyFlg) {
        let item = {};
        item.key = key;
        item.value = CodeList.storeAgencyFlg[key];
        //if (item.key == '0') {
        //	vm.storeAgencyFlg = item.key;
        //}
        vm.storeAgencyFlgList.push(item);
    }

    // 服务商认证结果描述设定
    vm.agencyResultList = [];
    for (let key in CommonConstants.agencyResult) {
        let item = {};
        item.key = key;
        item.value = CommonConstants.agencyResult[key];
        vm.agencyResultList.push(item);
    }
    
    // 懒人标签选择
    vm.selectAgencyResult = function(index) {
        vm.agencyResultDetail = CommonConstants.agencyResultDetail[index];
    };
    
    // 服务商审核确定
    vm.agencyVerifyConfirm = function() {
        MessageSrv.confirm('confirm.verify').then(() => {
        	let agencyVerifyData = {
        			agencyCertId: vm.agencyCertInfo.agencyCertId,
        			verifyResultFlg: vm.angencyVerifyResultFlg,
        			verifyResultDetail: vm.agencyResultDetail,
        			verifierCompanyId: '0',
        			verifierId: SessionSrv.getCurrentUser().id,
        			updateTime: vm.agencyCertInfo.updateTime,
        			companyId: vm.model.id,
        			storeAgencyFlg: vm.storeAgencyFlg,
        			agencyUpdateTime: vm.agencyInfo.updateTime
        	};

            // ajax企业审核确定
            return ApiSrv.exec('agencyCert/dataVerify', agencyVerifyData)
                .then(function() {
                    MessageSrv.success('message.verifyOk');
                    vm.back();
                });
        });
    };
    
    // 服务商入驻保证金审核确定
    vm.agencyDepositVerifyConfirm = function() {
        MessageSrv.confirm('confirm.verify').then(() => {
        	let agencyDepositVerifyData = {
        			agencyCertId: vm.agencyCertInfo.agencyCertId,
        			companyId: vm.model.id,
        			depositMemo: vm.capitalFlowMemo,
        			agencyUpdateTime: vm.agencyCertInfo.agencyUpdateTime
        			//depositUpdateTime : vm.agencyCertInfo.depositUpdateTime
        	};

            // ajax企业审核确定
            return ApiSrv.exec('agencyCert/depositVerify', agencyDepositVerifyData)
                .then(function() {
                    MessageSrv.success('message.verifyOk');
                    vm.back();
                });
        });
    };
    
    // 成为服务商
    vm.becomeAgencyConfirm = function() {
        MessageSrv.confirm('confirm.adopt').then(() => {
        	let becomeAgencyData = {
        			companyId: vm.model.id,
        			updateTime : vm.model.updateTime,
        			agencyUpdateTime : vm.agencyInfo.updateTime
        	};

            // ajax企业审核确定
            return ApiSrv.exec('agencyCert/success', becomeAgencyData)
                .then(function() {
                    MessageSrv.success('message.verifyOk');
                    vm.back();
                });
        });
    };

    //企业冻结/解冻模态框
    vm.dealFreezeModal = function() {
    	CompanySrv.dealFreezeModal(vm);
    };
}

module.exports = {
    name: 'CompanyAgencycertCtrl',
    fn: CompanyAgencycertCtrl
};