/**
 * 控制器：企业列表
 */
'use strict';

const _ = require('lodash');

function CompanyListCtrl($controller, $state, $scope, ApiSrv, MessageSrv, $location, IndustrySelectSrv, AreaSelectSrv) {
    'ngInject';

    // 扩展自list控制器基类
    let vm = this;

    // 取得当前处理的Action类别
    vm.action = _.last(_.split($state.current.name, '.'));

    // 判断迁移元画面
    // 检索前处理
    function preSearchFn(apiParams) {
    	if (vm.action === 'unhandleList') {
            apiParams.shelveCompanyFlg = '1'; // 0：不需要；1：需要
    	} else {
    		apiParams.shelveCompanyFlg = '0'; // 0：不需要；1：需要
    	}
        if (vm.action === 'agencyList') {
            apiParams.agencyFlg = '1'; // 0：企业；1服务商
    		vm.searchCondition.agencyFlg = '1'; // 0：企业；1服务商
        }
    }

    let ctrlOpts = {
            modelName: 'company',
            preSearchFn: preSearchFn
        };
    
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    // 行业list 初期化
    vm.industryList1 = [];
    vm.industryList2 = [];

    // 地区list 初期化
    vm.areaList1 = [];
    vm.areaList2 = [];
    vm.areaList3 = [];
    
    // 行业一,二下拉框初期化
    IndustrySelectSrv.initIndustryList($scope, vm, 'searchCondition.industry1', 'searchCondition.industry2');
    
    // 地区一,二，三下拉框初期化
    AreaSelectSrv.initAreaList($scope, vm, 'searchCondition.companyArea1', 'searchCondition.companyArea2', 'searchCondition.companyArea3');
}

module.exports = {
    name: 'CompanyListCtrl',
    fn: CompanyListCtrl
};
