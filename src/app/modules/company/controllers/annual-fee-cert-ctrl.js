/**
 * 控制器：企业认证
 */
'use strict';

function AnnualFeeCertCtrl($controller, CompanySrv, ApiSrv, MessageSrv) {
    'ngInject';

    let vm = this, ctrlOpts = {
        modelName: 'company'
    };
    
    vm.annualFeeAgencyFlg = '0';
    vm.annualFeeLevel = '1';
    
    angular.extend(this, $controller('BaseCrudCtrl', {
        vm: vm,
        ctrlOpts: ctrlOpts
    }));

    // 企业详细情报取得
    CompanySrv.initDetail(vm);
    
    // 年费审核确定
    vm.agencyAnnualFeeVerifyConfirm = function() {
        MessageSrv.confirm('confirm.verify').then(() => {
        	let annualFeeVerifyData = {
        			companyId: vm.model.id,
        			agencyFlg: vm.annualFeeAgencyFlg,
        			annualFeeGrade: vm.annualFeeLevel,
        			realAnnualFee: vm.annualFeeReal,
        			realCurrencyType: vm.annualFeeCurrency,
        			startDate: vm.annualFeeStartDate,
        			endDate: vm.annualFeeEndDate,
        			notes: vm.annualFeeNotes
        	};

            // ajax企业审核确定
            return ApiSrv.exec('annualFee/create', annualFeeVerifyData)
                .then(function() {
                    MessageSrv.success('message.verifyOk');
                    vm.back();
                });
        });
    };
    
    // 服务商区分选择，设定年费等级
    vm.annualFeeSet = function() {
    	if (vm.annualFeeAgencyFlg !== '1') {
    		vm.annualFeeLevel = '1';
    	}
    };

    //企业冻结/解冻模态框
    vm.dealFreezeModal = function() {
    	CompanySrv.dealFreezeModal(vm);
    };
}

module.exports = {
    name: 'AnnualFeeCertCtrl',
    fn: AnnualFeeCertCtrl
};