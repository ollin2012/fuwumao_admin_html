/**
 * 控制器：企业详情
 */
'use strict';

function CompanyDetailCtrl($controller, CompanySrv) {
    'ngInject';

    let vm = this, ctrlOpts = {
        modelName: 'company'
    };
    angular.extend(this, $controller('BaseCrudCtrl', {
        vm: vm,
        ctrlOpts: ctrlOpts
    }));
    
    // 企业详细情报取得
    CompanySrv.initDetail(vm);

    //企业冻结/解冻模态框
    vm.dealFreezeModal = function() {
    	CompanySrv.dealFreezeModal(vm);
    };
}

module.exports = {
    name: 'CompanyDetailCtrl',
    fn: CompanyDetailCtrl
};