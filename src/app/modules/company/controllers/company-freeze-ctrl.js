/**
 * 控制器：企业冻结解冻
 */
'use strict';

function CompanyFreezeCtrl($controller, ApiSrv, MessageSrv, modalParams, $uibModalInstance) {
    'ngInject';

    let vmFreeze = this;
    
    // 画面初期化
    vmFreeze.freezeCompanyId = modalParams.freezeCompanyId;
    vmFreeze.freezeCompanyName = modalParams.freezeCompanyName;
    vmFreeze.freezeCompanyStatus = modalParams.freezeCompanyStatus;
    vmFreeze.freezeCompanyUpdateTime = modalParams.freezeCompanyUpdateTime;
    
    // 确认按钮按下
    vmFreeze.freezeConfirm = function() {
        MessageSrv.confirm('confirm.verify').then(() => {
        	let frozenFlg = '';
        	if (vmFreeze.freezeCompanyStatus === '8') {
        		frozenFlg = '2';
        	} else {
        		frozenFlg = '1';
        	}
        	let freezeData = {
        			id: vmFreeze.freezeCompanyId,
        			frozenFlg: frozenFlg,
        			frozenReason: vmFreeze.freezeReason,
        			updateTime: vmFreeze.freezeCompanyUpdateTime
        	};

            // ajax企业冻结解冻
            return ApiSrv.exec('company/freezeUnfreeze', freezeData)
                .then(function() {
                	$uibModalInstance.close();
                });
        });
    };

}

module.exports = {
    name: 'CompanyFreezeCtrl',
    fn: CompanyFreezeCtrl
};