/**
 * 控制器：企业删除
 */
'use strict';

function CompanyDeleteCtrl($controller) {
    'ngInject';

    let vm = this,
        ctrlOpts = {
            modelName: 'company'
        };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    vm.getDetail();
}

module.exports = {
    name: 'CompanyDeleteCtrl',
    fn: CompanyDeleteCtrl
};
