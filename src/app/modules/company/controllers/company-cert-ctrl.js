/**
 * 控制器：企业认证
 */
'use strict';

function CompanyCertCtrl($controller, CompanySrv, ApiSrv, SessionSrv, CodeList, CommonConstants, MessageSrv) {
    'ngInject';

    let vm = this, ctrlOpts = {
        modelName: 'company'
    };
    
    vm.companyInstitute = '0'; // 初期设定为“非企业协会”
    
    vm.artificialCompanyCertRusultFlg = true;
    
    angular.extend(this, $controller('BaseCrudCtrl', {
        vm: vm,
        ctrlOpts: ctrlOpts
    }));

    // 企业详细情报取得
    CompanySrv.initDetail(vm);

    // 企业协会区分设定
    vm.companyInstituteList = [];
    for (let key in CodeList.companyInstitute) {
        let item = {};
        item.key = key;
        item.value = CodeList.companyInstitute[key];
        //if (item.key == '0') {
        //	vm.companyInstitute = item.key;
        //}
        vm.companyInstituteList.push(item);
    }

    // 企业认证结果描述设定
    vm.companyResultList = [];
    for (let key in CommonConstants.companyResult) {
        let item = {};
        item.key = key;
        item.value = CommonConstants.companyResult[key];
        vm.companyResultList.push(item);
    }
    
    // 懒人标签选择
    vm.selectCompanyResult = function(index) {
        vm.companyResultDetail = CommonConstants.companyResultDetail[index];
    };
    
    // 人工审核结果选择
    vm.artificialCompanyCert = function(result) {
    	if (result === '1') {
    		vm.artificialCompanyCertRusultFlg = true;
    	} else {
    		vm.artificialCompanyCertRusultFlg = false;
    	}
    };
    
    // 企业审核确定
    vm.companyVerifyConfirm = function() {
        MessageSrv.confirm('confirm.verify').then(() => {
        	let companyVerifyData = {
        			companyId: vm.model.id,
        			id: vm.companyCertInfo.companyCertId,
        			verifyResultFlg: vm.companyVerifyResultFlg,
        			verifyResultDetail: vm.companyResultDetail,
        			verifierCompanyId: '0',
        			verifierId: SessionSrv.getCurrentUser().id,
        			updateTime: vm.companyCertInfo.updateTime,
        			verifyAccountList: vm.companyCertInfo.companyCertAccount,
        			companyInstitute: vm.companyInstitute,
        			companyUpdateTime: vm.model.updateTime,
        			receiveBankCode: vm.receiveBankCode,
        			payBankCode: vm.payBankCode
        	};

            // ajax企业审核确定
            return ApiSrv.exec('companyCert/verify', companyVerifyData)
                .then(function() {
                    MessageSrv.success('message.verifyOk');
                    vm.back();
                });
        });
    };

    //企业冻结/解冻模态框
    vm.dealFreezeModal = function() {
    	CompanySrv.dealFreezeModal(vm);
    };
}

module.exports = {
    name: 'CompanyCertCtrl',
    fn: CompanyCertCtrl
};