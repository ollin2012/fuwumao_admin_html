'use strict';
/**
 * 消息一览 路由设置
 * @param  {Object} $stateProvider [参照ui-router的$stateProvider]
 * @return {function}              [路由设置function]
 */
module.exports = function($stateProvider) {
    'ngInject';

    $stateProvider.state('main.message', {
        template: '<div ui-view></div>',
        url: '/message',
        abstract: true
    })
    .state('main.message.list', {
        url: '/',
        templateUrl: 'message/views/message-list.html',
        controller: 'MessageListCtrl as vm',
    })    
    .state('main.message.unreadList', {
        url: '/',
        templateUrl: 'message/views/message-list.html',
        controller: 'MessageListCtrl as vm',
    })    
    .state('main.message.new', {
    	url: '/new',
        templateUrl: 'message/views/message-new.html',
        controller: 'messageNewCtrl as vm',
    })
    .state('main.message.detail', {
        url: '/:id/detail',
        templateUrl: 'message/views/message-detail.html',
        controller: 'messageDetailCtrl as vm',
    })
    .state('main.message.edit', {
        url: '/:id/edit',
        templateUrl: 'message/views/message-edit.html',
        controller: 'messageEditCtrl as vm',
    })
    .state('main.message.delete', {
        url: '/:id/delete',
        templateUrl: 'message/views/message-detail.html',
        controller: 'messageDeleteCtrl as vm'
    });    
};
