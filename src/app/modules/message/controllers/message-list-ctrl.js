/**
 * 控制器：消息一览列表
 */
'use strict';

function MessageListCtrl($controller, SessionSrv, MessageSrv, ApiSrv, $state) {
    'ngInject';

    let vm = this;

    //检索前处理
    function preSearchFn(apiParams) {
        apiParams.userId = SessionSrv.getCurrentUser().id;
    }
    // 检索后处理
    function postSearchFn(data) {
        angular.forEach(data, function(message) {
            if (message.readFlg === '0') {
                message.fontWeight = 'messageReadFlg-on';
            } else {
                message.fontWeight = '';
            }
            message.linkedit = message.links + '({id:' + message.receiverId + '})';
            let msg = MessageSrv.getMessage(message.templateCode, message.templateParam);
            message.messageContent = msg;

        });
    }

    // 扩展自list控制器基类
    let ctrlOpts = {
        modelName: 'message',
        preSearchFn: preSearchFn,
        postSearchFn: postSearchFn
    };
    angular.extend(this, $controller('BaseListCtrl', { vm: vm, ctrlOpts: ctrlOpts }));

    //msg的参数  如字符串'{"p0":"交易a","p1":"企业b"}'
    vm.getContentParam = function(paramStr) {
        return MessageSrv.getMessageParams(paramStr);
    };

    vm.searchByMessageType = function(messageType) {
        if (messageType) {
            vm.searchCondition.messageType = messageType;
            vm.search();
        } else {
            delete vm.searchCondition.messageType;
            vm.search();
        }
    };
    let _readFlgUpdate = function(apiParams) {
        ApiSrv.exec('message/updateFlg', apiParams).then(function() {
            MessageSrv.success('message.updateOk');
            $state.reload();
        });
    };

    vm.readFlgUpdate = function(row) {
        vm.messageId = [];
        //vm.urlHref = $state.href(row.links,{id:row.id});
        //$window.open(vm.urlHref,'_blank');
        if (row.readFlg === '0') {
            vm.messageId.push(row.id);
            _readFlgUpdate({ messageId: vm.messageId });
        }
    };

    vm.allReadFlgUpdate = function() {
        _readFlgUpdate({ maxSendTime: new Date() });
    };
}

module.exports = {
    name: 'MessageListCtrl',
    fn: MessageListCtrl
};
