/**
 * APP首次启动时的事件
 */
'use strict';

function OnRun($rootScope, $state, SessionSrv, CodeList, MultiselectTranslationTexts, MessageSrv, CkeditorSrv, CommonConstants, ApiSrv) {
    'ngInject';

    $rootScope.codelist = CodeList;

    // 刷新Token
    function _refreshToken() {
        ApiSrv.exec('adminUser/refreshToken').then((result) => {
            let currentUser = SessionSrv.getCurrentUser();
            if (currentUser && result.token) {
                currentUser.token = result.token;
                SessionSrv.saveCurrentUser(currentUser);
            }
        });
    }

    /**
     * 监听路由状态变化
     */
    function _watchStateChange() {
        $rootScope.$on('$stateChangeStart', function(e, toState) {
            let currentUser = SessionSrv.getCurrentUser();
            if (!toState.isAnon && toState.name !== CommonConstants.loginState) {
                if (!currentUser) {
                    e.preventDefault();
                    $state.go(CommonConstants.loginState);
                } else {
                    _refreshToken();
                }
            }
        });
    }

    // 监听路由状态变化
    _watchStateChange();

    // 多选下拉框的翻译文本
    $rootScope.multiselectTranslationTexts = MultiselectTranslationTexts;

    // Ckeditor组件初始化
    CkeditorSrv.init();
}

module.exports = OnRun;
