'use strict';

function RemainingDaysFilter(uibDateParser) {
    'ngInject';
    let parseStr = function(str) {
        str = str.substring(0, 10);
        //yyyy/MM/dd HH:mm:ss
        let date = uibDateParser.parse(str, 'yyyy-MM-dd');
        if (!date) {
            date = uibDateParser.parse(str, 'yyyy/MM/dd');
            if (!date) {
                return null;
            }
        }
        return date;
    };
    return function(endDateStr) {
        let endDate;
        if (endDateStr instanceof Date) {
            endDate = endDateStr;
        } else if (typeof endDateStr === 'string') {
            endDate = parseStr(endDateStr);
        }
        if(!endDate){
            return '--';
        }

        let nowDate = new Date();
        let milliseconds = endDate.getTime() - nowDate.getTime();
        if (milliseconds < 0) {
            return '0';
        }
        return Math.ceil(milliseconds / (24 * 60 * 60 * 1000));
    };
}

module.exports = {
    name: 'remainingDays',
    fn: RemainingDaysFilter
};
