'use strict';

let _ = require('lodash');

function ConnWordsFilter() {
	'ngInject';
	return function (wordArray,separator) {
        return _.compact(wordArray).join(separator);
    };
}

module.exports = {
    name: 'connWords',
    fn: ConnWordsFilter
};