'use strict';

function MoneyFormatFilter() {
    'ngInject';
    return function(input) {
        if (!input) {
        	return input;
        }
        let output = input.split('.')[0];

        return output;
    };
}

module.exports = {
    name: 'moneyFormat',
    fn: MoneyFormatFilter
};
