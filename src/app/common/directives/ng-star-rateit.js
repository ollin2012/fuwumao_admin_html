'use strict';


/**
 * @class NgStarRateit 星级评分组件
 * @alias module:common/directives.NgStarRateit
 * @description:参考https://github.com/akempes/angular-rateit
 * <ng-star-rate-it 
 *    ng-model = "String, Number, Array"
 *    min = "Double"
 *    max = "Double"
 *    step = "Double" [1/0.5]
 *    read-only = "Boolean"
 *    pristine = "Boolean"
 *    resetable = "Boolean"
 *    star-width = "Integer"
 *    star-height = "Integer"
 *    rated = "Function"
 *    reset = "Function"
 *    before-rated = "Function: return promise"
 *    before-reset = "Function: return promise"
 *    >
 * </ng-star-rate-it>
 * @param  {timeout} timeout timeout服务
 * @return {Directive}
 */

function NgStarRateit($q) {
    'ngInject';

    /*jslint unparam:true */
    let _link = function($scope, $element, $attrs) {

        if (!$attrs.readOnly) {
            $scope.readOnly = function() {
                return false;
            };
        }

        if (!$attrs.resetable) {
            $scope.resetable = function() {
                return true;
            };
        }

        if (!$attrs.beforeRated) {
            $scope.beforeRated = function() {
                let d = $q.defer();
                d.resolve();
                return d.promise;
            };
        }

        if (!$attrs.rated) {
            $scope.rated = function() {
                return;
            };
        }

        if (!$attrs.beforeReset) {
            $scope.beforeReset = function() {
                let d = $q.defer();
                d.resolve();
                return d.promise;
            };
        }

        if (!$attrs.reset) {
            $scope.reset = function() {
                return;
            };
        }

    };
    
    /** @ngInject */
    function ngRateItController($scope, $timeout) {

        $scope.hide = false;
        $scope.orgValue = angular.copy($scope.ngModel);

        $scope.min = $scope.min || 0;
        $scope.max = $scope.max || 5;
        $scope.step = $scope.step || 1; // 默认1 
        $scope.pristine = $scope.orgValue === $scope.ngModel;

        $scope.starWidth = $scope.starWidth || 16;
        $scope.starPartWidth = $scope.starWidth * $scope.step;
        $scope.starHeight = $scope.starHeight || 16;
        $scope.canelWidth = $scope.canelWidth || $scope.starWidth;
        $scope.cancelHeight = $scope.cancelHeight || $scope.starHeight;

        let diff = $scope.max - $scope.min, steps = diff / $scope.step, garbage = $scope.$watch('ngModel', function() {
            $scope.pristine = $scope.orgValue === $scope.ngModel;
        }),

        getValue = function(index) {
            return (index + 1) / steps * diff;
        };

        $scope.getStartParts = function() {
            return new Array(steps);
        };

        $scope.getStarOffset = function(index) {
            let ratio = 1 / $scope.step, offset = -($scope.starWidth / ratio) * (index % ratio);
            return offset;
        };

        $scope.isSelected = function(index) {
            // return getValue(index) <= $scope.ngModel - $scope.min;//原逻辑1.5 显示1星
            let selected;
            if (parseInt($scope.step) === 1) {
                selected = getValue(index) <= $scope.ngModel - $scope.min + $scope.step / 2;
            } else {
                selected = getValue(index) <= $scope.ngModel - $scope.min;
            }
            return selected;
        };

        $scope.removeRating = function() {
            if ($scope.resetable() && !$scope.readOnly()) {
                $scope.beforeReset().then(function() {
                    $scope.ngModel = $scope.min;
                    $scope.reset();
                });
            }
        };

        $scope.setValue = function(index) {
            if (!$scope.readOnly()) {
                $scope.hide = true; // Hide element due to presisting IOS :hover
                let tmpValue = angular.copy($scope.min + getValue(index));
                $scope.beforeRated(tmpValue).then(function() {
                    $scope.ngModel = tmpValue;
                    $timeout(function() {
                        $scope.rated();
                    });
                });
                $timeout(function() {
                    $scope.hide = false;
                }, 5); // Show rating s.a.p. 
            }
        };

        $scope.$on('$destroy', function() {
            garbage();
        });

    }

    let directive = {
        restrict: 'AE',
        scope: {
            ngModel: '=',
            min: '=?min',
            max: '=?max',
            step: '=?step',
            readOnly: '&?readOnly',
            pristine: '=?pristine',
            resetable: '&?resetable',
            starWidth: '=?starWidth',
            starHeight: '=?starHeight',
            canelWidth: '=?canelWidth',
            cancelHeight: '=?cancelHeight',
            rated: '=?rated',
            reset: '=?reset',
            beforeRated: '=?beforeRated',
            beforeReset: '=?beforeReset'
        },
        template: '<div class="ngrateit" ng-class="{\'ngrateit-readonly\': readOnly()}">' + '<div ng-if="!hide" id="origin" class="ngrateit-rating">' + '<span ' + 'class="ngrateit-star ngrateit-bg-star"' + 'ng-repeat="i in getStartParts() track by $index" ' + 'ng-class="{\'ngrateit-selected\': isSelected($index) }"' + 'ng-click="setValue($index)"' + 'ng-style="{\'width\': starPartWidth+\'px\', \'height\':starHeight+\'px\', \'background-position\': getStarOffset($index)+\'px 0\'}"' + '><span>' + '</div>',
        require: 'ngModel',
        replace: true,
        link: _link,
        controller: ngRateItController
    };
    return directive;


}

module.exports = {
    name: 'ngStarRateIt',
    fn: NgStarRateit
};