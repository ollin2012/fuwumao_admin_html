'use strict';


/**
 * @class UploadItem 上传项目
 * @alias module:common/directives.UploadItem
 * @return {Directive}
 */

function UploadItem() {
    'ngInject';

    // 模板
    let _template = 
        `<div class="progress upload-progress" ng-show="uploadItem.isUploading">
            <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploadItem.progress + '%' }"></div>
         </div>
         <div class="uploadItem-img-wrapper" ng-if="uploadItem.isSuccess">
            <div class="uploadItem-remove">
                <a class="fa fa-remove" href="" ng-click="uploadItem.remove()"></a>
            </div>   
            <oss-image class="uploadItem-img" img-src="uploadItem.result.url"></oss-image>
         </div>`;   

    // 链接
    let _link = function($scope, $element) {
        let width = $element[0].offsetWidth;
        $scope.width = width;
    };

    // 指令返回
    let directive = {
        restrict: 'A',
        scope: {
            uploadItem: '='
        },
        template: _template,
        link: _link
    };
    return directive;
}

module.exports = {
    name: 'uploadItem',
    fn: UploadItem
};