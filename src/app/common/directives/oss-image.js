'use strict';


/**
 * @class OssImage OSS图片
 * 根据容易的大小重写图片resizeUrl
 * @alias module:common/directives.OssImage
 * @return {Directive}
 */

function OssImage() {
    'ngInject';

    let _template = `<image class="{{clazz}}" ng-src="{{imgSrc ? imgSrc + '?x-oss-process=' + ossProcess : null}}">`;

    // 链接
    let _link = function($scope, $element, $attrs) {
        let eleWidth = $element[0].offsetWidth;
        let parentWidth = $element[0].parentElement.offsetWidth;
        let width = eleWidth > parentWidth ? eleWidth : parentWidth;
        $scope.clazz = $attrs.class;

        if (!$scope.ossProcess) {
            $scope.ossProcess = 'image/resize,w_' + width;
        }
    };

    // 指令返回
    let directive = {
        restrict: 'AE',
        scope: {
            imgSrc: '=',
            ossProcess: '@'
        },
        template: _template,
        link: _link,
        replace: true
    };
    return directive;
}

module.exports = {
    name: 'ossImage',
    fn: OssImage
};