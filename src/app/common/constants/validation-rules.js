'use strict';

/**
 * @class ValidationRules 验证规则
 * @alias module:common/constants.ValidationRules
 */
let ValidationRules;

/**
 * 验证规则
 * @type {Object}
 * @alias module:common/constants.ValidationRules.expression
 */
let _expression = {
    required: function(value) {
        return !!value;
    },
    url: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
    email: function(value) {
        if (value) {
            return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        } else {
            return true;
        }
    },
    number: function(value) {
        if (value) {
            return /^\d+$/.test(value);
        } else {
            return true;
        }
    },
    minlength: function(value, scope, element, attrs, param) {
        if (value) {
            return value.length >= param;
        } else {
            return true;
        }
    },
    maxlength: function(value, scope, element, attrs, param) {
        if (value) {
            return value.length <= param;
        } else {
            return true;
        }
    },
    alphaNumber: /^[a-zA-Z0-9]*$/,
    mobilephone: /^[1-9][0-9]{10}$/,
    percentages: function(value) {
        if (value === undefined || value === '') {
            return true;
        }
        return value >= 0 && value <= 1;
    },
    password: /(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[0-9a-zA-Z]).{6,100}/,
    passwordMatch: function(value, scope) {
        if (!value) {
            return false;
        } else {
            return value === scope.vm.model.password;
        }
    },
    depositMoney: function(value) {
        if (value) {
            return /^\d{1,13}(\.\d{1,2}?)?$/.test(value.toString());
        } else {
            return true;
        }
    },
    percent: function(value) {
        if (value) {
            return /^\d(\.\d{1,2}?)?$/.test(value.toString());
        } else {
            return true;
        }
    },
    percent1: function(value) {
        if (value) {
        	if (parseFloat(value).toFixed(2) > 1.00) {
        		return false;
        	}
            return /^\d(\.\d{1,2}?)?$/.test(value.toString());
        } else {
            return true;
        }
    },
    percent100: function(value) {
        if (value) {
            return (/^\d+$/.test(value)&&(value>=0 && value<=100));
        } else {
            return true;
        }
    }
    
};

/**
 * 默认消息
 * @type {Object}
 * @alias module:common/constants.ValidationRules.defaultMsg
 */
let _defaultMsg = {
    required: {
        error: '请输入'
    },
    url: {
        error: 'URL格式不正确',
    },
    email: {
        error: '邮箱地址的格式不正确',
    },
    number: {
        error: '请输入数字'
    },
    minlength: {
        error: '最小长度不正确'
    },
    maxlength: {
        error: '最大长度不正确'
    },
    alphaNumber: {
        error: '请输入英文或者数字'
    },
    mobilephone: {
        error: '请输入正确的电话号码'
    },
    percentages: {
        error: '请输入0和1之间的小数'
    },
    password: {
        error: '请输入长度为6位及以上的密码(必须包含英文字母和数字)'
    },
    passwordMatch: {
        error: '两次密码输入不一致'
    },
    depositMoney: {
        error: '请输入正确的格式（最大13位整数，2位小数）'
    },
    percent: {
        error: '请输入正确的比率格式（1位整数，2位小数）'
    },
    percent1: {
        error: '请输入0~1之间的数（两位小数）'
    },
    percent100: {
        error: '请输入0~100的整数'
    }
};

ValidationRules = {
    expression: _expression,
    defaultMsg: _defaultMsg
};


module.exports = {
    name: 'ValidationRules',
    fn: ValidationRules
};
