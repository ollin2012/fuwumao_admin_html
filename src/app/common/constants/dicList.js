'use strict';

/**
 * @class DicList 词典列表
 * @alias module:common/constants.DicList
 */
let DicList = {
	// 词典定义
	'COP_COMPANY':'企业',
	'SVC_SERVICE':'服务',
	'COP_DEPARTMANT_ID':'部门ID',
	'COP_COMPANY_ID':'企业ID',
	'COM_FOUNDS_SERIAL_NUMBER':'资金流水号',
	'SVC_SERVICE_VERIFY':'服务审核',
	'TRD_SERVICE_RESERVATION':'预约服务',
	'USR_EMAIL':'邮箱',
	'COP_COMPANY_NUMBER':'企业编号',
	'TRD_ORDER_INF':'订单信息',
	'TRD_VERIFY_INF':'审核信息',
	'TRD_SELLER_AGENCY':'卖方服务商',
	'TRD_BUYER_COMPANY':'买方企业',
	'TRD_SERVICE_PLATFORM':'服务平台',
	'SVC_SERVICE_INF':'服务信息'
};

module.exports = {
    name: 'DicList',
    fn: DicList
};
