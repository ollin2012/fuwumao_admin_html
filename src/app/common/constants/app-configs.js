'use strict';
/**
 * @class AppConfigs APP配置信息
 * @alias module:common/constants.AppConfigs
 * 
 * @param {String} ENV 当前环境-分为：dev, test, staging, product
 * @param {String} API_BASE_URL	API基础路径
 * @param {String} SOCKET_URL	Socket基础路径
 * @param {String} USER_TOKEN_KEY	用户Token的保存Key
 * @param {String} UPLOAD_GET_SIGN_URL  上传文件获得签名的URL
 * @param {String} FRONT_BASE_URL  前台画面基础地址
 */
let AppConfigs = {
	ENV: '${ENV}',
	API_BASE_URL: '${API.BASE}',
	SOCKET_URL: '${API.BASE}',
	USER_TOKEN_KEY: 'token',
	UPLOAD_GET_SIGN_URL: '${UPLOAD_GET_SIGN_URL}',
	DATE_FORMAT: 'YYYY-MM-DD',
	FRONT_BASE_URL: '${FRONT_BASE_URL}'
};

module.exports = {
     name: 'AppConfigs',
     fn: AppConfigs
};
