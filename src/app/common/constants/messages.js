'use strict';

/**
 * @class MessageList 消息列表
 * @alias module:common/constants.MessageList
 */
let MessageList = {
	'error.system.error': '发生系统错误',
	'error.required': '请输入{{p0}}。',
	'error.url': '请输入正确格式的网址!',
	'error.email': '请输入正确的{{p0}}。',
	'error.number': '{{p0}}必须是数字。',
	'error.minlength': '最小长度不正确。',
	'error.maxlength': '超过了最大长度。',
	'error.alphaNumber': '请输入英文或数字。',
	'error.fileUpload': '文件上传时发生错误，请重新上传。',
	'error.fileExtension': '只能上传以下扩展名的文件{{p0}}',
	'error.fileSize': '只能上传不大于{{p0}}的文件',
	'error.fileLimit': '只能上传不超过{{p0}}个文件',
	'File extension error.': '上传文件的格式错误。',
	'error.currencyNon-existent':'币种不能全部无效',
	'error.dateNon-existent':'日期不能为空',
	'confirm.update': '确定要更新吗？',
	'confirm.create': '确定要新增吗？',
	'confirm.save': '确定要保存吗？',
	'confirm.delete': '确定要删除吗？',
	'confirm.logout': '确定要退出吗？',
	'confirm.dateNoSave':'变更的数据将不会保存，确认换页吗？',
	'confirm.verify': '确定要审核吗？',
	'confirm.stop': '确定要终止吗？',
	'confirm.shelve': '确定要下架吗？',
	'confirm.adopt': '确定要通过吗？',
	'message.updateOk': '更新成功',
	'message.saveOk': '保存成功',
	'message.createOk': '新增成功',
	'message.deleteOk': '删除成功',
	'message.loanOk': '放款成功',
	'message.verifyOk': '审核操作成功',
	'message.stopOk': '终止操作成功',
	'message.shelveOk': '下架操作成功',
	'message.freezeOk': '冻结/解冻操作成功',
	'message.relateOk': '关联成员企业操作成功',
	'message.noUpdate': '没有进行变更，无需提交',
	'message.compareDateErr': '{{p0}}必须大于{{p1}}。',

	// MSG_COM
	'ERR_USR_LOGIN_ERROR': '您的邮箱或密码有误，请重新输入。',
	'ERR_COM_LOGIN_FAILED': '用户身份验证失败。',
	'INF_COM_LOGIN_SUCCESS': '用户登入成功。',
	'ERR_COM_NO_AUTH': '没有权限',
	'ERR_COM_GET_FAILED': '{{p0}}取得失败。',
	'INF_COM_NO_SEARCH_RESULT': '没有检索到数据。',
	'ERR_TRD_NOT_ONGOING': '交易不在进行中。',
	'ERR_COM_INVALID_PERMISSION': '无权限访问',
	'ERR_COM_INVALID_TOKEN': '请重新登录',
	'ERR_COM_CONCURRENT_EXCEPTION': '处理失败，请刷新页面后重试。',
	'ERR_COM_OSS_CONVERT_EXCEPTION': '发生系统错误。',
	'ERR_COM_INTERNAL_SERVER_ERROR': '系统错误，请稍后再试。',
	// MSG_API
	'ERR_USR_LOGIN_FAILED': '用户身份验证失败。',
	'INF_USR_LOGIN_SUCCESS': '用户登入成功。',
	'ERR_USR_WITHOUT_COMPANY': '操作用户不属于该企业。',
	'ERR_COM_IS_REQUIRED': '{{p0}}是必须项。',
	'INF_COP_FREEZE_SUCCESS': '企业冻结成功。',
	'INF_COP_UNFREEZE_SUCCESS': '企业解冻成功。',
	'INF_COP_VERIFY_SUCCESS': '企业审核通过。',
	'INF_COP_VERIFY_FAILED': '企业审核未通过。',
	'INF_AGY_VERIFY_SUCCESS': '服务商审核已通过。',
	'INF_AGY_VERIFY_FAILED': '服务商审核未通过。',
	'ERR_COM_LOGIN_ERROR': '您的用户名或密码有误，请重新输入!',
	'ERR_COM_NUMBER_GEN': '{{p0}}採番失败。',
	'ERR_USR_LINK_EXPIRATION': '申请链接过期了，请重新注册。',
	'ERR_COM_INSERT_ERROR': '{{p0}}新增失败。',
	'ERR_USR_NOT_SELF': '不能编辑其他用户的个人信息。',
	'ERR_COM_IS_NUMBER': '{{p0}}必须是整数。',
	'ERR_COM_IS_LENGTH_MAX': '{{p0}}长度不能大于{{p1}}。',
	'ERR_COM_REPEAT_EDIT': '{{p0}}重复编辑。',
	'ERR_COM_DELETE_ERROR': '{{p0}}删除失败。',
	'ERR_USR_ADD_FAILED': '该用户已经是交易相关人员，添加失败。',
	'ERR_TRD_REPEAT_COMPLAINT': '该交易已经有投诉，添加失败。',
	'ERR_COP_NOT_EXISTS': '该企业不存在。',
	'ERR_COP_HAS_INVITED': '该企业已经被其他企业邀请过了。',
	'INF_TRD_EVALUATION': '交易（{{p0}}）：{{p1}}对{{p2}}做出评价。',
	'INF_TRD_GIVEN_EVALUATION': '交易（{{p0}}）：{{p1}}已给出评价。',
	'INF_TRD_EVALUATION_REPLY': '交易（{{p0}}）：{{p1}}对{{p2}}做出评价回复。',
	'INF_TRD_GIVEN_EVALUATION_REPLY': '交易（{{p0}}）：{{p1}}已给出评价回复。',
	'INF_TRD_COMPLAINT': '交易（{{p0}}）：{{p1}}对{{p2}}发起投诉。',
	'INF_TRD_GIVEN_COMPLAINT': '交易（{{p0}}）：{{p1}}发起投诉。',
	'INF_TRD_COMPLAINT_FINISH': '交易（{{p0}}）：{{p1}}的投诉已处理，同意处理结果。',
	'INF_TRD_COMPLAINT_REVOKE': '交易（{{p0}}）：{{p1}}的投诉已撤销。',
	'INF_TRD_COMPLAINT_REPLY': '交易（{{p0}}）：{{p1}}的投诉已回复。',
	'ERR_COM_PASSWORD_ERROR': '请输入6位及以上的密码(包含英文字母和数字) ',
	'INF_COP_FREEZE_FAILED': '企业状态不正常，企业冻结失败。',
	'INF_COP_UNFREEZE_FAILED': '企业状态不是冻结状态，企业解冻失败。',
	'INF_COP_AGENCY_VERIFYING': '企业已提交服务商审核申请，请不要重复提交。',
	'INF_AGY_DATA_VERIFY_SUCCESS': '服务商的资料审核已通过。',
	'INF_AGY_DATA_VERIFY_FAILED': '服务商的资料审核未通过。',
	'INF_AGY_DEPOSIT_VERIFY_SUCCESS': '服务商的入驻保证金审核已通过。',
	'INF_AGY_DEPOSIT_PAY': '服务商的入驻保证金已支付。',
	'INF_COP_ACCOUNT_ABNORMAL': '企业资金账户不正常。',
	'ERR_USR_OLD_PASSWORD_ERROR': '密码不正确。',
	'ERR_USR_NUMBER_EXIST': '工号已存在。',
	'ERR_USR_MAILBOX_EXIST': '邮箱已存在。',
	'ERR_USR_ORGANIZATION_NAME_EXIST': '组织名称已存在。',
	'ERR_USR_ROLE_NAME_EXIST': '角色名称已存在。',
	'ERR_SVC_VERIFY_RESULT_ABNORMAL': '服务审核结果异常。',
	'INF_COP_ANNUAL_FEE_SUCCESS': '年费认证成功。',
	'ERR_COP_PREFERRED_LANGUAGE_ERROR': '企业默认语言必须属于您发布的语言版本。',
	// MSG_BackEND
	'ERR_COM_GET_LIST_FAILED': '{{p0}}一览取得失败。',
	'ERR_COM_GET_DETAIL_FAILED': '{{p0}}详情取得失败。',
	'INF_SVC_SUBMIT_EXAMINE': '服务（{{p0}}）：服务商已提交服务审核申请，请及时审核。',
	'INF_TRD_STAGE_ACCEPT_COMPLETE': '交易（{{p0}}）：阶段验收完成，请及时放款。',
	'INF_TRD_VERIFY_SUCCESS': '交易（{{p0}}）：交易定制内容由{{p1}}审核通过。',
	'INF_TRD_VERIFY_FAILED': '交易（{{p0}}）：交易定制内容由{{p1}}审核失败。',
	'INF_TRD_PAY_ADVANCE_PAYMENT': '交易（{{p0}}）：请及时支付预付款。',
	'ERR_CON_CONTENT_TYPE_DUPLICATE': '此内容类型已存在。',
	'ERR_CON_CONTENT_TYPE_IS_NOT_EXIST': '此内容类型不存在。',
	'INF_TRD_BUYER_PAYED_STAGE_PRICE': '交易（{{p0}}）：买方已支付（{{p1}}阶段）的预付款，金额为：{{p2}}{{p3}}',
	'INF_TRD_PLATFORM_PAYED_STAGE_PRICE': '交易（{{p0}}）：您已收到（{{p1}}）阶段的交易款，扣除手续费（{{p2}}{{p3}}）后，实际收到金额为：{{p4}}{{p3}}',
	// MSG_Front
	'INF_USR_MAIL_PW_SEND_SUCCESS': '找回密码验证邮件已发送到您的邮箱：{{p0}}。请点击邮件中的链接，重新设置登录密码。',
	'ERR_USR_MAIL_PW_SEND_FAILED': '找回密码验证邮件发送失败，请重新发送',
	'ERR_USR_PW_CERT_FAILED': '找回密码认证失败，请重新认证。',
	'INF_USR_PW_CERT_SUCCESS': '密码重置成功。',
	'INF_USR_MAIL_CERT_SEND_SUCCESS': '注册激活邮件已发送到您的邮箱：{{p0}}。请点击邮件里的链接，激活邮箱。',
	'ERR_USR_MAIL_CERT_SEND_FAILED': '注册激活邮件发送失败，请重新发送。',
	'ERR_USR_MAIL_CERT_FAILED': '邮箱注册认证失败，请重新认证。',
	'INF_USR_MAIL_CERT_SUCCESS': '邮箱注册认证成功。',
	'ERR_USR_INTERVAL_TIME': '认证请求频繁，请稍后再试。',
	'INF_USR_PW_CHANGE_SUCCESS': '密码修改成功。',
	'ERR_COP_LOGIN_FAILED': '用户身份验证失败。',
	'INF_COP_LOGIN_SUCCESS': '用户登入成功。',
	'ERR_COP_REGISTER_FAILED': '企业注册失败。',
	'INF_COP_DEP_ADD_FAILED': '企业部门添加失败。',
	'INF_COP_DEP_UPDATE_FAILED': '企业部门修改失败。',
	'INF_COP_DEP_DELETE_FAILED': '企业部门删除失败。',
	'INF_COP_DEP_USERSET_FAILED': '企业部门人员设定失败。',
	'INF_COP_ROLE_ADD_FAILED': '企业角色添加失败。',
	'INF_COP_ROLE_UPDATE_FAILED': '企业角色修改失败。',
	'INF_COP_ROLE_DELETE_FAILED': '企业角色删除失败。',
	'INF_COP_USR_ADD_FAILED': '企业员工添加失败。',
	'INF_COP_USR_UPDATE_FAILED': '企业员工修改失败。',
	'INF_COP_USR_DELETE_FAILED': '企业员工删除失败。',
	'INF_COP_INVITE_FAILED': '企业邀请失败。',
	'ERR_COP_BANK_ACCOUNT_REPEAT_ERROR': '您的银行账号有重复，请重新输入。',
	'ERR_COP_STATUS_ABNORMAL': '企业状态异常。',
	'INF_CON_NEWS_UPDATE_FAILED': '新闻动态编辑失败。',
	'INF_CON_NEWS_DELETE_FAILED': '新闻动态删除失败。',
	'INF_CON_NEWS_ADD_FAILED': '新闻动态添加失败。',
	'INF_TRD_CREATE_NEW_ORDER': '订单({{p0}})已经预约。',
	'INF_TRD_ORDER_BOOKED': '您的订单({{p0}})已经预约成功。',
	'INF_TRD_NEW_ORDER': '您有新的订单({{p0}})，请注意接收。',
	'INF_TRD_ACCEPTED_ORDER': '服务商已接受订单({{p0}})。',
	'INF_TRD_ACCEPTED_ORDER_AGENCY': '{{p0}}已接受订单({{p1}})。',
	'INF_TRD_ACCEPTED_ORDER_BUYER': '您的订单已被受理，交易进入交易定制阶段，请及时联系服务商。',
	'INF_TRD_CUSTOM_FINISH': '交易（{{p0}}）：交易定制内容已完成，请及时确认。',
	'INF_TRD_WAIT_FOR_PLATFORM_VERIFY': '交易（{{p0}}）：交易定制内容买卖双方互审通过，请及时平台审核。',
	'INF_TRD_CUSTOMIZED_VERRYING': '交易（{{p0}}）：交易定制内容由{{p1}}内部审核中。',
	'INF_TRD_CUSTOMIZED_CONFIRM_FAILED': '交易（{{p0}}）：交易定制内容买方确认不通过，请重新定制交易。',
	'INF_TRD_TRADE_CANCEL': '交易（{{p0}}）：由{{p1}}取消了交易。',
	'ERR_TRD_STATUS_ABNORMAL': '交易状态异常。',
	'INF_TRD_STAGE_ACCEPT': '交易（{{p0}}）：{{p1}}阶段验收完成。',
	'INF_TRD_TRADE_TERMINATION': '交易（{{p0}}）：由{{p1}}终止了交易。',
	'INF_TRD_CUSTOMIZED_VERRY_APPLY_PLATFORM': '交易（{{p0}}）：交易定制内容向平台提出审核申请。',
	'INF_TRD_TRADE_COMPLETED': '交易（{{p0}}）：交易完成。',
	'INF_TRD_STAGE_TRANSACTION_COMPLETED': '交易（{{p0}}）：交易阶段实施完成。',
	'ERR_COM_VERIFYCODE_ERROR': '您的验证码有误，请重新输入。',
	'ERR_AGY_STATUS_ABNORMAL': '服务商状态异常。',
	'INF_SVC_VERIFY_SUCCESS': '您的服务审核通过了。',
	'INF_SVC_VERIFY_FAILED': '对不起，您的服务审核未通过。',
	'INF_SVC_CREATE_SUCCESS': '服务（{{p0}}）：成功创建了一个新的服务。',
	'INF_SVC_DELETE_SUCCESS': '服务（{{p0}}）：成功删除服务。',
	'INF_SVC_VERRY_APPLY': '服务（{{p0}}）：服务已提交审核。',
	'ERR_SVC_CAN_NOT_EDITED': '当前服务不能被编辑。',
	'ERR_SVC_STATUS_ABNORMAL': '服务状态异常。',
	'ERR_USR_MAIL_CERT_REPEAT_ERROR': '您注册用邮箱已被使用，请使用其他邮箱注册。',
	'ERR_USR_VERIFYCODE_ERROR': '验证码错误',
	'ERR_AGY_CERT_THREE_CREDENTIALS_REQUIRED': '请上传三证扫描件。',
	'ERR_AGY_CERT_ID_REQUIRED': '请上传法人身份证扫描件。',
	'ERR_USR_HAS_BEEN_ACTIVATED': '该用户已被激活。',
	'ERR_SVC_ORDER_BOOKED_FAILED': '您的服务不能被预约。',
	'INF_SVC_OFF_SHELF_ADMIN': '由于您的服务（{{p0}}）有违规之处，被管理员下架。违规内容：{{p1}}。'


};

module.exports = {
    name: 'MessageList',
    fn: MessageList
};
