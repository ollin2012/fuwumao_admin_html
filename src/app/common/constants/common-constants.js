'use strict';
/**
 * @class CommonConstant 共同定数
 * @alias module:common/constants.CommonConstant
 */
let CommonConstants = {
    /** @property {Object} loginState 用户登录的state */
    loginState: 'login',
    /** @property {Object} companyResult 企业认证结果描述 */
    'companyResult': {
        '1': '1元认证未到账',
        '2': '账号实名与企业名不符',
    },
    /** @property {Object} companyResultDetail 企业认证结果描述详细 */
    'companyResultDetail': {
        '1': '1元认证未到服务猫账户，无法确认。',
        '2': '账号实名与企业名不符，请确认。',
    },
    /** @property {Object} agencyResult 服务商认证结果描述 */
    'agencyResult': {
        '1': '材料不全',
        '2': '图片不清',
        '3': '内容不符',
        '4': '内容不实',
    },
    /** @property {Object} agencyResultDetail 服务商认证结果描述详细 */
    'agencyResultDetail': {
        '1': '提供文件材料的内容与公司业务描述不符。',
        '2': '上传的图片不清晰，无法确认。',
        '3': '附件内容与企业信息不符，请确认。',
        '4': '附件内容夸大其词，无真实性。',
    },
    /** @property {Object} serviceResult 服务认证结果描述 */
    'serviceResult': {
        '1': '材料不全',
        '2': '图片不清',
        '3': '内容不符',
        '4': '内容不实',
    },
    /** @property {Object} serviceResultDetail 服务认证结果描述详细 */
    'serviceResultDetail': {
        '1': '提供材料不全，无法确认。',
        '2': '服务介绍图片不清晰，无法确认。',
        '3': '服务介绍内容不符，无法确认。',
        '4': '服务介绍内容不实，无法确认。',
    },
    /** @property {Object} tradeResult 交易认证结果描述 */
    'tradeResult': {
        '1': '材料不全',
        '2': '图片不清',
        '3': '内容不符',
        '4': '内容不实',
        '5': '价格跳水',
    },
    /** @property {Object} tradeResultDetail 交易认证结果描述详细 */
    'tradeResultDetail': {
        '1': '提供材料不全，无法确认。',
        '2': '交易图片不清晰，无法确认。',
        '3': '交易内容不符，无法确认。',
        '4': '交易内容不实，无法确认。',
        '5': '交易价格跳水。',
    },
};

module.exports = {
    name: 'CommonConstants',
    fn: CommonConstants
};
