'use strict';

/**
 * @class MultiselectTranslationTexts 多项选择组件的翻译文本
 * @alias module:common/constants.MultiselectTranslationTexts
 */
let MultiselectTranslationTexts = {
	checkAll: '全部选择',
	uncheckAll: '全部不选',
	selectionCount: '已选',
	buttonDefaultText: '选择',
	dynamicButtonTextSuffix: '已选'
};

module.exports = {
     name: 'MultiselectTranslationTexts',
     fn: MultiselectTranslationTexts
};
