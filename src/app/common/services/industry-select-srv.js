'use strict';

let _ = require('lodash');
const INDUSTRY_API_URL = 'industry/getList';

/**
 * @class IndustrySelectSrv 行业选择框组件相关服务
 * @alias module:common/services.IndustrySelectSrv
 * 
 */

function IndustrySelectSrv($q, ApiSrv, CodeList) {
    'ngInject';

    /**
     * @method module:common/services.AreaSrv#getAllAreaList 
     * @description 取得所有地区列表，并且按阶层分组
     * @memberOf AreaSrv
     * @param {String} [status]   地区选择状态：默认为'2'-前台表示
     * @return Promise
     */
    let _getAllIndustryList = function(status) {
        let d = $q.defer();

        // 处理行业列表返回
        function _procGetIndustryList(result) {
            let listAll = result.list;

            listAll = _.map(listAll, (industry) =>{
                industry.key = 'industry.' + industry.id;
                industry.name = CodeList.industry[industry.id];
                return industry;
            });
            
            let groupedList = _.groupBy(listAll, 'level');

            // 设置2级地区
            let industry2List = groupedList[2];
            
            // 设置1级地区，将2级地区写入1级的child中
            let industry1List = groupedList[1];
            industry1List = _.map(industry1List, (industry1) => {
                industry1.childs = _.filter(industry2List, {'parentId': industry1.id});
                return industry1;
            });

            return d.resolve(industry1List);
        }

        // 取得行业列表  
        if ((typeof(status) === 'undefined')) {
            status = '0';
        } else {
            status = status;
        }
        ApiSrv.exec(INDUSTRY_API_URL, {
            status: status
        }).then(_procGetIndustryList); 

        return d.promise;       
    }; 

    /**
     * @private
     * @method module:common/services.IndustrySrv#setIndustry2List 
     * @description 取得二级行业下来框选择内容
     * @param {String} parentId     上级行业ID（vm中的行业一选择对象ID）
     * @param {Object} vm   控制器的value model
     * @return void
     */
    let _setIndustry2List = function(_parentId, vm) {
        // 取得二级级业务覆盖行业 
        vm.industryList2 = _.find(vm.industryList1, {'id': _parentId}).childs;
    };

    /**
     * @public
     * @method module:common/services.IndustrySrv#initIndustryList
     * @description 初期化一级，二级行业下来框选择内容
     * @example
     *    控制层： 
     *       // 行业一,二下拉框初期化 
     *       // 'searchCondition.industry1' 为行业一绑定的key 注意去掉vm
     *       // 'searchCondition.industry2' 为行业一绑定的key 注意去掉vm
     *       IndustrySrv.initIndustryList(vm, 'searchCondition.industry1', 'searchCondition.industry2');
     *    显示层： 
     *        <!--行业一-->
     *        <!--ng-model 绑定可以是vm里的任意值， ng-options,ng-change,ng-click 写法固定-->
     *        <select name="industry1" class="form-control"
     *                 ng-model="vm.searchCondition.industry1" 
     *                 ng-options="'' + industry.id as industry.name  for industry in vm.industryList1">
     *             <option value=""></option>
     *         </select>
     *         <!--行业二-->
     *         <select name="industry2" class="form-control"
     *                 ng-model="vm.searchCondition.industry2" 
     *                 ng-options="'' + industry.id as industry.name  for industry in vm.industryList2">
     *             <option value=""></option>
     *         </select>
     * @param {Object} vm   控制器的value model
     * @param {String} industry1Key     一级行业选择项绑定的vm对象的key(例子'vm.searchCondition.industry1' 
     *                              入力'searchCondition.industry1')
     * @param {String} industry2Key     二级级行业选择项绑定的vm对象的key(例子'vm.searchCondition.industry2' 
     *                              入力'searchCondition.industry2')
     * @return void
     */
    let _initIndustryList = function($scope, vm, industry1Key, industry2Key) {
        if (!vm) {
            vm = {};
        }
        // 行业list 初期化
        vm.industryList1 = [];
        vm.industryList2 = [];

        // 取得所有地区
        _getAllIndustryList(null).then(function(result) {
            // 取得一级行业下来框选择内容
            vm.industryList1 = result;

            // 一级行业选择项不为空时
            let industry1 = _.at(vm, industry1Key)[0];
            if (industry1) {
                // 取得二级行业下来框选择内容
                _setIndustry2List(industry1, vm);
            }

            // 一级行业选项变更时
            let changeIndustry1 = function() {
                // 行业一选择项为空白时
                let _parentId = _.at(vm, industry1Key)[0];
                if (_.isEmpty(_parentId)) {
                    // 行业二下拉内容清空
                    vm.industryList2 = [];
                } else {
                    // 取得二级行业下来框选择内容
                    _setIndustry2List(_parentId, vm);
                }
                // 行业二 选择项清空
                _.set(vm, industry2Key, '');
            };
            let industry1ValueChange = $scope.$watch(function() {
                return _.at(vm, industry1Key)[0];
            }, function(oldVal, newVal) {
                if (newVal !== oldVal) {
                    changeIndustry1();
                }
            });

            $scope.$on('$destroy', function() {
                industry1ValueChange();
            });         
        });
    };

    return {
        initIndustryList: _initIndustryList,
        getAllIndustryList: _getAllIndustryList
    };
}

module.exports = {
    name: 'IndustrySelectSrv',
    fn: IndustrySelectSrv
};