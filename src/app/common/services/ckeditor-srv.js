'use strict';

/**
 * @class CkeditorSrv Ckeditor相关服务
 * @alias module:common/services.CkeditorSrv
 * 
 */
function CkeditorSrv($rootScope) {
    'ngInject';

    // 初始化
    let init = function() {
		CKEDITOR.on('dialogDefinition', function(ev) {
	        // Take the dialog name and its definition from the event
	        // data.
	        var dialogName = ev.data.name;
	        var dialogDefinition = ev.data.definition;

	        // Check if the definition is from the dialog we're
	        // interested on (the "Link" dialog).
	        if (dialogName === 'image') {

	            // Get a reference to the "Link Info" tab and the "Browse" button.
	            var infoTab = dialogDefinition.getContents('info');
	            infoTab.add({
	                type: 'html',
	                id: 'upload_image',
	                align: 'center',
	                label: 'upload',
	                html: '<input type="file">',
	                onChange: function(evt) {
	                    let ele = evt.sender.$;
	                    let files = ele.files;
	                    $rootScope.$broadcast('onCkFileAdded', files);

	                }
	            }, 'browse'); //place front of the browser button
	        }
	    }); 
    };

    return {
        init
    };
}

module.exports = {
    name: 'CkeditorSrv',
    fn: CkeditorSrv
};