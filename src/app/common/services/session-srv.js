'use strict';

/**
 * @class SessionSrv Session相关服务
 * @alias module:common/services.SessionSrv
 * 
 * @param {localStorageService} localStorageService
 */
function SessionSrv(localStorageService) {
    'ngInject';

    let KEY_CURRENT_USER = 'ADMIN_CURRENT_USER';

    let _currentUser;

    /**
     * @public
     * @method module:common/services.SessionSrv#saveCurrentUser 
     * @description 保存当前用户
     * @param {Object} user 当前用户
     */
    let _saveCurrentUser = function(user) {
        if (user) {
            _currentUser = user;
            localStorageService.set(KEY_CURRENT_USER, user);
        }
    };

    /**
     * @public
     * @method module:common/services.SessionSrv#clearCurrentUser 
     * @description 清除当前用户
     */
    let _clearCurrentUser = function() {
        localStorageService.remove(KEY_CURRENT_USER);
        _currentUser = null;
    };

    /**
     * @public
     * @method module:common/services.SessionSrv#getCurrentUser 
     * @description 取得当前用户
     * @return {Object} 当前用户
     */
    let _getCurrentUser = function() {
        if (!_currentUser) {
            _currentUser = localStorageService.get(KEY_CURRENT_USER);
        }
        return _currentUser;
    };

    return {
        saveCurrentUser: _saveCurrentUser,
        clearCurrentUser: _clearCurrentUser,
        getCurrentUser: _getCurrentUser
    };
}

module.exports = {
    name: 'SessionSrv',
    fn: SessionSrv
};
