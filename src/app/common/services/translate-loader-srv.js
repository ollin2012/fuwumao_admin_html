'use strict';

let _ = require('lodash');

/**
 * @class TranslateLoader 多语言转换内容Loader
 * 用于Translate customize loader, 读取消息 
 * @alias module:common/services.TranslateLoader
 * 
 * @param $rootScope  
 * @param $q        
 * @param {object} MessageList 消息一览
 */
function TranslateLoader($rootScope, $q, MessageList, DicList) {
    'ngInject';


    function _loader() {
        let d = $q.defer();

        d.resolve(_.merge({}, MessageList, DicList));

        return d.promise;
    }

    return _loader;
}

module.exports = {
    name: 'TranslateLoader',
    fn: TranslateLoader
};
