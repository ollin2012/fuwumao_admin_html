'use strict';

/**
 * @class TradeSrv 交易相关组件
 * @alias module:common/services.TradeSrv
 *
 */
function TradeSrv() {
    'ngInject';

    // 取得当前交易阶段
    function getCurrentStage(trade) {
        let currentStage;
        let stageList = trade.tradeStageInfo;
        if (stageList) {
            stageList.forEach((stage) => {
                if (stage.stageStatus === '10' ||
                        stage.stageStatus === '20' ||
                        stage.stageStatus === '30') {
                    currentStage = stage;
                }
            });
        }
        return currentStage;
    }

    return {
        getCurrentStage
    };
}

module.exports = {
    name: 'TradeSrv',
    fn: TradeSrv
};
