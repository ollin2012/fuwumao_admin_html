'use strict';

let _ = require('lodash');
const areaApiUrl = 'area/getList';

/**
 * @class AreaSelectSrv 地区选择框组件相关服务
 * @alias module:common/services.AreaSelectSrv
 * 
 */

function AreaSelectSrv($q, $translate, ApiSrv, CodeList) {
    'ngInject';


    /**
     * @method module:common/services.AreaSrv#getAllAreaList 
     * @description 取得所有地区列表，并且按阶层分组
     * @memberOf AreaSrv
     * @param {String} [status]   地区选择状态：默认为'2'-前台表示
     * @return Promise
     */
    let _getAllAreaList = function(status) {
        let d = $q.defer();

        // 处理地区列表返回
        function _procGetAreaList(result) {
            let listAll = result.list;

            listAll = _.map(listAll, (area) =>{
                area.key = 'area.' + area.id;
                area.name = CodeList.area[area.id];
                return area;
            });

            let groupedList = _.groupBy(listAll, 'level');

            // 设置2级地区，将3级地区写入2级的child中
            let area2List = groupedList[2];
            area2List = _.map(area2List, (area2) => {
                area2.childs = _.filter(groupedList[3], {'parentId': area2.id});
                return area2;
            });
            
            // 设置1级地区，将2级地区写入1级的child中
            let area1List = groupedList[1];
            area1List = _.map(area1List, (area1) => {
                area1.childs = _.filter(area2List, {'parentId': area1.id});
                return area1;
            });

            return d.resolve(area1List);
        }

        // 取得地区列表
        if ((typeof(status) === 'undefined')) {
            status = '0';
        } else {
            status = status;
        }
        
        ApiSrv.exec(areaApiUrl, {
            status: status
        }).then(_procGetAreaList); 

        return d.promise;       
    };

    /**
     * @method module:common/services.AreaSrv#setArea2List 
     * @description 取得二级地区下来框选择内容
     * @memberOf AreaSrv
     * @param {String} parentId     上级地区ID（vm中的地区二选择对象ID）
     * @param {Object} vm   控制器的value model
     * @return void
     */
    let _setArea2List = function(_parentId, vm) {
        // 取得二级级业务覆盖地区
        vm.areaList2 = _.find(vm.areaList1, {'id': _parentId}).childs;
    };

    /**
     * @method module:common/services.AreaSrv#setArea3List 
     * @description 取得二级地区下来框选择内容
     * @memberOf AreaSrv
     * @param {String} parentId     上级地区ID（vm中的地区三选择对象ID）
     * @param {Object} vm   控制器的value model
     * @return void
     */
    let _setArea3List = function(_parentId, vm) {
        // 取得二级级业务覆盖地区
        vm.areaList3 = _.find(vm.areaList2, {'id': _parentId}).childs;
    };

    /**
     * @public
     * @method module:common/services.AreaSrv#initAreaList
     * @description 初期化一级，二级，三级地区下来框选择内容
     * @example
     *    控制层： 
     *       // 地区一,二下拉框初期化 
     *       // 'searchCondition.industry1' 为地区一绑定的key 注意去掉vm
     *       // 'searchCondition.industry2' 为地区一绑定的key 注意去掉vm
     *       IndustrySelectSrv.initIndustryList(vm, 'searchCondition.industry1', 'searchCondition.industry2');
     *    显示层： 
     *        <!--地区一-->
     *        <!--ng-model 绑定可以是vm里的任意值， ng-options,ng-change,ng-click 写法固定-->
     *        <select name="companyArea1" class="form-control"
     *                 ng-model="vm.searchCondition.companyArea1" 
     *                 ng-options="'' + area.id as area.name  for area in vm.areaList1">
     *             <option value=""></option>
     *         </select>
     *         <!--地区二-->
     *         <select name="companyArea2" class="form-control"
     *                 ng-model="vm.searchCondition.companyArea2" 
     *                 ng-options="'' + area.id as area.name  for area in vm.areaList2">
     *             <option value=""></option>
     *         </select>
     *         <!--地区三-->
     *         <select name="companyArea3" class="form-control"
     *                 ng-model="vm.searchCondition.companyArea3" 
     *                 ng-options="'' + area.id as area.name  for area in vm.areaList3">
     *             <option value=""></option>
     *         </select>
     * @param {Object} vm   控制器的value model
     * @param {String} area1Key     一级地区选择项绑定的vm对象的key(例子'vm.searchCondition.area1' 
     *                              入力'searchCondition.area1')
     * @param {String} area2Key     二级级地区选择项绑定的vm对象的key(例子'vm.searchCondition.area2' 
     *                              入力'searchCondition.area2')
     * @param {String} area3Key     三级级地区选择项绑定的vm对象的key(例子'vm.searchCondition.area3' 
     *                              入力'searchCondition.area3')
     * @return void
     */
    let _initAreaList = function($scope, vm, area1Key, area2Key, area3Key) {
        if (!vm) {
            vm = {};
        }
        // 地区list 初期化
        vm.areaList1 = [];
        vm.areaList2 = [];
        vm.areaList3 = [];


        // 取得所有地区
        _getAllAreaList(null).then(function(result) {
            // 取得一级地区下来框选择内容
            vm.areaList1 = result;

             // 一级地区选择项不为空时
            let area1 = _.at(vm, area1Key)[0];
            if (area1) {
                // 取得二级地区下来框选择内容
                _setArea2List(area1, vm);
            }
            // 二级地区选择项不为空时
            let area2 = _.at(vm, area2Key)[0];
            if (area2) {
                // 取得二级地区下来框选择内容
                _setArea3List(area2, vm);
            }

            // 一级地区选项变更时
            let changeArea1 = function() {
                // 地区一选择项为空白时
                let _parentId = _.at(vm, area1Key)[0];
                if (_.isEmpty(_parentId)) {
                    // 地区二下拉内容清空
                    vm.areaList2 = [];
                    // 地区三下拉内容清空
                    vm.areaList3 = [];
                } else {
                    // 取得二级地区下来框选择内容
                    _setArea2List(_parentId, vm);
                    // 地区三下拉内容清空
                    vm.areaList3 = [];
                }
                // 地区二 选择项清空
                _.set(vm, area2Key, '');
                // 地区三 选择项清空
                _.set(vm, area3Key, '');
            };
            let area1ValueChange = $scope.$watch(function() {
                return _.at(vm, area1Key)[0];
            }, function(oldVal, newVal) {
                if (newVal !== oldVal) {
                    changeArea1();
                }
            });

            // 二级地区选项变更时
            let changeArea2 = function() {
                // 地区一选择项为空白时
                let _parentId = _.at(vm, area2Key)[0];
                if (_.isEmpty(_parentId)) {
                    // 地区三下拉内容清空
                    vm.areaList3 = [];
                } else {
                    // 取得三级地区下来框选择内容
                    _setArea3List(_parentId, vm);
                }
                // 地区三 选择项清空
                _.set(vm, area3Key, '');
            };
            let area2ValueChange = $scope.$watch(function() {
                return _.at(vm, area2Key)[0];
            }, function(oldVal, newVal) {
                if (newVal !== oldVal) {
                    changeArea2();
                }
            });

            $scope.$on('$destroy', function() {
                area1ValueChange();
                area2ValueChange();
            });            
        });   
    };

    return {
        initAreaList: _initAreaList,
        getAllAreaList: _getAllAreaList
    };
}

module.exports = {
    name: 'AreaSelectSrv',
    fn: AreaSelectSrv
};