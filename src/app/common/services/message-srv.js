'use strict';
/**
 * @class MessageSrv 消息服务
 * @alias module:common/services.MessageSrv
 *
 * @param $q
 * @param $uibModal
 * @param growl
 * @param MessageList
 */
function MessageSrv($q, $uibModal, $translate, growl, MessageList) {
    'ngInject';

    /**
     * @public
     * @method module:common/services.MessageSrv#confirm 弹出确认消息
     * @param  {String} msg 消息文本
     * @return {Promise}
     */
    let _confirm = function(msg) {
        let d = $q.defer();

        let modalInstance = $uibModal.open({
            template: `<div class="modal-header">
                           <div class="modal-title"><strong>确认</strong> </div>
                        </div>
                        <div class="modal-body" translate="{{message}}">
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" ng-click="$close()">确认</button>
                            <button class="btn btn-default" ng-click="$dismiss()">取消</button>
                        </div>`,
            controller: function($scope, message) {
                $scope.message = message;
            },
            resolve: {
                message: function() {
                    return msg;
                }
            }
        });

        modalInstance.result.then(function() {
            return d.resolve();
        }, function() {
            return d.notify();
        });

        return d.promise;
    };

    /**
     * @private
     * @method module:common/services.MessageSrv#buildMessageParams 整理置换文字列
     * @param  [String] params 消息文本
     * @return messageParams
     */
	let _buildMessageParams = function(params) {
		if (params) {
			var messageParams = {};
			var paramsArr = Array.isArray(params) ? params : [params];
        	for (var i = 0; i < paramsArr.length; i++) {
        		messageParams['p' + i] = $translate.instant(paramsArr[i]);
        	}
        	return messageParams;
		} else {
			return null;
		}
	};

    /**
     * @public
     * @method module:common/services.MessageSrv#error 追加错误消息
     * @param  {String} message 消息文本 【例如：['xxxxx', 'XXXXX']】
     */
    let _error = function(message, params) {
		if (params) {
			let messageParams = _buildMessageParams(params);
        	$translate(message, messageParams).then(function(errorMessage) {
        		growl.error(errorMessage);
        	});
		} else {
			growl.error(message);
		}
    };

    /**
     * @public
     * @method module:common/services.MessageSrv#info 追加提示消息
     * @param  {String} message 消息文本 【例如：['xxxxx', 'XXXXX']】
     */
    let _info = function(message, params) {
		if (params) {
			let messageParams = _buildMessageParams(params);
        	$translate(message, messageParams).then(function(infoMessage) {
        		growl.info(infoMessage);
        	});
		} else {
			growl.info(message);
		}
    };

    /**
     * @public
     * @method module:common/services.MessageSrv#success 追加成功消息
     * @param  {String} message 消息文本
     */
    let _success = function(message) {
        growl.success(message);
    };

    /**
     * @public
     * @method module:common/services.MessageSrv#getMessage 取得系统消息
     * @param  {String} message 消息文本 【例如：['xxxxx', 'XXXXX']】
     */
    let _getMessage = function(message, params) {
		let messageContent = MessageList[message];
        params = _getMessageParams(params);
		if (params) {
			if (params && params.length > 0) {
    			for (let i = 0; i < params.length; i++) {
    				messageContent = messageContent.replace('{{p' + i.toString() + '}}', params[i]);
    			}
			}
		}
		return messageContent;
    };

    /**
     * @public
     * @method module:common/services.MessageSrv#getMessageParams 取得消息参数
     * @param  {String} params 消息参数
     */
    let _getMessageParams = function(params) {
        //参数不是json格式
        return _buildMessageParams(eval(params)); // jshint ignore:line
    };

    return {
        confirm: _confirm,
        // 追加错误消息
        error: _error,
        // 追加正常消息
        info: _info,
        // 追加成功消息
        success: _success,
        // 取得系统消息
        getMessage: _getMessage,
        // 取得消息参数
        getMessageParams: _getMessageParams
    };
}

module.exports = {
    name: 'MessageSrv',
    fn: MessageSrv
};
