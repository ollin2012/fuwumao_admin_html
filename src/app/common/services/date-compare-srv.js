'use strict';

/**
 * @class DateCompareSrv API服务
 * @alias module:common/services.DateCompareSrv
 * 
 * @param MessageSrv
 */
function DateCompareSrv(MessageSrv) {
    'ngInject';

    /**
     * @method module:common/services.DateCompareSrv#exec API执行
     * @param  {Object} dateCompParams  输入的日期对象
     * @param  {Object} messageParams  错误信息置换文字列
     */
    function dateCompare(dateCompParams, messageParams) {
    	// 判断输入日期是否是未来日期
    	let targetDate;
    	let compareDate;

    	if (dateCompParams.targetDate instanceof Date === true) {
    		targetDate = dateCompParams.targetDate;
    	} else {
    		targetDate = new Date(dateCompParams.targetDate);
    	}

    	if (dateCompParams.compareDate instanceof Date === true) {
    		compareDate = dateCompParams.compareDate;
    	} else {
    		compareDate = new Date(dateCompParams.compareDate);
    	}

    	let judgeFlg = false;
        if (targetDate.getFullYear() < compareDate.getFullYear()) {
        	judgeFlg = true;
        } else if (targetDate.getFullYear() === compareDate.getFullYear()) {
        	if (targetDate.getMonth() < compareDate.getMonth()) {
            	judgeFlg = true;
        	} else if (targetDate.getMonth() === compareDate.getMonth()) {
        		if (targetDate.getDate() <= compareDate.getDate()) {
                	judgeFlg = true;
        		}
        	}
        }

        if (judgeFlg) {
        	MessageSrv.error('message.compareDateErr', messageParams);
            return true;
        }
    }

    return {
    	dateCompare
    };
}

module.exports = {
    name: 'DateCompareSrv',
    fn: DateCompareSrv
};
