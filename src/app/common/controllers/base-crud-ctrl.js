'use strict';

const _ = require('lodash');

/**
 * @class BaseCrudCtrl 控制器基类：增删改查的控制器
 * @alias module:common/controllers.BaseCrudCtrl
 * 
 * @param $window
 * @param $state
 * @param $stateParams
 * @param $q
 * @param ApiSrv
 * @param {Object} vm           控制器的value model
 * @param {Object} ctrlOpts
 * @param {String} ctrlOpts.modelName 模型的名称
 * @param {Function} [ctrlOpts.preSaveFn] 保存前处理，如果返回false或者Promise.reject的话则不做保存
 * @param {Function} [ctrlOpts.postSaveFn] 保存后处理
 * @param {Function} [ctrlOpts.preGetDetailFn] 取得详情前处理
 * @param {Function} [ctrlOpts.postGetDetailFn] 取得详情后处理
 *
 * @example
 * 
 *  // 更新前处理，可选返回处理结果或者Promise
    function preSaveFn() {        
        vm.model.xxx = 'xxxxxx';    // 编辑模型的内容
    }

    let ctrlOpts = {
        modelName: 'adminRole',
        preSaveFn
    };
    angular.extend(this, $controller('BaseCrudCtrl', { vm: vm, ctrlOpts }));
 *
 */
function BaseCrudCtrl($window, $state, $stateParams, $q, ApiSrv, vm, ctrlOpts, MessageSrv) {
    'ngInject';

    // 初始化
    (function init() {
        vm.model = {};

        // 取得当前处理的Action类别
        vm.action = _.last(_.split($state.current.name, '.'));

        vm.isDeleteAction = (vm.action === 'delete');
    })();

    function _getListState() {
        return  'main.' + ctrlOpts.modelName + '.list';
    }

    // 新增
    function _saveForNew() {
        let listState = _getListState();

        ApiSrv.exec(ctrlOpts.modelName + '/create', vm.model)
            .then(function() {
                if (ctrlOpts.postSaveFn) {
                    ctrlOpts.postSaveFn();
                } else {
                    MessageSrv.success('message.createOk');
                    $state.go(listState);
                }
            });
    }

    // 更新
    function _saveForEdit() {
        ApiSrv.exec(ctrlOpts.modelName + '/update', vm.model)
            .then(function() {
                if (ctrlOpts.postSaveFn) {
                    ctrlOpts.postSaveFn();
                } else {                
                    MessageSrv.success('message.updateOk');
                    vm.back();
                }
            });
    }

    // 删除
    function _delete() {
        ApiSrv.exec(ctrlOpts.modelName + '/delete', vm.model)
            .then(function() {
                if (ctrlOpts.postSaveFn) {
                    ctrlOpts.postSaveFn();
                } else {
                    MessageSrv.success('message.deleteOk');
                    vm.back();
                }
            });        
    }

    /**
     * 取得详情
     * 
     * 调用getDetail API，取得结果设置到vm.model中
     * 如果定义了ctrlOpts.preGetDetailFn，则在取得结果前会先调用该函数
     * 如果定义了ctrlOpts.postGetDetailFn，则在取得结果后会先调用该函数
     */
    vm.getDetail = function() {
    	let apiParams = {};
    	if (ctrlOpts.preGetDetailFn) {
    		apiParams = ctrlOpts.preGetDetailFn(apiParams);
    	} else {
    		apiParams.id = $stateParams.id;
    	}
        ApiSrv.exec(ctrlOpts.modelName + '/getDetail', apiParams)
            .then(function(data) {
                if (ctrlOpts.postGetDetailFn) {
                    vm.model = ctrlOpts.postGetDetailFn(data);
                }  else {
                    vm.model = data;
                }
            });
    };

    /**
     * 保存
     * 
     * 根据当前画面的种类，调用create、update或delete API
     * 如果定义了ctrlOpts.preSaveFn，则在保存前会先调用该函数
     */
    vm.save = function() {
        let confirmMsg;
        let saveFn;

        // 设定提示消息以及保存的函数
        if (vm.action === 'new') {
            confirmMsg = 'confirm.create';
            saveFn = _saveForNew;
        } else if (vm.action === 'edit') {
            confirmMsg = 'confirm.update';
            saveFn = _saveForEdit;
        } else if (vm.action === 'delete') {
            confirmMsg = 'confirm.delete';
            saveFn = _delete;
        }       
        // 前处理hook
        if (ctrlOpts.preSaveFn) {
            $q.when(ctrlOpts.preSaveFn()).then(result => {
                if (result === undefined || result) {
                    // 确认保存后执行
                    MessageSrv.confirm(confirmMsg).then(() => {
                        // 执行保存        
                        saveFn();
                    });
                }
            });
        } else {
            // 确认保存后执行
            MessageSrv.confirm(confirmMsg).then(() => {
                // 执行保存        
                saveFn();
            });
        }
    };

    /**
     * 回退
     */
    vm.back = function() {
        if ($window.history.length > 1) {
            $window.history.back();
        } else {
            $state.go(_getListState());
        }
    };
}

/**
 * BaseCrudCtrl 基础CRUD控制器
 */
module.exports = {
    name: 'BaseCrudCtrl',
    fn: BaseCrudCtrl
};
