/**
 * mock for {{modelText}}详情
 * @type {Object}
 *
 * params:
 * {id: "1"}
 */
module.exports = {
    '/api/{{modelName}}/getDetail': {
        POST: {
            data: {
                resultCd: 1,
                results: {
                    id: 1,
                    name: '{{modelText}}1',
                }
            }
        }
    }
};
