/**
 * mock for {{modelText}}删除
 * @type {Object}
 */
module.exports = {
    '/api/{{modelName}}/delete': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
