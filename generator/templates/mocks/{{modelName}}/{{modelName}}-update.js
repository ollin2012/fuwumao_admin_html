/**
 * mock for {{modelText}}变更
 * @type {Object}
 */
module.exports = {
    '/api/{{modelName}}/update': {
        POST: {
            data: {
                resultCd: 1
            }
        }
    }
};
